{-# OPTIONS_GHC -fno-warn-orphans #-}

module Donna.Data.BaseUrlGen where

import Data.Char as Char
import Donna.Data.BaseUrl
import Donna.Data.GenImport

instance GenUnchecked Scheme

instance GenValid Scheme

instance GenUnchecked BaseUrl

instance GenValid BaseUrl where
  genValid =
    BaseUrl <$> genValid <*> ((:) <$> validChar <*> validString)
      <*> (((`mod` 65536) . abs) <$> genValid)
      <*> validString `suchThat` isValid
    where
      validString = genListOf validChar
      validChar = (chr <$> choose (0, 128)) `suchThat` (\c -> Char.isAlphaNum c && Char.isLatin1 c && c /= ' ')
