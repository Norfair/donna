{-# OPTIONS_GHC -fno-warn-orphans #-}

module Donna.Data.AliasGen where

import Donna.Data.GenImport

import Donna.Data.Alias

instance GenValid Alias where
  genValid = genValidStructurally
  shrinkValid = shrinkValidStructurally
