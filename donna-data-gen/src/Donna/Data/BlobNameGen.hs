{-# OPTIONS_GHC -fno-warn-orphans #-}

module Donna.Data.BlobNameGen where

import Donna.Data.GenImport

import Donna.Data.BlobName

instance GenValid BlobName where
  genValid = genValidStructurally
  shrinkValid = shrinkValidStructurally
