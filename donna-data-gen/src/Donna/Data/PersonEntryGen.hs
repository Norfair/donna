{-# LANGUAGE FlexibleInstances #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}

module Donna.Data.PersonEntryGen where

import Donna.Data.GenImport

import Donna.Data.PersonEntry

instance GenValid RawYaml where
  genValid = genValidStructurally
  shrinkValid = shrinkValidStructurally
instance GenValid  a => GenValid (ForEditor a) where
  genValid = genValidStructurally
  shrinkValid = shrinkValidStructurally

instance GenValid PersonEntry where
  genValid = genValidStructurally
  shrinkValid = shrinkValidStructurally

instance GenValid PersonPropertyVal where
  genValid = genValidStructurally
  shrinkValid = shrinkValidStructurally

instance GenValid a => GenValid (WithLastUpdated a) where
  genValid = genValidStructurally
  shrinkValid = shrinkValidStructurally
