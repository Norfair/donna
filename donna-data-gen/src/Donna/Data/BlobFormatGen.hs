{-# OPTIONS_GHC -fno-warn-orphans #-}

module Donna.Data.BlobFormatGen where

import Donna.Data.GenImport

import Donna.Data.BlobFormat

instance GenValid BlobFormat where
  genValid = genValidStructurally
  shrinkValid = shrinkValidStructurally
