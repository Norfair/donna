{-# OPTIONS_GHC -fno-warn-orphans #-}

module Donna.Data.UsernameGen where

import Data.Char
import qualified Data.Text as T
import Donna.Data.GenImport
import Donna.Data.Username

instance GenValid UsernameChar where
  genValid = ((UsernameChar . chr) <$> choose (0, 128)) `suchThat` isValid
  shrinkValid = shrinkValidStructurally

instance GenValid Username where
  genValid =
    (Username . T.pack . map unUsernameChar)
      <$> ((:) <$> genValid <*> ((:) <$> genValid <*> ((:) <$> genValid <*> genValid)))
  shrinkValid = shrinkValidStructurally
