{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE UndecidableInstances #-}
{-# OPTIONS -fno-warn-orphans #-}

module Donna.Data.GenImport
  ( module X
  ) where

import Donna.Data.Import as X
import Donna.Data.Sql as X ()

import Test.QuickCheck as X

import Data.GenValidity as X
import Data.GenValidity.ByteString as X ()
import Data.GenValidity.Containers as X ()
import Data.GenValidity.Text as X ()
import Data.GenValidity.Time as X ()
import Data.GenValidity.UUID.Typed as X ()

import Database.Persist
import Database.Persist.Sql

instance ToBackendKey SqlBackend record => GenUnchecked (Key record) where
  genUnchecked = toSqlKey <$> genUnchecked
  shrinkUnchecked = shrinkNothing

instance ToBackendKey SqlBackend record => GenValid (Key record) where
  genValid = toSqlKey <$> genValid

instance (GenUnchecked a, ToBackendKey SqlBackend a) => GenUnchecked (Entity a) where
  genUnchecked = Entity <$> genUnchecked <*> genUnchecked
  shrinkUnchecked (Entity k v) = [Entity k' v' | (k', v') <- shrinkUnchecked (k, v)]

instance (GenValid a, ToBackendKey SqlBackend a) => GenValid (Entity a) where
  genValid = Entity <$> genValid <*> genValid
  shrinkValid (Entity k v) = [Entity k' v' | (k', v') <- shrinkValid (k, v)]
