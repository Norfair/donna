{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE TypeApplications #-}

module Donna.Data.BaseUrlSpec
  ( spec
  ) where

import Donna.Data.TestImport

spec :: Spec
spec = do
  describe "BaseUrl" $ do
    eqSpecOnValid @BaseUrl
    ordSpecOnValid @BaseUrl
    jsonSpecOnValid @BaseUrl
    persistSpecOnValid @BaseUrl
