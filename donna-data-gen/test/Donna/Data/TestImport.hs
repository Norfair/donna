{-# LANGUAGE GADTs #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE LiberalTypeSynonyms #-}
{-# LANGUAGE RankNTypes #-}

module Donna.Data.TestImport
  ( module Donna.Data.TestImport
  , module X
  ) where

import Donna.Data.GenImport as X

import Test.Hspec as X
import Test.Validity as X
import Test.Validity.Aeson as X
import Test.Validity.Persist as X

import Donna.Data as X
import Donna.DataGen as X ()

failure :: String -> IO a
failure s = do
  expectationFailure s
  undefined -- Won't get here anyway
