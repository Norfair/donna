{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE TypeApplications #-}

module Donna.Data.PersonEntrySpec
  ( spec
  ) where

import Donna.Data.TestImport

spec :: Spec
spec = do
  describe "PersonEntry" $ do
    eqSpecOnValid @PersonEntry
    ordSpecOnValid @PersonEntry
    jsonSpecOnValid @PersonEntry
    persistSpecOnValid @PersonEntry
    describe "newPersonEntry" $
      it "produces valids on valids" $ producesValidsOnValids newPersonEntry
    describe "personEntry" $ it "produces valids on valids" $ producesValidsOnValids personEntry
    describe "entryContentsText" $
      it "produces valids on valids" $ producesValidsOnValids entryContentsText
    describe "entryContentsBS" $
      it "produces valids on valids" $ producesValidsOnValids entryContentsBS
    describe "updatePersonEntry" $
      it "produces valids on valids" $ producesValidsOnValids3 updatePersonEntry
    describe "constructPersonEntry" $
      it "produces valids on valids" $ producesValidsOnValids2 constructPersonEntry
    describe "reconstructPersonEntry" $
      it "produces valids on valids" $ producesValidsOnValids3 reconstructPersonEntry
    describe "parsePersonEntry" $
      it "produces valids on valids" $ producesValidsOnValids2 parsePersonEntry
    describe "parseEntryFileContents" $
      it "produces valids on valids" $ producesValidsOnValids parseEntryFileContents
  describe "PersonProperty" $ do
    eqSpecOnValid @PersonProperty
    ordSpecOnValid @PersonProperty
    jsonSpecOnValid @PersonProperty
  describe "PersonPropertyVal" $ do
    eqSpecOnValid @PersonPropertyVal
    ordSpecOnValid @PersonPropertyVal
    jsonSpecOnValid @PersonPropertyVal
