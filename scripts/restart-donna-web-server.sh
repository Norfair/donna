#!/usr/bin/env bash
set -e
set -x

killall 'donna-web-server serve' || true

donna-web-server serve &
