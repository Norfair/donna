#!/usr/bin/env bash

set -e
set -x

cd donna-server

stack install :donna-server \
  --file-watch \
  --exec='../scripts/restart-donna-server.sh' \
  --fast \
  --ghc-options=-freverse-errors \
  --flag donna-server:dev
