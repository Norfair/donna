#!/usr/bin/env bash

set -e
set -x

cd donna-web-server

stack install :donna-web-server \
  --file-watch \
  --exec='../scripts/restart-donna-web-server.sh' \
  --fast \
  --ghc-options=-freverse-errors \
  --flag donna-server:dev \
  --flag donna-web-server:dev $@ \
  --no-nix-pure
