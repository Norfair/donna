#!/bin/bash
set -e
set -x

pkill -f 'donna-server serve' || true

donna-server serve &
