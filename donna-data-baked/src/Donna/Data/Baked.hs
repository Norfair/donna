{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE OverloadedStrings #-}

module Donna.Data.Baked
  ( module Donna.Data.Baked
  , module X
  ) where

import Donna.Data.Baked.Import

import Donna.Data.Baked.FromProperty as X
import Donna.Data.Baked.EmailAddress as X
import Donna.Data.Baked.PhoneNumber as X
import Donna.Data.Baked.Name as X

import Donna.Data

newtype Met =
  Met
    { metText :: Text
    }
  deriving (Show, Eq, Generic)

instance Validity Met

instance FromProperty (Maybe Met) where
  fromProperty pp = Met <$> pp `atKey` "met"

data Gender
  = Male
  | Female
  | Other Text
  deriving (Show, Eq, Generic)

instance Validity Gender

instance FromProperty (Maybe Gender) where
  fromProperty pp =
    flip fmap (pp `atKey` "gender") $ \gt ->
      case gt of
        "male" -> Male
        "female" -> Female
        _ -> Other gt

data EmailAddressWithPurpose =
  EmailAddressWithPurpose (Maybe Text) Text
  deriving (Show, Eq, Generic)

instance FromProperty [EmailAddressWithPurpose] where
  fromProperty pp =
    case pp `atKey` "email" of
      Nothing -> []
      Just pp' -> emailsFrom pp'
    where
      emailsFrom :: PersonProperty -> [EmailAddressWithPurpose]
      emailsFrom (WithLastUpdated _ pp') =
        case pp' of
          PVal t -> [EmailAddressWithPurpose Nothing t]
          PList ls -> concatMap emailsFrom ls
          PMap ls ->
            flip concatMap ls $ \(t, p) ->
              map
                (\(EmailAddressWithPurpose mp ea) ->
                   EmailAddressWithPurpose (Just $ fromMaybe t mp) ea) $
              emailsFrom p
