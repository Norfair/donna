{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE NoImplicitPrelude #-}

module Donna.Data.Baked.FromProperty where

import Donna.Data.Baked.Import

import Donna.Data

class FromProperty a where
  fromProperty :: PersonProperty -> a

atKey :: FromProperty (Maybe a) => PersonProperty -> Text -> Maybe a
atKey (WithLastUpdated _ (PMap tups)) key = lookup key tups >>= fromProperty
atKey _ _ = Nothing

possiblyMultipleAt :: Text -> PersonProperty -> [Text]
possiblyMultipleAt key pp =
  case lastUpdatedValue pp of
    PVal _ -> []
    PList _ -> []
    PMap kvs ->
      case lookup key kvs of
        Nothing -> []
        Just pv ->
          case lastUpdatedValue pv of
            PVal v -> [v]
            PList vs -> mapMaybe (onValue . lastUpdatedValue) vs
            PMap vs -> mapMaybe (onValue . lastUpdatedValue . snd) vs
  where
    onValue (PVal v) = Just v
    onValue _ = Nothing

fromEntry :: FromProperty a => PersonEntry -> a
fromEntry = inEntry fromProperty

inEntry :: (PersonProperty -> a) -> PersonEntry -> a
inEntry func = func . personEntryProperties

instance FromProperty (Maybe Text) where
  fromProperty (WithLastUpdated _ (PVal ppv)) = Just ppv
  fromProperty _ = Nothing

instance FromProperty (Maybe PersonProperty) where
  fromProperty = pure
