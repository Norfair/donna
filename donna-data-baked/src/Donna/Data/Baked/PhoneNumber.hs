{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE OverloadedStrings #-}

module Donna.Data.Baked.PhoneNumber where

import Donna.Data.Baked.Import

import Data.Aeson
import Data.Char as Char
import qualified Data.Set as S
import qualified Data.Text as T

import Donna.Data.Baked.FromProperty

newtype PhoneNumber =
  PhoneNumber
    { phoneNumberText :: Text
    }
  deriving (Show, Eq, Ord, Generic, FromJSON, ToJSON)

-- TODO normalisation and rendering?
instance Validity PhoneNumber where
  validate (PhoneNumber t) =
    mconcat
      [ delve "phoneNumberText" t
      , decorateList (T.unpack t) $ \c ->
          declare "The character is not a space" $ not $ Char.isSpace c
      ]

instance FromProperty [PhoneNumber] where
  fromProperty = map PhoneNumber . possiblyMultipleAt "phone number"

instance FromProperty (Set PhoneNumber) where
  fromProperty = S.fromList . fromProperty

phoneNumber :: Text -> PhoneNumber
phoneNumber = PhoneNumber . T.filter (not . Char.isSpace)
