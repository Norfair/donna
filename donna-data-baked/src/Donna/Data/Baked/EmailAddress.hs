{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE OverloadedStrings #-}

module Donna.Data.Baked.EmailAddress where

import Donna.Data.Baked.Import

import Data.Aeson
import qualified Data.Set as S

import Donna.Data.Baked.FromProperty

newtype EmailAddress =
  EmailAddress
    { emailAddressText :: Text
    }
  deriving (Show, Eq, Ord, Generic, FromJSON, ToJSON)

-- TODO normalisation and rendering?
instance Validity EmailAddress

instance FromProperty [EmailAddress] where
  fromProperty = map EmailAddress . possiblyMultipleAt "email"

instance FromProperty (Set EmailAddress) where
  fromProperty = S.fromList . fromProperty

emailAddress :: Text -> Maybe EmailAddress
emailAddress = constructValid . EmailAddress
