{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE UndecidableInstances #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}

module Donna.Web.Server.Data.DB
  ( module Donna.Web.Server.Data.DB
  , module Donna.Data
  , SqlBackend
  , SqlPersistT
  , module Database.Persist
  ) where

import Donna.Web.Server.Data.Import

import Database.Persist
import Database.Persist.Sql
import Database.Persist.TH

import Donna.Data

import Network.Google.People.Types as Gogol

import Donna.Web.Server.Data.Google ()

share
  [mkPersist sqlSettings, mkMigrate "migrateAll"]
  [persistLowerCase|

-- Just a cache, no harm is done if this is lost.
-- We can get rid of this when gogol supports
-- web server auth https://github.com/brendanhay/gogol/issues/136
GoogleContact
  user Username
  person Gogol.Person
  timestamp UTCTime
  deriving Show Eq Generic
|]
