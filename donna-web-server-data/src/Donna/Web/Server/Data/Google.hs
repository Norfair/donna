{-# LANGUAGE NoImplicitPrelude #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}

module Donna.Web.Server.Data.Google where

import Donna.Web.Server.Data.Import

import Database.Persist
import Database.Persist.Sql


import Network.Google.People.Types as Gogol

instance PersistField Gogol.Person where
  fromPersistValue = fromPersistValueJSON
  toPersistValue = toPersistValueJSON

instance PersistFieldSql Gogol.Person where
  sqlType Proxy = SqlBlob
