{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TypeOperators #-}
{-# OPTIONS_GHC -fno-warn-missing-signatures #-}

module Donna.Client
  ( module Donna.API
  , module Donna.Client
  ) where

import Donna.API.Import

import Servant.API.Flatten
import Servant.Client

import Donna.API

clientPostRegister :: Register -> ClientM NoContent
clientPostLogin ::
     Login
  -> ClientM (Headers '[ Header "Set-Cookie" SetCookie, Header "Set-Cookie" SetCookie] NoContent)
clientPostRegister :<|> clientPostLogin = client (flatten donnaUnprotectedAPI)

clientGetAliases :: Token -> ClientM (Map Alias PersonUuid)
clientGetPersonAliases :: Token -> PersonUuid -> ClientM (Set Alias)
clientPostAlias :: Token -> PersonUuid -> Alias -> ClientM NoContent
clientDeleteAlias :: Token -> Alias -> ClientM NoContent
clientDeletePersonAlias :: Token -> PersonUuid -> Alias -> ClientM NoContent
clientGetPerson :: Token -> PersonUuid -> ClientM PersonInfo
clientGetPeopleProperties :: Token -> ClientM (Map PersonUuid PersonPropertyInfo)
clientPostPersonWithAliases :: Token -> Set Alias -> ClientM PersonUuid
clientPostNote :: Token -> NewNote -> ClientM NoContent
clientPostEntry :: Token -> PersonUuid -> PersonEntry -> ClientM NoContent
clientPostSync :: Token -> ClientSyncRequest -> ClientM ServerSyncResponse
clientGetGraph :: Token -> ClientM SocialGraph
clientGetAliases :<|> clientGetPersonAliases :<|> clientPostAlias :<|> clientDeleteAlias :<|> clientDeletePersonAlias :<|> clientGetPerson :<|> clientPostPersonWithAliases :<|> clientGetPeopleProperties :<|> clientPostNote :<|> clientPostEntry :<|> clientPostSync :<|> clientGetGraph =
  client (flatten donnaProtectedAPI)

runClient :: ClientEnv -> ClientM a -> IO (Either ClientError a)
runClient = flip runClientM

clientLogin :: Login -> ClientM (Either HeaderProblem Token)
clientLogin = fmap (fmap sessionToToken) . clientLoginSession

clientLoginSession :: Login -> ClientM (Either HeaderProblem SetCookie)
clientLoginSession lf = do
  res <- clientPostLogin lf
  pure $
    case res of
      Headers NoContent (HCons _ (HCons sessionHeader HNil)) ->
        case sessionHeader of
          MissingHeader -> Left ProblemMissingHeader
          UndecodableHeader b -> Left $ ProblemUndecodableHeader b
          Header session -> Right session

sessionToToken :: SetCookie -> Token
sessionToToken = Token . setCookieValue

data HeaderProblem
  = ProblemMissingHeader
  | ProblemUndecodableHeader ByteString
  deriving (Show, Eq, Generic)

login :: ClientEnv -> Login -> IO (Either LoginError Token)
login cenv lf = do
  errOrRes <- runClient cenv $ clientLogin lf
  pure $
    case errOrRes of
      Left se -> Left $ LoginServantError se
      Right (Left hp) -> Left $ LoginHeaderProblem hp
      Right (Right t) -> Right t

data LoginError
  = LoginServantError ClientError
  | LoginHeaderProblem HeaderProblem
  deriving (Show, Eq, Generic)
