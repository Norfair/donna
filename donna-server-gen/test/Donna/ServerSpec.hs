{-# LANGUAGE NoImplicitPrelude #-}

module Donna.ServerSpec
  ( spec
  ) where

import Donna.Server.TestImport

spec :: Spec
spec =
  serverSpec $ do
    describe "GetPerson" $
      it "produces valids on valids" $ \cenv ->
        forAllValid $ \puuid -> do
          withValidNewUser cenv $ \t -> do
            res <- runTestClientOrError cenv $ clientGetPerson t puuid
            shouldBeValid res
    describe "PostPersonWithAliases" $
      it "produces valids on valids" $ \cenv ->
        forAllValid $ \as -> do
          withValidNewUser cenv $ \t -> do
            res <- runTestClientOrError cenv $ clientPostPersonWithAliases t as
            shouldBeValid res
    describe "GetPeopleProperties" $
      it "produces valids on valids" $ \cenv -> do
        withValidNewUser cenv $ \t -> do
          res <- runTestClientOrError cenv $ clientGetPeopleProperties t
          shouldBeValid res
    describe "PostNote" $
      it "produces valids on valids" $ \cenv ->
        forAllValid $ \nn -> do
          withValidNewUser cenv $ \t -> do
            res <- runTestClientOrError cenv $ clientPostNote t nn
            shouldBeValid res
    describe "PostEntry" $
      it "produces valids on valids" $ \cenv ->
        forAllValid $ \puuid ->
          forAllValid $ \e -> do
            withValidNewUser cenv $ \t -> do
              res <- runTestClientOrError cenv $ clientPostEntry t puuid e
              shouldBeValid res
    describe "GetGraph" $
      it "produces valids on valids" $ \cenv -> do
        withValidNewUser cenv $ \t -> do
          res <- runTestClientOrError cenv $ clientGetGraph t
          shouldBeValid res
