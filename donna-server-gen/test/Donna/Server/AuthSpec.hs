{-# LANGUAGE NoImplicitPrelude #-}

module Donna.Server.AuthSpec where

import Donna.Server.TestImport

spec :: Spec
spec =
  serverSpec $ do
    describe "PostRegister" $
      it "does not crash" $ \c ->
        forAllValid $ \register -> do
          NoContent <- runTestClientOrError c $ clientPostRegister register
          pure ()
    describe "PostLogin" $
      it "logs in succesfully with a new account" $ \c ->
        forAllValid $ \register -> do
          NoContent <- runTestClientOrError c $ clientPostRegister register
          void $ login c $ registerLogin register
          pure ()
