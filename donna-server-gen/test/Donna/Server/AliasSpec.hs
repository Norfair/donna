{-# LANGUAGE NoImplicitPrelude #-}

module Donna.Server.AliasSpec
  ( spec
  ) where

import Donna.Server.TestImport

import qualified Data.Map as M
import qualified Data.Set as S

spec :: Spec
spec =
  serverSpec $ do
    describe "GetAliases" $ do
      it "produces valids on valids" $ \cenv -> do
        withValidNewUser cenv $ \t -> do
          res <- runTestClientOrError cenv $ clientGetAliases t
          shouldBeValid res
      it "gets the alias that was just created" $ \cenv ->
        forAllValid $ \puuid ->
          forAllValid $ \a ->
            withValidNewUser cenv $ \t -> do
              res <-
                runTestClientOrError cenv $ do
                  NoContent <- clientPostAlias t puuid a
                  clientGetAliases t
              res `shouldBe` M.singleton a puuid
    describe "GetPersonAliases" $ do
      it "produces valids on valids" $ \cenv ->
        forAllValid $ \puuid -> do
          withValidNewUser cenv $ \t -> do
            res <- runTestClientOrError cenv $ clientGetPersonAliases t puuid
            shouldBeValid res
      it "gets the alias that was just created" $ \cenv ->
        forAllValid $ \puuid ->
          forAllValid $ \a ->
            withValidNewUser cenv $ \t -> do
              res <-
                runTestClientOrError cenv $ do
                  NoContent <- clientPostAlias t puuid a
                  clientGetPersonAliases t puuid
              res `shouldBe` S.singleton a
    describe "PostAlias" $ do
      it "produces valids on valids" $ \cenv ->
        forAllValid $ \puuid ->
          forAllValid $ \a -> do
            withValidNewUser cenv $ \t -> do
              res <- runTestClientOrError cenv $ clientPostAlias t puuid a
              shouldBeValid res
      it "fails if the alias already exists for a different person" $ \cenv ->
        forAllValid $ \puuid1 ->
          forAll (genValid `suchThat` (/= puuid1)) $ \puuid2 ->
            forAllValid $ \a ->
              withValidNewUser cenv $ \t -> do
                errOrUnit <-
                  runClient cenv $ do
                    NoContent <- clientPostAlias t puuid1 a
                    NoContent <- clientPostAlias t puuid2 a
                    pure ()
                case errOrUnit of
                  Right () -> failure "Should not have succeeded"
                  Left (FailureResponse _ r) -> statusCode (responseStatusCode r) `shouldBe` 409
                  Left err -> failure $ "Wrong type of error: " <> ppShow err
    describe "DeleteAlias" $ do
      it "produces valids on valids" $ \cenv ->
        forAllValid $ \a -> do
          withValidNewUser cenv $ \t -> do
            res <- runTestClientOrError cenv $ clientDeleteAlias t a
            shouldBeValid res
      it "truly deletes an alias" $ \cenv ->
        forAllValid $ \puuid ->
          forAllValid $ \a -> do
            withValidNewUser cenv $ \t -> do
              res <-
                runTestClientOrError cenv $ do
                  NoContent <- clientPostAlias t puuid a
                  NoContent <- clientDeleteAlias t a
                  clientGetPersonAliases t puuid
              res `shouldNotSatisfy` (S.member a)
    describe "DeletePersonAlias" $ do
      it "produces valids on valids" $ \cenv ->
        forAllValid $ \puuid -> do
          forAllValid $ \a -> do
            withValidNewUser cenv $ \t -> do
              res <- runTestClientOrError cenv $ clientDeletePersonAlias t puuid a
              shouldBeValid res
      it "truly deletes a person's alias" $ \cenv ->
        forAllValid $ \puuid ->
          forAllValid $ \a -> do
            withValidNewUser cenv $ \t -> do
              res <-
                runTestClientOrError cenv $ do
                  NoContent <- clientPostAlias t puuid a
                  NoContent <- clientDeletePersonAlias t puuid a
                  clientGetPersonAliases t puuid
              res `shouldNotSatisfy` (S.member a)
