{-# LANGUAGE GADTs #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE LiberalTypeSynonyms #-}
{-# LANGUAGE RankNTypes #-}

module Donna.Server.GenImport
  ( module Donna.Server.GenImport
  , module X
  ) where

import Donna.APIGen as X

import Test.Hspec as X
import Test.Hspec.QuickCheck as X

import Network.HTTP.Client as Http
import Network.HTTP.Types as X (Status(..))
import Network.Wai.Handler.Warp as Warp

import Path.IO

import Servant.API as X
import Servant.Auth.Server ()
import Servant.Client as X

import Donna.Client as X

import Donna.Server
import Donna.Server.Env
import Donna.Server.OptParse.Types

serverSpec :: SpecWith ClientEnv -> Spec
serverSpec = modifyMaxShrinks (const 0) . around withTestServer

withTestServer :: (ClientEnv -> IO a) -> IO a
withTestServer func = do
  man <- Http.newManager Http.defaultManagerSettings
  withSystemTempFile "donna-server-test" $ \tmpFile _ -> do
    let testSettings =
          ServeSettings {serveSetPort = error "should not be used", serveSetDBFile = tmpFile}
    withServerEnv testSettings $ \se -> do
      let makeTestApp = pure $ makeDonnaServerApp se
      Warp.testWithApplication makeTestApp $ \p -> do
        let cenv = mkClientEnv man (BaseUrl Http "127.0.0.1" p "")
        func cenv

withValidNewUser :: ClientEnv -> (Token -> IO ()) -> Expectation
withValidNewUser cenv func = withValidNewUserAndData cenv $ \_ -> func

withValidNewUserAndData :: ClientEnv -> (Register -> Token -> IO ()) -> Expectation
withValidNewUserAndData cenv func = do
  r <- randomRegistration
  withNewUser cenv r $ func r

randomRegistration :: IO Register
randomRegistration = do
  u1 <- nextRandomUUID :: IO (UUID Username) -- Dummy's that are significantly likely to be random enough
  u2 <- nextRandomUUID :: IO (UUID Text)
  pure
    Register
      {registerUsername = fromJust $ parseUsername $ uuidText u1, registerPassword = uuidText u2}

withNewUser :: MonadIO m => ClientEnv -> Register -> (Token -> m a) -> m a
withNewUser cenv r func = do
  t <-
    liftIO $ do
      NoContent <- runTestClientOrError cenv $ clientPostRegister r
      testLogin cenv (registerLogin r)
  func t

registerLogin :: Register -> Login
registerLogin register =
  Login {loginUsername = registerUsername register, loginPassword = registerPassword register}

testLogin :: ClientEnv -> Login -> IO Token
testLogin cenv lf = do
  errOrRes <- login cenv lf
  case errOrRes of
    Left err -> failure $ "Failed to login: " <> show err
    Right t -> pure t

runTestClientOrError :: ClientEnv -> ClientM a -> IO a
runTestClientOrError cenv cFunc = do
  errOrRes <- runClient cenv cFunc
  case errOrRes of
    Left se -> failError se
    Right a -> pure a

failError :: ClientError -> IO a
failError se =
  failure $
  case se of
    ConnectionError t -> displayException t
    _ -> displayException se

failure :: String -> IO a
failure s = do
  expectationFailure s
  undefined -- Won't get here anyway
