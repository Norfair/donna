{-# LANGUAGE OverloadedStrings #-}

module Donna.CLI.TestImport
  ( module Donna.CLI.TestImport,
    module X,
  )
where

import Donna.CLI.Env as X
import Donna.CLI.GenImport as X
import Donna.CLI.OptParse.Types as CLI
import Donna.Server.GenImport as X
import Path as X
import Path.IO as X
import Test.Validity as X

cliSpec :: SpecWith Env -> Spec
cliSpec sw = do
  describe "Without Sync" $ cliSpecWithoutSync sw
  describe "With Sync" $ cliSpecWithSync sw

cliSpecWithoutSync :: SpecWith Env -> Spec
cliSpecWithoutSync = around go
  where
    go :: (Env -> IO a) -> IO a
    go func =
      withSystemTempFile "donna-cli-test-cli-db" $ \tmpCliFile _ -> do
        let cliTestSettings =
              CLI.Settings
                { setDBFile = tmpCliFile,
                  setBaseUrl = Nothing,
                  setUsername = Nothing,
                  setPassword = Nothing,
                  setSyncStrategy = NeverSync
                }
        withEnv cliTestSettings func

cliSpecWithSync :: SpecWith Env -> Spec
cliSpecWithSync = serverSpec . (aroundWith go :: SpecWith Env -> SpecWith ClientEnv)
  where
    go :: (Env -> IO a) -> (ClientEnv -> IO a)
    go func cenv =
      withSystemTempFile "donna-cli-test-cli-db" $ \tmpCliFile _ -> do
        let burl = baseUrl cenv
        let cliTestSettings =
              CLI.Settings
                { setDBFile = tmpCliFile,
                  setBaseUrl = Just burl,
                  setUsername = Just $ Username "testuser",
                  setPassword = Nothing,
                  setSyncStrategy = AlwaysSync
                }
        withEnv cliTestSettings func

withLoggedInSync :: Env -> Register -> (ClientEnv -> D ()) -> IO ()
withLoggedInSync env reg func = runD env $ withLoggedInSyncD (envSyncClientEnv env) reg func

withLoggedInSyncD :: Maybe ClientEnv -> Register -> (ClientEnv -> D ()) -> D ()
withLoggedInSyncD mcenv reg func =
  case mcenv of
    Nothing -> pure ()
    Just cenv ->
      withNewUser cenv reg $ \_ -> withLoginUnprompted cenv (registerLogin reg) $ \_ -> func cenv
