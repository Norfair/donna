{-# OPTIONS_GHC -fno-warn-orphans #-}

module Donna.API.EndpointGen.SyncGen
  ( module Donna.API.EndpointGen.Sync.EntriesGen,
  )
where

import Data.GenValidity.Mergeful ()
import Data.GenValidity.Mergeless ()
import Donna.API.EndpointGen.Sync.EntriesGen
import Donna.API.GenImport

instance GenValid ClientSyncRequest where
  genValid = genValidStructurally
  shrinkValid = shrinkValidStructurally

instance GenValid ServerSyncResponse where
  genValid = genValidStructurally
  shrinkValid = shrinkValidStructurally

instance GenValid ClientStore where
  genValid = genValidStructurally
  shrinkValid = shrinkValidStructurally

instance GenValid AddedAlias where
  genValid = genValidStructurally
  shrinkValid = shrinkValidStructurally

instance GenValid AddedNote where
  genValid = genValidStructurally
  shrinkValid = shrinkValidStructurally

instance GenValid AddedBlob where
  genValid = genValidStructurally
  shrinkValid = shrinkValidStructurally

instance GenValid AddedEntry where
  genValid = genValidStructurally
  shrinkValid = shrinkValidStructurally
