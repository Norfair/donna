{-# OPTIONS_GHC -fno-warn-orphans #-}

module Donna.API.EndpointGen.AuthGen where

import Donna.API.GenImport

instance GenValid Register where
  genValid = genValidStructurally
  shrinkValid = shrinkValidStructurally

instance GenValid Login where
  genValid = genValidStructurally
  shrinkValid = shrinkValidStructurally
