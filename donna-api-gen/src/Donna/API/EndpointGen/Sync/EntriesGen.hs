module Donna.API.EndpointGen.Sync.EntriesGen where

import Donna.API.GenImport

genLaterAddedEntryForSamePerson :: AddedEntry -> Gen AddedEntry
genLaterAddedEntryForSamePerson (AddedEntry c p ae) = AddedEntry c p <$> genLaterPersonEntry ae

genLaterPersonEntry :: PersonEntry -> Gen PersonEntry
genLaterPersonEntry (PersonEntry pp) = PersonEntry <$> genLaterPersonProperty pp

genLaterPersonProperty :: PersonProperty -> Gen PersonProperty
genLaterPersonProperty (WithLastUpdated u _) =
  WithLastUpdated <$> (genValid `suchThat` (> u)) <*> genValid
