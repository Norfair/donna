{-# OPTIONS_GHC -fno-warn-orphans #-}

module Donna.API.EndpointGen.PersonGen where

import Donna.API.GenImport

instance GenValid PersonInfo where
  genValid = genValidStructurallyWithoutExtraChecking
  shrinkValid = shrinkValidStructurallyWithoutExtraFiltering

instance GenValid PersonNote where
  genValid = genValidStructurallyWithoutExtraChecking
  shrinkValid = shrinkValidStructurallyWithoutExtraFiltering
