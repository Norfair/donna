{-# OPTIONS_GHC -fno-warn-orphans #-}

module Donna.API.EndpointGen.PeopleGen where

import Donna.API.GenImport

instance GenValid PersonPropertyInfo where
  genValid = genValidStructurallyWithoutExtraChecking
  shrinkValid = shrinkValidStructurallyWithoutExtraFiltering
