{-# OPTIONS_GHC -fno-warn-orphans #-}

module Donna.API.EndpointGen.NoteGen where

import Donna.API.GenImport

instance GenValid NewNote where
  genValid = genValidStructurallyWithoutExtraChecking
  shrinkValid = shrinkValidStructurallyWithoutExtraFiltering
