{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE TypeApplications #-}

module Donna.API.PeopleSpec
  ( spec
  ) where

import Donna.API.TestImport

spec :: Spec
spec = do
  eqSpecOnValid @PersonPropertyInfo
  ordSpecOnValid @PersonPropertyInfo
  genValidSpec @PersonPropertyInfo
  jsonSpecOnValid @PersonPropertyInfo
