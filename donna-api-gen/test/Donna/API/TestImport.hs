{-# LANGUAGE GADTs #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE LiberalTypeSynonyms #-}
{-# LANGUAGE RankNTypes #-}

module Donna.API.TestImport
  ( module Donna.API.TestImport
  , module X
  ) where

import Donna.API.Import as X hiding (Result(..))

import Test.Hspec as X
import Test.QuickCheck as X
import Test.Validity as X
import Test.Validity.Aeson as X

import Donna.API as X
import Donna.APIGen as X

failure :: String -> IO a
failure s = do
  expectationFailure s
  undefined -- Won't get here anyway
