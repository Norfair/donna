{ envname }:
{ lib, pkgs, config, ... }:
with lib;

let
  cfg = config.services.donna."${envname}";
in
{
  options.services.donna."${envname}" = {
    enable = mkEnableOption "Donna ${envname} Service";
    web-hosts = mkOption {
      type = types.listOf (types.string);
      example = "donna.cs-syd.eu";
      description = "The host to serve web requests on";
    };
    api-hosts = mkOption {
      type = types.listOf (types.string);
      example = "api.donna.cs-syd.eu";
      description = "The host to serve API requests on";
    };
    web-port = mkOption {
      type = types.int;
      default = 8000;
      example = 8000;
      description = "The port to serve web requests on";
    };
    api-port = mkOption {
      type = types.int;
      default = 8001;
      example = 8001;
      description = "The port to serve API requests on";
    };
    google-analytics-tracking-id = mkOption {
      type = types.nullOr types.string;
      example = "UA-53296133-1";
      description = "The tracking id for google analytics";
    };
    google-search-console-verification-tag = mkOption {
      type = types.nullOr  types.string;
      example = "ADkAx2F-JQO9KJBBdLfAGuJ_OMqPOsX5MdGDsfd0Ggw";
      description = "The verification tag for google search console";
    };
    google-project-client-id = mkOption {
      type = types.nullOr  types.string;
      example =
        "000000000000-aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa.apps.googleusercontent.com";
      description = "The project client id for google";
    };
    google-project-client-secret = mkOption {
      type = types.nullOr  types.string;
      example = "aaaaaa-aaaaaaaaaaaaaaaaa";
      description = "The project client secret for google";
    };
    
    
    
    
  };
  config = mkIf cfg.enable {
    systemd.services = 
      let
        service =
          let
            workingDir = "/www/donna/${envname}/data/";
            donnaPkgs = (import ./pkgs.nix).donnaPackages;
          in
            {
              description = "Donna ${envname} Service";
              wantedBy = [ "multi-user.target" ];
              environment = {
                "DONNA_WEB_HOST" = "${builtins.toString (head cfg.web-hosts)}";
                "DONNA_WEB_PORT" = "${builtins.toString (cfg.web-port)}";
                "DONNA_API_PORT" = "${builtins.toString (cfg.api-port)}";
                "DONNA_API_URL" =
                  "https://${builtins.toString (head cfg.api-hosts)}";
                "DONNA_GOOGLE_CLIENT_ID" = "${cfg.google-project-client-id}";
                "DONNA_GOOGLE_CLIENT_SECRET" =
                  "${cfg.google-project-client-secret}";
                "DONNA_GOOGLE_ANALYTICS_TRACKING" =
                  "${cfg.google-analytics-tracking-id}";
                "DONNA_GOOGLE_SEARCH_CONSOLE_VERIFICATION" =
                  "${cfg.google-search-console-verification-tag}";
              };
              script = ''
                mkdir -p "${workingDir}"
                cd "${workingDir}"
                ${donnaPkgs.donna-web-server}/bin/donna-web-server \
                  serve
              '';
              serviceConfig = {
                Restart = "always";
                RestartSec = 1;
                Nice = 15;
              };
            };
      in {
        "donna-${envname}" = service;
      };
    networking.firewall.allowedTCPPorts =
      optionals cfg.enable [ (cfg.web-port) (cfg.api-port) ];
    services.nginx.virtualHosts =
          optionalAttrs cfg.enable {
            "${head (cfg.web-hosts)}" = {
              enableACME = true;
              forceSSL = true;
              locations."/".proxyPass =
                "http://localhost:${builtins.toString (cfg.web-port)}";
              serverAliases = tail cfg.web-hosts;
            };
            "${head (cfg.api-hosts)}" = {
              enableACME = true;
              forceSSL = true;
              locations."/".proxyPass =
                "http://localhost:${builtins.toString (cfg.api-port)}";
              serverAliases = tail cfg.api-hosts;
            };
          };
  };
}
