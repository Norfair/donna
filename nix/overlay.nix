final:
  previous:
    with final.haskell.lib;
    {
      donnaPackages =
      let
        donnaPkg = name:
          overrideCabal (doBenchmark (
            addBuildDepend (
              failOnAllWarnings (
                disableLibraryProfiling (
                    final.haskellPackages.callCabal2nix name (final.gitignoreSource (../. + "/${name}")) {}
                  )
                )
              )(final.haskellPackages.autoexporter)
              )) (old:
              {
                  preBuild = ''
                    export FRONT_CODE=${final.donna-web-server-front}
                  '';
              });
        donnaPkgWithComp =
          exeName: name:
            generateOptparseApplicativeCompletion exeName (donnaPkg name);
        donnaPkgWithOwnComp = name: donnaPkgWithComp name name;

            
      in {
        "donna-data" = donnaPkg "donna-data";
        "donna-data-gen" = donnaPkg "donna-data-gen";
        "donna-data-baked" = donnaPkg "donna-data-baked";
        "donna-data-baked-gen" = donnaPkg "donna-data-baked-gen";
        "donna-api" = donnaPkg "donna-api";
        "donna-api-gen" = donnaPkg "donna-api-gen";
        "donna-client" = donnaPkg "donna-client";
        "donna-client-gen" = donnaPkg "donna-client-gen";
        "donna-server-data" = donnaPkg "donna-server-data";
        "donna-server" = donnaPkgWithOwnComp "donna-server";
        "donna-server-gen" = donnaPkg "donna-server-gen";
        "donna-cli-data" = donnaPkg "donna-cli-data";
        "donna-cli" = donnaPkgWithComp "donna" "donna-cli";
        "donna-cli-gen" = donnaPkg "donna-cli-gen";
        "donna-web-server-data" = donnaPkg "donna-web-server-data";
        "donna-web-server-gen" = donnaPkg "donna-web-server-gen";
        "donna-web-server" = 
          let 
            bootstrap-css = builtins.fetchurl {
              url = https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css;
              sha256 = "sha256:0dldiln2s3z8iqc5ccjid2i5gh9527naas064bwly8x9lrfrxcb0";
            };
            bootstrap-js = builtins.fetchurl {
              url = https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js;
              sha256 = "sha256:0yspqrh258iqzvkacih0a1rnnngxhalvnmpczvsc2ff589wahd0a";
            };
            jquery-js = builtins.fetchurl {
              url = https://code.jquery.com/jquery-3.1.1.min.js;
              sha256 = "sha256:1gyrxy9219l11mn8c6538hnh3gr6idmimm7wv37183c0m1hnfmc5";
            };
            popper-js = builtins.fetchurl {
              url = https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js;
              sha256 = "sha256:1hhi8jw0gv2f1ir59ydzvgrspj6vb121wf36ddl4mdm93xza1wv6";
            };
            elm-js = builtins.fetchurl {
              url = https://unpkg.com/elm-canvas/elm-canvas.js;
              sha256 = "sha256:132naxmh1ln9051g8kbxkz8pps6y6srjflrj02f046s5p0w3i9b7";
            };
            rawPkg = donnaPkgWithOwnComp "donna-web-server";
          in overrideCabal rawPkg (old:
            { preConfigure = ''
                ${old.preConfigure or ""}

                mkdir -p static/
                cp ${jquery-js} static/jquery.js
                cp ${bootstrap-css} static/bootstrap.css
                cp ${popper-js} static/popper.js
                cp ${bootstrap-js} static/bootstrap.js
                cp ${elm-js} elm-canvas.js


              '';
            }
          );
        };
      haskellPackages = previous.haskellPackages.override (old:
        {
          overrides = final.lib.composeExtensions (old.overrides or (_:
            _:
              {})) (self:
            super:
              let
                typedUuidRepo = final.fetchFromGitHub {
                  owner = "NorfairKing";
                  repo = "typed-uuid";
                  rev = "dbc8fd4b56b78b1f9cf00bc2890d43dc19b97c5c";
                  sha256 = "sha256:12b8na513xq5smlgwvqaqpplj8blfl452vdq0j2kv40qaw6y9qp7";
                };
                typedUuidPkg = name:
                  super.callCabal2nix name (typedUuidRepo + "/${name}") {};
                persistentRepo = final.fetchFromGitHub {
                  owner = "yesodweb";
                  repo = "persistent";
                  rev = "919db853b9228c778543825ddf2469849dd06c00";
                  sha256 = "sha256:08psb5ibw8bafz5p8wq4ra308ivs6dn1rgj1ia5l98fdfhjymbr7";
                };
                persistentPkg = name:
                  addBuildDepend (dontCheck (super.callCabal2nix name (persistentRepo + "/${name}") {})) final.sqlite;
                yesodRepo = final.fetchFromGitHub {
                  owner = "yesodweb";
                  repo = "yesod";
                  rev = "636f35b0815b2dd0d534cfdcdf77f794aa91fd3b";
                  sha256 = "sha256:0ix52a3ckg0yihz83kvsyvgx13zad3lsp7pk6v65mjiijryk4nbf";
                };
                yesodPkg = name:
                  dontCheck (super.callCabal2nix name (yesodRepo + "/${name}") {});
                yesodStaticRemoteRepo = final.fetchFromGitHub {
                  owner = "NorfairKing";
                  repo = "yesod-static-remote";
                  rev = "22c0a92c1d62f1b8d432003844ef0636a9131b08";
                  sha256 = "sha256:1mz1fb421wccx7mbpn9qaj214w4sl4qali5rclx9fqp685jkfj05";
                };
                yesodStaticRemote =
                  dontCheck (super.callCabal2nix "yesod-static-remote" yesodStaticRemoteRepo {});
              in final.lib.genAttrs [
                "typed-uuid"
                "genvalidity-typed-uuid"
              ] typedUuidPkg // final.lib.genAttrs [
                "persistent"
                "persistent-sqlite"
                "persistent-template"
              ] persistentPkg //  final.lib.genAttrs [
                "yesod"
                "yesod-auth"
                "yesod-auth-oauth"
                "yesod-bin"
                "yesod-core"
                "yesod-eventsource"
                "yesod-form"
                "yesod-newsfeed"
                "yesod-persistent"
                "yesod-sitemap"
                "yesod-static"
                "yesod-test"
                "yesod-websockets"
              ] yesodPkg // {
                sqlite = null; # No idea why this is necessary ?!
                "yesod-static-remote" = yesodStaticRemote;
              } // final.donnaPackages);
        });
    }
