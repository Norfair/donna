let
  pkgsv = import (import ./nixpkgs.nix);
  pkgs = pkgsv {};
  validity-overlay = import (pkgs.fetchFromGitHub (import ./validity-version.nix) + "/nix/overlay.nix");
  pretty-relative-time-overlay = import (pkgs.fetchFromGitHub (import ./pretty-relative-time-version.nix) + "/nix/overlay.nix");
  mergeless-overlay = import (pkgs.fetchFromGitHub (import ./mergeless-version.nix) + "/nix/overlay.nix");
  mergeful-overlay = import (pkgs.fetchFromGitHub (import ./mergeful-version.nix) + "/nix/overlay.nix");
in pkgsv {
    overlays = [
      (import ./gitignore-src.nix)
      validity-overlay
      pretty-relative-time-overlay
      mergeless-overlay
      mergeful-overlay
      (import ../donna-web-server/front/nix/overlay.nix)
      (import ./overlay.nix)
    ];
    config.allowUnfree = true;
    config.allowBroken = true;
  }
