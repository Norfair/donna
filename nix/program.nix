{ lib, pkgs, config, ... }:

with lib;

let
  cfg = config.programs.donna;


in
{
  options =
    {
      programs.donna =
        {
          enable = mkEnableOption "Donna cli and syncing";
          extraConfig =     
            mkOption {    
              type = types.str;
              description = "Extra contents for the config file";
              default = ""; 
            };        
          backup =
            mkOption {
              default = null;
              type =
                types.nullOr (
                  types.submodule {
                    options = {
                      enable = mkEnableOption "Donna backups";
                      backupDir =
                        mkOption {
                          type = types.str;
                          default = ".donna/backup";
                          description = "The directory to backup to";
                        };
                    };
                  }
                );
              };
          sync =
            mkOption {
              default = null;
              type =
                types.nullOr (
                  types.submodule {
                    options =
                      {
                        enable = mkEnableOption "Donna syncing";
                        server-url =
                          mkOption {
                            type = types.str;
                            example = "api.donna.cs-syd.eu";
                            description = "The url of the sync server";
                          };
                        username =
                          mkOption {
                            type = types.str;
                            example = "syd";
                            description =
                              "The username to use when logging into the sync server";
                          };
                        password =
                          mkOption {
                            type = types.str;
                            example = "hunter12";
                            description =
                              "The password to use when logging into the sync server";
                          };
                      };
                  }
                );
            };

        };
    };
  config =
    let
      donnaPkgs = (import ./pkgs.nix).donnaPackages;

      backupDonnaName = "backup-donna";
      backupDonnaService =
        {
          Unit =
            {
              Description = "Backup donna";
            };
          Service =
            {
              ExecStart =
                "${pkgs.writeShellScript "backup-donna-service-ExecStart"
                  ''
                    export PATH="$PATH:${pkgs.coreutils}/bin:${pkgs.gnutar}/bin:${pkgs.gzip}/bin"
                    set -ex
                    backupdir="$HOME/${cfg.backup.backupDir}"
                    mkdir -p "''${backupdir}"
                    tmpfile="''${backupdir}/''$(date +%F_%T).db"
                    backupfile="''${backupdir}/''$(date +%F_%T).tar.gz"
                    ${pkgs.sqlite}/bin/sqlite3 ~/.donna/data.sqlite ".backup ''${tmpfile}"
                    tar -cvzf "''${backupfile}" "''${tmpfile}"
                    rm "''${tmpfile}"
                  ''}";
              Type = "oneshot";
            };
        };
      backupDonnaTimer =
        {
          Unit =
            {
              Description = "Backup donna every day";
            };
          Install =
            {
              WantedBy = [ "timers.target" ];
            };
          Timer =
            {
              OnCalendar = "*-*-* 00:00";
              Persistent = true;
              Unit = "${backupDonnaName}.service";
            };
          };

      syncConfigContents = optionalString (cfg.sync.enable or false) ''

          url: "${cfg.sync.server-url}"
          username: "${cfg.sync.username}"
          password: "${cfg.sync.password}"
          sync-strategy: NeverSync

      '';

      donnaConfigContents =
        concatStringsSep "\n" [
          syncConfigContents
          cfg.extraConfig
        ];

      syncDonnaName = "sync-donna";
      syncDonnaService =
              {
          Unit =
            {
              Description = "Sync donna";
              Wants = [ "network-online.target" ];
            };
          Service =
            {
              ExecStart =
                "${pkgs.writeShellScript "sync-donna-service-ExecStart"
                  ''
                    exec ${donnaPkgs.donna-cli}/bin/donna sync
                  ''}";
              Type = "oneshot";
            };
        };
      syncDonnaTimer =
        {
          Unit =
            {
              Description = "Sync donna every five minutes for";
            };
          Install =
            {
              WantedBy = [ "timers.target" ];
            };
          Timer =
            {
              OnCalendar = "*:0/5";
              Persistent = true;
              Unit = "${syncDonnaName}.service";
            };
        };



      services =
        (
          optionalAttrs (cfg.backup.enable or false) {
            "${backupDonnaName}" = backupDonnaService;
          } //
          optionalAttrs (cfg.sync.enable or false) {
            "${syncDonnaName}" = syncDonnaService;
          }
        );
      timers =
        (
          optionalAttrs (cfg.backup.enable or false) {
            "${backupDonnaName}" = backupDonnaTimer;
          } //
          optionalAttrs (cfg.sync.enable or false) {
            "${syncDonnaName}" = syncDonnaTimer;
          }
        );
      packages =
        [
          donnaPkgs.donna-cli
        ];


    in
      mkIf cfg.enable {
        xdg.configFile."donna/config.yaml".text = donnaConfigContents;
        systemd.user =
          {
            startServices = true;
            services = services;
            timers = timers;
          };
        home.packages = packages;
      };
}
