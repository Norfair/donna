{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE OverloadedStrings #-}

module Donna.CLI.Commands.Login where

import Donna.CLI.Commands.Import

commandLogin :: D ()
commandLogin = withSyncEnv $ \cenv -> withLogin cenv $ \_ ->liftIO $ putStrLn "Succes"
