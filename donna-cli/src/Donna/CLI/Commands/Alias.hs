{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE NoImplicitPrelude #-}

module Donna.CLI.Commands.Alias
  ( commandAlias,
  )
where

import Donna.CLI.Commands.Import
import Donna.CLI.Query

commandAlias :: Alias -> Maybe Alias -> D ()
commandAlias a = \case
  Nothing -> void $ runDB $ getOrCreateAlias a
  Just a' -> void $ runDB $ createAliasFor a' a
