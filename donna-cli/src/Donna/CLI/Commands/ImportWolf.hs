{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE NoImplicitPrelude #-}

module Donna.CLI.Commands.ImportWolf
  ( commandImportWolf,
  )
where

import Data.Aeson
import qualified Data.Map as M
import qualified Data.Set as S
import Donna.CLI.Commands.Import

commandImportWolf :: Path Abs File -> D ()
commandImportWolf d = do
  repo <- liftIO $ loadWolfRepo d
  importRepo repo

data WolfRepo
  = WolfRepo
      { wolfIndex :: Map Alias PersonUuid,
        wolfNotes :: Map NoteUuid WolfNote,
        wolfEntries :: Map PersonUuid WolfEntry
      }
  deriving (Show, Eq)

instance FromJSON WolfRepo where
  parseJSON =
    withObject "WolfRepo" $ \o -> do
      wolfIndex <- o .: "person-index"
      notes <- o .: "notes"
      noteIndices <- o .: "note-indices"
      let personUuids = S.fromList $ map snd $ M.toList wolfIndex
      let wolfNotes =
            flip M.mapWithKey notes $ \nuuid note ->
              let mentioningPeople =
                    flip mapMaybe (M.toList noteIndices) $ \(puuid, nuuids) ->
                      if nuuid `S.member` nuuids && puuid `S.member` personUuids
                        then Just puuid
                        else Nothing
               in note
                    { wolfNotePeople = S.union (S.fromList mentioningPeople) $ wolfNotePeople note
                    }
      entries <- o .: "person-entries"
      let wolfEntries = M.filterWithKey (\k _ -> k `S.member` personUuids) entries
      pure WolfRepo {..}

data WolfNote
  = WolfNote
      { wolfNoteContents :: Text,
        wolfNotePeople :: Set PersonUuid,
        wolfNoteTimestamp :: UTCTime
      }
  deriving (Show, Eq)

instance FromJSON WolfNote where
  parseJSON =
    withObject "WolfNote" $ \o ->
      WolfNote <$> o .: "contents" <*> (S.fromList <$> (o .:? "relevant-people" .!= []))
        <*> o .: "timestamp"

data WolfEntry
  = WolfEntry WolfEntryVal
  deriving (Show, Eq, Ord)

instance FromJSON WolfEntry where
  parseJSON = withObject "WolfEntry" $ \o -> WolfEntry <$> o .: "properties"

data WolfEntryVal
  = WPVal WolfEntryValue
  | WPList [WolfEntryVal]
  | WPMap [(Text, WolfEntryVal)]
  deriving (Show, Eq, Ord)

instance FromJSON WolfEntryVal where
  parseJSON o = (WPVal <$> parseJSON o) <|> (WPList <$> parseJSON o) <|> (WPMap <$> parseJSON o)

data WolfEntryValue
  = WolfEntryValue
      { wolfEntryValueContents :: Text,
        wolfEntryValueLastUpdated :: UTCTime
      }
  deriving (Show, Eq, Ord)

instance FromJSON WolfEntryValue where
  parseJSON =
    withObject "WolfEntryValue" $ \o -> WolfEntryValue <$> o .: "value" <*> o .: "last-updated"

loadWolfRepo :: Path Abs File -> IO WolfRepo
loadWolfRepo = decodeOrDie

decodeOrDie :: FromJSON a => Path Abs File -> IO a
decodeOrDie f = do
  errOrRes <- eitherDecodeFileStrict' $ fromAbsFile f
  case errOrRes of
    Left err -> die $ unlines ["Error while decoding", fromAbsFile f, err]
    Right r -> pure r

importRepo :: WolfRepo -> D ()
importRepo WolfRepo {..} = do
  now <- liftIO getCurrentTime
  runDB $ do
    forM_ (M.toList wolfIndex) $ \(alias, puuid) ->
      insertUnique $
        PersonAlias
          { personAliasName = alias,
            personAliasPerson = puuid,
            personAliasCreated = now,
            personAliasDeletedLocally = False,
            personAliasServerId = Nothing
          }
    forM_ (sortOn (wolfNoteTimestamp . snd) $ M.toList wolfNotes) $ \(nuuid, WolfNote {..}) -> do
      void
        $ insertUnique
        $ Note
          { noteUuid = nuuid,
            noteContents = wolfNoteContents,
            noteCreated = wolfNoteTimestamp,
            noteDeletedLocally = False,
            noteServerId = Nothing
          }
      forM_ (S.toList wolfNotePeople) $ \p ->
        insertUnique $ NoteRelevancy {noteRelevancyNote = nuuid, noteRelevancyPerson = p}
    forM_ (M.toList wolfEntries) $ \(puuid, we) -> do
      insertUnique $
        Entry
          { entryPerson = puuid,
            entryContents = convertToDonnaEntry we,
            entryCreated = now,
            entryChangedLocally = False,
            entryDeletedLocally = False,
            entryServerId = Nothing,
            entryServerTime = Nothing
          }

convertToDonnaEntry :: WolfEntry -> PersonEntry
convertToDonnaEntry (WolfEntry wp) = PersonEntry $ go wp
  where
    go we =
      WithLastUpdated (maxLastUpdated we) $
        case we of
          WPVal WolfEntryValue {..} -> PVal wolfEntryValueContents
          WPList wev -> PList $ map go wev
          WPMap wevs -> PMap $ map (second go) wevs

maxLastUpdated :: WolfEntryVal -> UTCTime
maxLastUpdated we =
  case we of
    WPVal wev -> wolfEntryValueLastUpdated wev
    WPList wevs -> maximum $ map maxLastUpdated wevs
    WPMap wevs -> maximum $ map (maxLastUpdated . snd) wevs
