{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE OverloadedStrings #-}

module Donna.CLI.Commands.Register where

import Donna.CLI.Commands.Import

import Servant.API

commandRegister :: D ()
commandRegister =
  withSyncEnv $ \cenv -> do
    un <- promptUsername
    pw <- promptPassword
    let reg = Register un pw
    NoContent <- runClientOrError cenv $ clientPostRegister reg
    pure ()
