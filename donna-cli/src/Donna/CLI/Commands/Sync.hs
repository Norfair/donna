{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeFamilies #-}

module Donna.CLI.Commands.Sync
  ( commandSync,
  )
where

import qualified Data.Map as M
import qualified Data.Mergeful as Mergeful
import qualified Data.Mergeful.Persistent as Mergeful
import qualified Data.Mergeless as Mergeless
import qualified Data.Mergeless.Persistent as Mergeless
import qualified Data.Set as S
import Donna.CLI.Commands.Import
import qualified Donna.Server.Data as Server

commandSync :: D ()
commandSync =
  withSyncEnv $ \cenv ->
    withLogin cenv $ \t -> runDB $ do
      sreq <- makeSyncRequest
      sresp <- lift $ runClientOrError cenv $ clientPostSync t sreq
      mergeSyncResponse sresp

makeSyncRequest :: MonadIO m => SqlPersistT m ClientSyncRequest
makeSyncRequest = do
  csyncRequestAliases <- makePersonAliasSyncRequest
  csyncRequestNotes <- makeNoteSyncRequest
  csyncRequestBlobs <- makeBlobSyncRequest
  csyncRequestEntries <- makeEntrySyncRequest
  pure ClientSyncRequest {..}

makePersonAliasSyncRequest :: MonadIO m => SqlPersistT m (Mergeless.SyncRequest PersonAliasId Server.PersonAliasId AddedAlias)
makePersonAliasSyncRequest = Mergeless.clientMakeSyncRequestQuery load PersonAliasServerId PersonAliasDeletedLocally
  where
    load :: PersonAlias -> AddedAlias
    load PersonAlias {..} =
      AddedAlias
        { addedAliasName = personAliasName,
          addedAliasPerson = personAliasPerson,
          addedAliasCreated = personAliasCreated
        }

makeEntrySyncRequest :: MonadIO m => SqlPersistT m (Mergeful.SyncRequest EntryId Server.EntryId AddedEntry)
makeEntrySyncRequest = Mergeful.clientMakeSyncRequestQuery EntryServerId EntryServerTime EntryChangedLocally EntryDeletedLocally loadUnsynced loadSynced loadDeleted
  where
    loadUnsynced :: Entry -> AddedEntry
    loadUnsynced Entry {..} =
      AddedEntry
        { addedEntryCreated = entryCreated,
          addedEntryPerson = entryPerson,
          addedEntryContents = entryContents
        }
    loadSynced :: Entry -> (Server.EntryId, Mergeful.Timed AddedEntry)
    loadSynced Entry {..} =
      ( fromJust entryServerId,
        Mergeful.Timed
          { Mergeful.timedValue = AddedEntry {addedEntryCreated = entryCreated, addedEntryPerson = entryPerson, addedEntryContents = entryContents},
            Mergeful.timedTime = fromJust entryServerTime
          }
      )
    loadDeleted :: Entry -> (Server.EntryId, Mergeful.ServerTime)
    loadDeleted Entry {..} =
      ( fromJust entryServerId,
        fromJust entryServerTime
      )

makeNoteSyncRequest :: MonadIO m => SqlPersistT m (Mergeless.SyncRequest NoteId Server.NoteId AddedNote)
makeNoteSyncRequest = do
  addedNotes <- selectList [NoteServerId ==. Nothing, NoteDeletedLocally ==. False] []
  syncRequestAdded <- fmap M.fromList $ forM addedNotes $ \(Entity cid Note {..}) -> do
    nrs <- S.fromList . map (noteRelevancyPerson . entityVal) <$> selectList [NoteRelevancyNote ==. noteUuid] []
    pure (cid, AddedNote {addedNoteUuid = noteUuid, addedNoteCreated = noteCreated, addedNoteContents = noteContents, addedNoteRelevantPeople = nrs})
  syncRequestSynced <- S.fromList . mapMaybe (noteServerId . entityVal) <$> selectList [NoteServerId !=. Nothing, NoteDeletedLocally ==. False] []
  syncRequestDeleted <- S.fromList . mapMaybe (noteServerId . entityVal) <$> selectList [NoteServerId !=. Nothing, NoteDeletedLocally ==. True] []
  pure Mergeless.SyncRequest {..}

makeBlobSyncRequest :: MonadIO m => SqlPersistT m (Mergeless.SyncRequest BlobId Server.BlobId AddedBlob)
makeBlobSyncRequest = do
  addedBlobs <- selectList [BlobServerId ==. Nothing, BlobDeletedLocally ==. False] []
  syncRequestAdded <- fmap M.fromList $ forM addedBlobs $ \(Entity cid Blob {..}) -> do
    nrs <- S.fromList . map (blobRelevancyPerson . entityVal) <$> selectList [BlobRelevancyBlob ==. blobUuid] []
    pure (cid, AddedBlob {addedBlobUuid = blobUuid, addedBlobCreated = blobCreated, addedBlobName = blobName, addedBlobFormat = blobFormat, addedBlobContents = blobContents, addedBlobRelevantPeople = nrs})
  syncRequestSynced <- S.fromList . mapMaybe (blobServerId . entityVal) <$> selectList [BlobServerId !=. Nothing, BlobDeletedLocally ==. False] []
  syncRequestDeleted <- S.fromList . mapMaybe (blobServerId . entityVal) <$> selectList [BlobServerId !=. Nothing, BlobDeletedLocally ==. True] []
  pure Mergeless.SyncRequest {..}

mergeSyncResponse :: MonadIO m => ServerSyncResponse -> SqlPersistT m ()
mergeSyncResponse ServerSyncResponse {..} = do
  mergeAliasSyncresponse ssyncResponseAliases
  mergeNoteSyncresponse ssyncResponseNotes
  mergeBlobSyncresponse ssyncResponseBlobs
  mergeEntriesSyncresponse ssyncResponseEntries

mergeAliasSyncresponse ::
  MonadIO m =>
  Mergeless.SyncResponse PersonAliasId Server.PersonAliasId AddedAlias ->
  SqlPersistT m ()
mergeAliasSyncresponse = Mergeless.clientMergeSyncResponseQuery create PersonAliasServerId PersonAliasDeletedLocally
  where
    create :: Server.PersonAliasId -> AddedAlias -> PersonAlias
    create sid AddedAlias {..} =
      PersonAlias
        { personAliasName = addedAliasName,
          personAliasPerson = addedAliasPerson,
          personAliasCreated = addedAliasCreated,
          personAliasServerId = Just sid,
          personAliasDeletedLocally = False
        }

mergeNoteSyncresponse ::
  forall m.
  MonadIO m =>
  Mergeless.SyncResponse NoteId Server.NoteId AddedNote ->
  SqlPersistT m ()
mergeNoteSyncresponse = Mergeless.mergeSyncResponseCustom proc
  where
    proc :: Mergeless.ClientSyncProcessor NoteId Server.NoteId AddedNote (SqlPersistT m)
    proc =
      Mergeless.ClientSyncProcessor
        { Mergeless.clientSyncProcessorSyncServerAdded = \m -> forM_ (M.toList m) $ \(sid, AddedNote {..}) -> do
            insert_
              Note
                { noteUuid = addedNoteUuid,
                  noteContents = addedNoteContents,
                  noteCreated = addedNoteCreated,
                  noteServerId = Just sid,
                  noteDeletedLocally = False
                }
            forM_ (S.toList addedNoteRelevantPeople) $ \puuid ->
              insert_ NoteRelevancy {noteRelevancyNote = addedNoteUuid, noteRelevancyPerson = puuid},
          Mergeless.clientSyncProcessorSyncClientAdded = \m -> forM_ (M.toList m) $ \(cid, sid) ->
            update cid [NoteServerId =. Just sid],
          Mergeless.clientSyncProcessorSyncServerDeleted = deletes,
          Mergeless.clientSyncProcessorSyncClientDeleted = deletes
        }
    deletes s =
      forM_ (S.toList s) $ \sid -> do
        mn <- getBy (UniqueNoteServerId $ Just sid)
        forM_ mn $ \(Entity nid Note {..}) -> do
          deleteWhere [NoteRelevancyNote ==. noteUuid]
          delete nid

mergeBlobSyncresponse ::
  forall m.
  MonadIO m =>
  Mergeless.SyncResponse BlobId Server.BlobId AddedBlob ->
  SqlPersistT m ()
mergeBlobSyncresponse = Mergeless.mergeSyncResponseCustom proc
  where
    proc :: Mergeless.ClientSyncProcessor BlobId Server.BlobId AddedBlob (SqlPersistT m)
    proc =
      Mergeless.ClientSyncProcessor
        { Mergeless.clientSyncProcessorSyncServerAdded = \m -> forM_ (M.toList m) $ \(sid, AddedBlob {..}) -> do
            insert_
              Blob
                { blobUuid = addedBlobUuid,
                  blobName = addedBlobName,
                  blobFormat = addedBlobFormat,
                  blobContents = addedBlobContents,
                  blobCreated = addedBlobCreated,
                  blobServerId = Just sid,
                  blobDeletedLocally = False
                }
            forM_ (S.toList addedBlobRelevantPeople) $ \puuid ->
              insert_ BlobRelevancy {blobRelevancyBlob = addedBlobUuid, blobRelevancyPerson = puuid},
          Mergeless.clientSyncProcessorSyncClientAdded = \m -> forM_ (M.toList m) $ \(cid, sid) ->
            update cid [BlobServerId =. Just sid],
          Mergeless.clientSyncProcessorSyncServerDeleted = deletes,
          Mergeless.clientSyncProcessorSyncClientDeleted = deletes
        }
    deletes s =
      forM_ (S.toList s) $ \sid -> do
        mn <- getBy (UniqueBlobServerId $ Just sid)
        forM_ mn $ \(Entity nid Blob {..}) -> do
          deleteWhere [BlobRelevancyBlob ==. blobUuid]
          delete nid

mergeEntriesSyncresponse ::
  MonadIO m => Mergeful.SyncResponse EntryId Server.EntryId AddedEntry -> SqlPersistT m ()
mergeEntriesSyncresponse = Mergeful.clientMergeSyncResponseQuery EntryServerId EntryServerTime EntryChangedLocally EntryDeletedLocally saveSynced loadSynced updateSynced Mergeful.mergeFromServerStrategy
  where
    saveSynced :: Server.EntryId -> Mergeful.Timed AddedEntry -> Entry
    saveSynced sid (Mergeful.Timed AddedEntry {..} st) =
      Entry
        { entryPerson = addedEntryPerson,
          entryContents = addedEntryContents,
          entryCreated = addedEntryCreated,
          entryServerId = Just sid,
          entryServerTime = Just st,
          entryChangedLocally = False,
          entryDeletedLocally = False
        }
    loadSynced :: Entry -> (Server.EntryId, Mergeful.Timed AddedEntry)
    loadSynced Entry {..} =
      ( fromJust entryServerId,
        Mergeful.Timed
          { Mergeful.timedValue =
              AddedEntry
                { addedEntryPerson = entryPerson,
                  addedEntryContents = entryContents,
                  addedEntryCreated = entryCreated
                },
            Mergeful.timedTime = fromJust entryServerTime
          }
      )
    updateSynced :: AddedEntry -> [Update Entry]
    updateSynced AddedEntry {..} = [EntryContents =. addedEntryContents] -- We don't update the created or the person.
