{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE OverloadedStrings #-}

module Donna.CLI.Commands.Blob where

import Donna.CLI.Commands.Import

import qualified Data.ByteString as SB
import qualified Data.Text as T
import qualified System.FilePath as FP

import Donna.CLI.Query

commandBlob :: FilePath -> Maybe BlobName -> Maybe BlobFormat -> Set Alias -> D ()
commandBlob fp mbn mbf people = do
  bn <-
    case mbn of
      Nothing ->
        case blobNameOrError $ T.pack fp of
          Left e -> liftIO $ die e
          Right bn -> pure bn
      Just bn -> pure bn
  let bf = fromMaybe (guessBlobFormat fp) mbf
  void $
    runDB $ do
      uuids <- getOrCreateAliases people
      contents <- liftIO $ SB.readFile fp
      createBlob uuids bn bf contents

guessBlobFormat :: FilePath -> BlobFormat
guessBlobFormat fp =
  case FP.takeExtensions fp of
    '.':ext -> case ext of
      "txt" -> FormatTxt
      "png" -> FormatPng
      "jpg" -> FormatJpg
      e -> FormatUnknown $ T.pack e
    e -> FormatUnknown $ T.pack e

