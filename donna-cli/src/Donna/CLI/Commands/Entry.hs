{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE OverloadedStrings #-}

module Donna.CLI.Commands.Entry where

import Donna.CLI.Commands.Import

import qualified Data.ByteString as SB
import qualified Data.Text as T

import Donna.CLI.Editor
import Donna.CLI.Query

commandEntry :: Alias -> D ()
commandEntry a =
  runDB $ do
    uuid <- getOrCreateAlias a
    tmpFile <- liftIO $ tmpPersonEntryFile uuid
    mpe <- getPersonEntry uuid
    (origPersonEntry, inFilePersonEntry) <-
      case mpe of
        Nothing -> do
          now <- liftIO getCurrentTime
          let blank = newPersonEntry now
          pure $
            (,) blank $
            fromMaybe blank $ do
              (fn, ln) <- parseFirstnameLastname a
              personEntry $
                WithLastUpdated now $
                PMap
                  [ ("first name", WithLastUpdated now $ PVal fn)
                  , ("last name", WithLastUpdated now $ PVal ln)
                  ]
        Just pe -> pure (pe, pe)
    liftIO $ SB.writeFile (toFilePath tmpFile) $ entryContentsBS inFilePersonEntry
    editResult <- startEditorOn tmpFile
    case editResult of
      EditingFailure reason ->
        liftIO $
        die $ unwords ["ERROR: failed to edit the entry file:", show reason, ". Not saving."]
      EditingSuccess -> do
        contents <- liftIO $ SB.readFile $ toFilePath tmpFile
        now <- liftIO getCurrentTime
        case updatePersonEntry now origPersonEntry contents of
          UpdateParseFailure ype ->
            liftIO $ die $ "Unable to parse entry file:\n" ++ prettyPrintEntryParseException ype
          UpdateValidityFailure ->
            liftIO $ die "Unable to reconstruct entry after update: Invalid entry."
          UpdateUnchanged -> liftIO $ putStrLn "Entry was not changed."
          UpdateSuccess pe -> void $ createOrUpdatePersonEntry uuid now pe
          UpdateWasDeletion -> liftIO $ putStrLn "Empty entry will not be saved."

parseFirstnameLastname :: Alias -> Maybe (Text, Text)
parseFirstnameLastname s =
  case T.words $ aliasText s of
    [fn, ln] -> Just (fn, ln)
    _ -> Nothing

tmpPersonEntryFile :: PersonUuid -> IO (Path Abs File)
tmpPersonEntryFile personUuid = do
  td <- liftIO getTempDir
  liftIO $ resolveFile td $ T.unpack (uuidText personUuid) ++ "-entry.yaml"
