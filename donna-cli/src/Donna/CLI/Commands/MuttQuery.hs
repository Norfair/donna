{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE OverloadedStrings #-}

module Donna.CLI.Commands.MuttQuery where

import Donna.CLI.Commands.Import

import qualified Data.Text as T
import qualified Data.Text.IO as T

import Donna.CLI.Query
import Donna.Data.Baked

commandMuttQuery :: SearchQuery -> D ()
commandMuttQuery t = do
  results <-
    runDB $ do
      aliases <- map entityVal <$> selectList [] []
      fmap concat $
        forM aliases $ \pa -> do
          let a = personAliasName pa
          if matchesAlias t a
            then do
              mpe <- getPersonEntry $ personAliasPerson pa
              pure $ searchResultsFor a mpe
            else pure []
  liftIO $ T.putStr $ formatSearchResults results

type SearchQuery = Text

data SearchResult =
  SearchResult
    { searchResultEmailAddress :: Text
    , searchResultLongName :: Text
    , searchResultOtherInfo :: Text
    }
  deriving (Show, Eq, Generic)

instance Validity SearchResult

searchResultsFor :: Alias -> Maybe PersonEntry -> [SearchResult]
searchResultsFor _ Nothing = []
searchResultsFor a (Just pe) = do
  EmailAddressWithPurpose mpur ea <- fromEntry pe :: [EmailAddressWithPurpose]
  pure
    SearchResult
      { searchResultEmailAddress = ea
      , searchResultLongName = aliasText a
      , searchResultOtherInfo = fromMaybe "" mpur
      }

matchesAlias :: SearchQuery -> Alias -> Bool
matchesAlias q = matchesText q . aliasText

matchesText :: SearchQuery -> Text -> Bool
matchesText q t = T.toCaseFold q `T.isInfixOf` T.toCaseFold t

-- According to the docs:
--
-- > It is the program executed when calling the function <query> and <complete-query>, that has to take as argument a string, representing the search string, and has to print a textual output consisting of lines of the format:
-- > `<email address> <tab> <long name> <tab> <other info> <newline>`
formatSearchResult :: SearchResult -> Text
formatSearchResult SearchResult {..} =
  T.concat [searchResultEmailAddress, "\t", searchResultLongName, "\t", searchResultOtherInfo, "\n"]

formatSearchResults :: [SearchResult] -> Text
formatSearchResults results = T.concat $ "\n" : map formatSearchResult results
    -- This newline was not in the spec but still necessary for some reason.
