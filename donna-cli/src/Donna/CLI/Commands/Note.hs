{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE OverloadedStrings #-}

module Donna.CLI.Commands.Note where

import Donna.CLI.Commands.Import

import qualified Data.Text.IO as T

import Donna.CLI.Editor
import Donna.CLI.Query

commandNote :: Set Alias -> D ()
commandNote people = do
  uuids <- runDB $ getOrCreateAliases people
  tnf <- liftIO tmpNoteFile
  liftIO $ ignoringAbsence $ removeFile tnf
  editingResult <- startEditorOn tnf
  case editingResult of
    EditingFailure reason ->
      liftIO $
      putStrLn $ unwords ["ERROR: failed to edit the note file:", show reason, ". Not saving."]
    EditingSuccess -> do
      contents <- liftIO $ T.readFile $ toFilePath tnf
      void $ runDB $ createNote uuids contents

tmpNoteFile :: IO (Path Abs File)
tmpNoteFile = do
  tmpDir <- getTempDir
  uuid <- nextRandomUUID
  resolveFile tmpDir $ uuidString uuid ++ ".txt"
