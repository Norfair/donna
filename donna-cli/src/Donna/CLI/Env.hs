{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE NoImplicitPrelude #-}

module Donna.CLI.Env where

import Control.Monad.Logger
import qualified Data.Text as T
import qualified Database.Persist.Sqlite as DB
import Donna.CLI.Data
import Donna.CLI.Import
import Donna.CLI.OptParse.Types (Settings (..), SyncStrategy (..))
import Donna.CLI.Prompt
import Donna.CLI.Query
import Donna.Client
import Lens.Micro
import Network.HTTP.Client as Http
import Network.HTTP.Client.TLS as Http

type D = ReaderT Env IO

data Env
  = Env
      { envConnectionPool :: DB.ConnectionPool,
        envSyncClientEnv :: Maybe ClientEnv,
        envUsername :: Maybe Username,
        envPassword :: Maybe Text,
        envSyncStrategy :: SyncStrategy
      }

runD :: Env -> D a -> IO a
runD = flip runReaderT

withEnvD :: Settings -> D a -> IO a
withEnvD s func = withEnv s $ runReaderT func

withEnv :: Settings -> (Env -> IO a) -> IO a
withEnv Settings {..} func = do
  mcenv <-
    forM setBaseUrl $ \burl -> do
      man <- newManager tlsManagerSettings
      pure $ mkClientEnv man burl
  withEnvRaw setDBFile mcenv setUsername setPassword setSyncStrategy func

withEnvRaw ::
  Path Abs File -> Maybe ClientEnv -> Maybe Username -> Maybe Text -> SyncStrategy -> (Env -> IO a) -> IO a
withEnvRaw dbPath mcenv mun mpw ss func =
  runNoLoggingT $ do
    DB.withSqlitePoolInfo
      ( DB.mkSqliteConnectionInfo (T.pack $ toFilePath dbPath) & DB.walEnabled .~ False
          & DB.fkEnabled .~ False
      )
      1
      $ \pool ->
        let env =
              Env
                { envConnectionPool = pool,
                  envSyncClientEnv = mcenv,
                  envUsername = mun,
                  envPassword = mpw,
                  envSyncStrategy = ss
                }
         in NoLoggingT
              $ flip runReaderT env
              $ do
                ensureDir $ parent dbPath
                setupDB
                liftIO $ func env

setupDB :: D ()
setupDB = runDB $ void $ DB.runMigrationSilent migrateAll

runDB :: DB.SqlPersistT D a -> D a
runDB func = do
  pool <- asks envConnectionPool
  DB.runSqlPool func pool

withLogin :: ClientEnv -> (Token -> D a) -> D a
withLogin cenv func = do
  mt <- runDB loadToken
  case mt of
    Just t -> func t
    Nothing -> do
      un <- promptUsername
      pw <- promptPassword
      let lf = Login un pw
      loginOrError cenv lf $ \session -> do
        runDB $ saveSession session
        let t = sessionToToken session
        func t

withLoginUnprompted :: ClientEnv -> Login -> (Token -> D a) -> D a
withLoginUnprompted cenv lf func = do
  mt <- runDB loadToken
  case mt of
    Just t -> func t
    Nothing -> do
      loginOrError cenv lf $ \session -> do
        runDB $ saveSession session
        let t = sessionToToken session
        func t

loginOrError :: MonadIO m => ClientEnv -> Login -> (SetCookie -> m b) -> m b
loginOrError cenv lf func = do
  errOrSession <- liftIO $ runClient cenv $ clientLoginSession lf
  case errOrSession of
    Left le -> liftIO $ die $ "Header Error while logging in: " <> show le
    Right (Left le) -> liftIO $ die $ "Servant Error while logging in: " <> show le
    Right (Right session) -> func session

promptUsername :: D Username
promptUsername = do
  msun <- asks envUsername
  case msun of
    Nothing -> liftIO $ promptUntil "username" parseUsername
    Just un -> pure un

promptPassword :: D Text
promptPassword = do
  mpw <- asks envPassword
  case mpw of
    Nothing -> liftIO $ promptSecret "password"
    Just pw -> pure pw

withSyncEnv :: (ClientEnv -> D a) -> D a
withSyncEnv func = do
  mCenv <- asks envSyncClientEnv
  case mCenv of
    Nothing -> liftIO $ die "No sync url configured."
    Just cenv -> func cenv

runClientOrError :: ClientEnv -> ClientM a -> D a
runClientOrError cenv func = do
  errOrResp <- liftIO $ runClient cenv func
  case errOrResp of
    Left err -> liftIO $ die $ show err
    Right res -> pure res
