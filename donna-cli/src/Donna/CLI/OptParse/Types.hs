{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE NoImplicitPrelude #-}

module Donna.CLI.OptParse.Types where

import Data.Yaml
import Donna.CLI.Data
import Donna.CLI.Import

data Instructions
  = Instructions Dispatch Settings
  deriving (Show, Eq, Generic)

data Dispatch
  = DispatchAlias Alias (Maybe Alias)
  | DispatchNote (Set Alias)
  | DispatchBlob FilePath (Maybe BlobName) (Maybe BlobFormat) (Set Alias)
  | DispatchEntry Alias
  | DispatchRegister
  | DispatchLogin
  | DispatchSync
  | DispatchMuttQuery Text
  | DispatchImportWolf (Path Abs File)
  deriving (Show, Eq, Generic)

data Settings
  = Settings
      { setDBFile :: !(Path Abs File),
        setBaseUrl :: !(Maybe BaseUrl),
        setUsername :: !(Maybe Username),
        setPassword :: !(Maybe Text),
        setSyncStrategy :: !SyncStrategy
      }
  deriving (Show, Eq, Generic)

data Arguments
  = Arguments Command Flags

data Command
  = CommandAlias [Text] (Maybe Alias)
  | CommandNote AliasesFlags
  | CommandBlob FilePath (Maybe BlobName) (Maybe BlobFormat) AliasesFlags
  | CommandEntry [Text]
  | CommandRegister
  | CommandLogin
  | CommandSync
  | CommandMuttQuery Text
  | CommandImportWolf FilePath
  deriving (Show, Eq, Generic)

data AliasesFlags
  = AliasesFlags [Text] (Set Alias)
  deriving (Show, Eq, Generic)

data Flags
  = Flags
      { flagConfigFile :: !(Maybe FilePath),
        flagDBFile :: !(Maybe FilePath),
        flagUrl :: !(Maybe String),
        flagUsername :: !(Maybe Username),
        flagPassword :: !(Maybe Text),
        flagSyncStrategy :: !(Maybe SyncStrategy)
      }
  deriving (Show, Eq, Generic)

data Environment
  = Environment
      { envConfigFile :: !(Maybe FilePath),
        envDBFile :: !(Maybe FilePath),
        envUrl :: !(Maybe String),
        envUsername :: !(Maybe Username),
        envPassword :: !(Maybe Text),
        envSyncStrategy :: !(Maybe SyncStrategy)
      }
  deriving (Show, Eq, Generic)

data Configuration
  = Configuration
      { confDBFile :: !(Maybe FilePath),
        confUrl :: !(Maybe String),
        confUsername :: !(Maybe Username),
        confPassword :: !(Maybe Text),
        confSyncStrategy :: !(Maybe SyncStrategy)
      }
  deriving (Show, Eq, Generic)

instance FromJSON Configuration where
  parseJSON =
    withObject "Configuration" $ \o ->
      Configuration <$> o .:? "db-file" <*> o .:? "url" <*> o .:? "username" <*> o .:? "password" <*> o .:? "sync-strategy"

data SyncStrategy
  = NeverSync
  | AlwaysSync
  deriving (Show, Read, Eq, Generic)

instance FromJSON SyncStrategy
