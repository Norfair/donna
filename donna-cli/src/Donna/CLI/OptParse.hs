{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE NoImplicitPrelude #-}

module Donna.CLI.OptParse
  ( getInstructions,
    Instructions (..),
    Dispatch (..),
    Settings (..),
  )
where

import qualified Data.ByteString as SB
import qualified Data.Set as S
import qualified Data.Text as T
import Data.Yaml as Yaml (decodeThrow)
import Donna.CLI.Data.DB
import Donna.CLI.Env
import Donna.CLI.Import
import Donna.CLI.OptParse.Types
import Options.Applicative
import qualified System.Environment as System (getArgs, getEnvironment)
import Text.Read

getInstructions :: IO Instructions
getInstructions = do
  args <- getArguments
  env <- getEnvironment
  mc <- getConfiguration args env
  buildInstructions args env mc

buildInstructions :: Arguments -> Environment -> Maybe Configuration -> IO Instructions
buildInstructions (Arguments cmd Flags {..}) Environment {..} mc =
  Instructions <$> disp <*> settings
  where
    mconf func = mc >>= func
    disp =
      case cmd of
        CommandAlias ts ma -> do
          a <- parseAliasWithSpaces ts
          pure $ DispatchAlias a ma
        CommandNote afs -> DispatchNote <$> resolveAliasesFlags afs
        CommandBlob fp mbn mbf afs -> DispatchBlob fp mbn mbf <$> resolveAliasesFlags afs
        CommandEntry ts -> do
          a <- parseAliasWithSpaces ts
          pure $ DispatchEntry a
        CommandRegister -> pure DispatchRegister
        CommandLogin -> pure DispatchLogin
        CommandSync -> pure DispatchSync
        CommandMuttQuery t -> pure $ DispatchMuttQuery t
        CommandImportWolf fp -> DispatchImportWolf <$> resolveFile' fp
    settings = do
      setDBFile <-
        case flagDBFile <|> envDBFile <|> mconf confDBFile of
          Nothing -> getDefaultDBFile
          Just fp -> resolveFile' fp
      setBaseUrl <- forM (flagUrl <|> envUrl <|> mconf confUrl) parseBaseUrl
      let setUsername = flagUsername <|> envUsername <|> mconf confUsername
          setPassword = flagPassword <|> envPassword <|> mconf confPassword
          setSyncStrategy =
            fromMaybe AlwaysSync $ flagSyncStrategy <|> envSyncStrategy <|> mconf confSyncStrategy
      pure Settings {..}

resolveAliasesFlags :: AliasesFlags -> IO (Set Alias)
resolveAliasesFlags (AliasesFlags ts as) = do
  a <- parseAliasWithSpaces ts
  pure $ S.insert a as

parseAliasWithSpaces :: [Text] -> IO Alias
parseAliasWithSpaces ts =
  case aliasOrError $ T.unwords ts of
    Left e -> die $ "Invalid alias: " <> e
    Right a -> pure a

getDefaultDBFile :: IO (Path Abs File)
getDefaultDBFile = do
  home <- getHomeDir
  resolveFile home ".donna/data.sqlite"

getConfiguration :: Arguments -> Environment -> IO (Maybe Configuration)
getConfiguration (Arguments _ Flags {..}) Environment {..} = do
  f <-
    case flagConfigFile <|> envConfigFile of
      Just fp -> resolveFile' fp
      Nothing -> getDefaultConfigFile
  mContents <- forgivingAbsence $ SB.readFile $ fromAbsFile f
  forM mContents Yaml.decodeThrow

getDefaultConfigFile :: IO (Path Abs File)
getDefaultConfigFile = do
  xdgConfigDir <- getXdgDir XdgConfig (Just [reldir|donna|])
  resolveFile xdgConfigDir "config.yaml"

getArguments :: IO Arguments
getArguments = do
  args <- System.getArgs
  let result = serveArgumentsParser args
  handleParseResult result

serveArgumentsParser :: [String] -> ParserResult Arguments
serveArgumentsParser =
  execParserPure
    ParserPrefs
      { prefMultiSuffix = "",
        prefDisambiguate = True,
        prefShowHelpOnError = True,
        prefShowHelpOnEmpty = True,
        prefBacktrack = True,
        prefColumns = 80
      }
    argParser

argParser :: ParserInfo Arguments
argParser = info (helper <*> parseArgs) (fullDesc <> progDesc "Donna")

parseArgs :: Parser Arguments
parseArgs = Arguments <$> parseCommand <*> parseFlags

parseCommand :: Parser Command
parseCommand =
  hsubparser $
    mconcat
      [ command "alias" parseCommandAlias,
        command "note" parseCommandNote,
        command "blob" parseCommandBlob,
        command "entry" parseCommandEntry,
        command "register" parseCommandRegister,
        command "login" parseCommandLogin,
        command "sync" parseCommandSync,
        command "mutt-query" parseCommandMuttQuery,
        command "import-wolf" parseCommandImportWolf
      ]

parseCommandAlias :: ParserInfo Command
parseCommandAlias = info parser modifier
  where
    parser =
      CommandAlias <$> some (strArgument (mconcat [metavar "ALIAS", help "The alias to create"]))
        <*> option
          (Just <$> eitherReader (aliasOrError . T.pack))
          ( mconcat
              [ short 'p',
                long "person",
                metavar "PERSON",
                help "The rest of the aliases",
                value Nothing,
                completer aliasCompletion
              ]
          )
    modifier = fullDesc <> progDesc "Create an alias"

parseCommandNote :: ParserInfo Command
parseCommandNote = info parser modifier
  where
    parser = CommandNote <$> parseAliasesFlags
    modifier = fullDesc <> progDesc "Make a note"

parseCommandBlob :: ParserInfo Command
parseCommandBlob = info parser modifier
  where
    parser =
      CommandBlob <$> strArgument (mconcat [metavar "FILEPATH", help "The blob to add"])
        <*> option
          (Just <$> eitherReader (blobNameOrError . T.pack))
          ( mconcat
              [short 'n', long "name", metavar "NAME", help "The name of the blob", value Nothing]
          )
        <*> option
          ((Just . blobFormatFromText) <$> str)
          ( mconcat
              [ short 'f',
                long "format",
                metavar "FORMAT",
                help
                  "The format to use for the blob, this will override the default guess that is based on the filename",
                value Nothing
              ]
          )
        <*> parseAliasesFlags
    modifier = fullDesc <> progDesc "Add a blob"

parseCommandEntry :: ParserInfo Command
parseCommandEntry = info parser modifier
  where
    parser =
      CommandEntry
        <$> some
          ( strArgument
              (mconcat [metavar "ALIAS", help "The alias to create", completer aliasCompletion])
          )
    modifier = fullDesc <> progDesc "Edit a person's entry"

parseCommandRegister :: ParserInfo Command
parseCommandRegister = info parser modifier
  where
    parser = pure CommandRegister
    modifier = fullDesc <> progDesc "Register a user at a donna server"

parseCommandLogin :: ParserInfo Command
parseCommandLogin = info parser modifier
  where
    parser = pure CommandLogin
    modifier = fullDesc <> progDesc "Login as a user at a donna server"

parseCommandSync :: ParserInfo Command
parseCommandSync = info parser modifier
  where
    parser = pure CommandSync
    modifier = fullDesc <> progDesc "Synchronise with a donna server"

parseCommandMuttQuery :: ParserInfo Command
parseCommandMuttQuery = info parser modifier
  where
    parser =
      CommandMuttQuery <$> strArgument (mconcat [metavar "QUERY", help "The name to search for"])
    modifier = fullDesc <> progDesc "A helper command to implement mutt-query"

parseCommandImportWolf :: ParserInfo Command
parseCommandImportWolf = info parser modifier
  where
    parser =
      CommandImportWolf
        <$> strArgument (mconcat [metavar "FILE", help "The wolf repository json file to import"])
    modifier = fullDesc <> progDesc "Import a wolf repository"

parseAliasesFlags :: Parser AliasesFlags
parseAliasesFlags =
  AliasesFlags
    <$> some (strArgument (mconcat [metavar "ALIAS", help "The first alias", completer aliasCompletion]))
    <*> ( S.fromList
            <$> many
              ( option
                  (eitherReader $ aliasOrError . T.pack)
                  ( mconcat
                      [ short 'p',
                        long "person",
                        metavar "PERSON",
                        help "The rest of the aliases",
                        completer aliasCompletion
                      ]
                  )
              )
        )

parseFlags :: Parser Flags
parseFlags =
  Flags
    <$> option
      (Just <$> str)
      ( mconcat
          [long "config", metavar "CONFIG_FILE", help "The configuration file to use", value Nothing]
      )
    <*> option
      (Just <$> str)
      (mconcat [long "database", metavar "DB_FILE", help "The database file to use", value Nothing])
    <*> option
      (Just <$> str)
      ( mconcat
          [ long "sync-server-url",
            metavar "URL",
            help "The url of the sync server to use",
            value Nothing
          ]
      )
    <*> option
      (Just <$> eitherReader (parseUsernameWithError . T.pack))
      ( mconcat
          [long "username", metavar "USERNAME", help "The username to use for syncing", value Nothing]
      )
    <*> option
      (Just <$> str)
      ( mconcat
          [long "password", metavar "PASSWORD", help "The password to use for syncing", value Nothing]
      )
    <*> syncStrategyOpt

syncStrategyOpt :: Parser (Maybe SyncStrategy)
syncStrategyOpt =
  flag Nothing (Just NeverSync) (mconcat [long "no-sync", help "Do not try to sync."])
    <|> flag Nothing (Just AlwaysSync) (mconcat [long "sync", help "Definitely try to sync."])

getEnvironment :: IO Environment
getEnvironment = do
  env <- System.getEnvironment
  let v :: IsString s => String -> Maybe s
      v k = fromString <$> lookup ("DONNA_" <> k) env
  let r :: Read a => String -> Maybe a
      r k = v k >>= readMaybe
  pure
    Environment
      { envConfigFile = v "CONFIG_FILE",
        envDBFile = v "DB" <|> v "DB_FILE" <|> v "DATABASE" <|> v "DATABASE_FILE",
        envUrl = v "URL",
        envUsername = v "USERNAME" >>= parseUsername,
        envPassword = T.pack <$> v "USERNAME",
        envSyncStrategy = r "SYNC_STRATEGY"
      }

aliasCompletion :: Completer
aliasCompletion = mkCompleter go
  where
    go :: String -> IO [String]
    go s = do
      dbFile <- getDefaultDBFile
      withEnvD
        Settings
          { setDBFile = dbFile,
            setBaseUrl = Nothing,
            setUsername = Nothing,
            setPassword = Nothing,
            setSyncStrategy = NeverSync
          }
        $ do
          pas <- runDB $ selectList [] []
          pure
            $ map T.unpack
            $ filter ((T.pack s `T.isPrefixOf`))
            $ map (aliasText . personAliasName . entityVal) pas
