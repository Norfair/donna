{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE NoImplicitPrelude #-}

module Donna.CLI.Query where

import qualified Data.Set as S
import Donna.CLI.Data

createAliasFor :: MonadIO m => Alias -> Alias -> SqlPersistT m (Maybe PersonUuid)
createAliasFor src dest = do
  mpa <- getBy $ UniqueAlias src
  forM mpa $ \(Entity _ PersonAlias {..}) -> do
    now <- liftIO getCurrentTime
    void
      $ insertUnique
      $ PersonAlias
        { personAliasName = dest,
          personAliasPerson = personAliasPerson,
          personAliasCreated = now,
          personAliasDeletedLocally = False,
          personAliasServerId = Nothing
        }
    pure personAliasPerson

getOrCreateAlias :: MonadIO m => Alias -> SqlPersistT m PersonUuid
getOrCreateAlias a = do
  mpa <- getBy $ UniqueAlias a
  case mpa of
    Nothing -> do
      uuid <- nextRandomUUID
      now <- liftIO getCurrentTime
      void
        $ insertUnique
        $ PersonAlias
          { personAliasName = a,
            personAliasPerson = uuid,
            personAliasCreated = now,
            personAliasDeletedLocally = False,
            personAliasServerId = Nothing
          }
      pure uuid
    Just (Entity _ pa) -> pure $ personAliasPerson pa

getOrCreateAliases :: MonadIO m => Set Alias -> SqlPersistT m (Set PersonUuid)
getOrCreateAliases = fmap S.fromList . mapM getOrCreateAlias . S.toList

createNote :: MonadIO m => Set PersonUuid -> Text -> SqlPersistT m NoteUuid
createNote ps c = do
  uuid <- nextRandomUUID
  now <- liftIO getCurrentTime
  void $
    insert
      Note
        { noteUuid = uuid,
          noteContents = c,
          noteCreated = now,
          noteDeletedLocally = False,
          noteServerId = Nothing
        }
  void
    $ insertMany
    $ flip map (S.toList ps)
    $ \p -> NoteRelevancy {noteRelevancyNote = uuid, noteRelevancyPerson = p}
  pure uuid

createBlob ::
  MonadIO m => Set PersonUuid -> BlobName -> BlobFormat -> ByteString -> SqlPersistT m BlobUuid
createBlob ps bn bf c = do
  uuid <- nextRandomUUID
  now <- liftIO getCurrentTime
  void $
    insert
      Blob
        { blobUuid = uuid,
          blobName = bn,
          blobFormat = bf,
          blobContents = c,
          blobCreated = now,
          blobDeletedLocally = False,
          blobServerId = Nothing
        }
  void
    $ insertMany
    $ flip map (S.toList ps)
    $ \p -> BlobRelevancy {blobRelevancyBlob = uuid, blobRelevancyPerson = p}
  pure uuid

getPersonEntry :: MonadIO m => PersonUuid -> SqlPersistT m (Maybe PersonEntry)
getPersonEntry uuid = fmap (entryContents . entityVal) <$> getBy (UniqueEntry uuid)

createOrUpdatePersonEntry ::
  MonadIO m => PersonUuid -> UTCTime -> PersonEntry -> SqlPersistT m (Entity Entry)
createOrUpdatePersonEntry uuid now pe =
  upsertBy
    (UniqueEntry uuid)
    (Entry {entryPerson = uuid, entryContents = pe, entryCreated = now, entryServerId = Nothing, entryServerTime = Nothing, entryChangedLocally = False, entryDeletedLocally = False})
    [EntryContents =. pe]

loadToken :: MonadIO m => SqlPersistT m (Maybe Token)
loadToken = do
  mCookie <- loadSession
  pure $ Token . setCookieValue <$> mCookie

loadSession :: MonadIO m => SqlPersistT m (Maybe SetCookie)
loadSession = fmap (loginTokenSession . entityVal) <$> getBy (UniqueLoginToken 0)

saveSession :: MonadIO m => SetCookie -> SqlPersistT m ()
saveSession setCookie =
  void $
    upsertBy
      (UniqueLoginToken 0)
      (LoginToken {loginTokenUnit = 0, loginTokenSession = setCookie})
      [LoginTokenSession =. setCookie]
