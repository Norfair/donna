{-# LANGUAGE NoImplicitPrelude #-}

module Donna.CLI
  ( donnaCLI,
  )
where

import Donna.CLI.Commands
import Donna.CLI.OptParse

donnaCLI :: IO ()
donnaCLI = do
  Instructions d s <- getInstructions
  withEnvD s $
    case d of
      DispatchAlias t ma -> commandAlias t ma
      DispatchNote as -> commandNote as
      DispatchBlob fp bn bf as -> commandBlob fp bn bf as
      DispatchEntry a -> commandEntry a
      DispatchRegister -> commandRegister
      DispatchLogin -> commandLogin
      DispatchSync -> commandSync
      DispatchMuttQuery q -> commandMuttQuery q
      DispatchImportWolf f -> commandImportWolf f
