# Donna

Donna is a Personal Relationship Management (PRM) system.

Its goal is to help people be better with people.

## Features

- [x] Data
  - [x] Aliases: A human-rememberable way to refer to people
  - [x] Entries: Long-term information about people
  - [x] Notes: Notes about people
  - [x] Edges: Edges between people, with metadata
  - [x] Blobs: Photos, documents and other files about people
- [x] Data format: sqlite
  - [x] Portable
  - [x] Future-proof
- [ ] Self-learning
  - [ ] Importing
    - [ ] Email
    - [ ] Google
      - [ ] Contacts
      - [ ] Calendar
    - [ ] Facebook
      - [ ] Friends
      - [ ] Events
    - [ ] Linkedin
    - [ ] Slack
    - [ ] Whatsapp
    - [ ] Telegram
    - [ ] Github
- [ ] Works Offline
  - [x] Sync server
  - [x] Local Syncing
    - [x] Automated syncing strategy
    - [ ] Manual conflict resolution
- [ ] Actionable results
  - [ ] Google calendar integration:
        Whenever you're going to meet someone, Donna can automatically find the event in google calendar, and add a note with a link so you can read up on the people that you are meeting with, just before the meeting.
