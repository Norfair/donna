{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}

module Donna.CLI.Data.DB
  ( module Donna.CLI.Data.DB,
    module Donna.Data,
    SqlBackend,
    SqlPersistT,
    module Database.Persist,
  )
where

import qualified Data.Mergeful as Mergeful
import Database.Persist
import Database.Persist.Sql
import Database.Persist.TH
import Donna.Data
import qualified Donna.Server.Data as Server

share
  [mkPersist sqlSettings, mkMigrate "migrateAll"]
  [persistLowerCase|

PersonAlias
    name Alias
    UniqueAlias name
    person PersonUuid sqltype=uuid
    created UTCTime
    serverId Server.PersonAliasId Maybe
    deletedLocally Bool
    deriving Show
    deriving Eq
    deriving Generic

Entry
    person PersonUuid sqltype=uuid
    UniqueEntry person
    contents PersonEntry
    created UTCTime
    serverId Server.EntryId  Maybe
    serverTime Mergeful.ServerTime Maybe
    changedLocally Bool
    deletedLocally Bool
    deriving Show
    deriving Eq
    deriving Generic

Note
    uuid NoteUuid sqltype=uuid
    UniqueNote uuid
    contents Text
    created UTCTime
    serverId Server.NoteId Maybe
    UniqueNoteServerId serverId !force
    deletedLocally Bool
    deriving Show
    deriving Eq
    deriving Generic

NoteRelevancy
    note NoteUuid sqltype=uuid
    person PersonUuid sqltype=uuid
    UniqueNoteRelevancy note person
    deriving Show
    deriving Eq
    deriving Generic

Blob
    uuid BlobUuid sqltype=uuid
    UniqueBlob uuid
    name BlobName
    format BlobFormat
    contents ByteString
    created UTCTime
    serverId Server.BlobId Maybe
    UniqueBlobServerId serverId !force
    deletedLocally Bool
    deriving Show
    deriving Eq
    deriving Generic

BlobRelevancy
    blob BlobUuid sqltype=uuid
    person PersonUuid sqltype=uuid
    UniqueBlobRelevancy blob person
    deriving Show
    deriving Eq
    deriving Generic

LoginToken
    unit Int -- Always 0
    session SetCookie
    UniqueLoginToken unit

|]

instance Validity PersonAlias

instance Validity Entry

instance Validity Note

instance Validity NoteRelevancy

instance Validity Blob

instance Validity BlobRelevancy
