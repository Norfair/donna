{-# OPTIONS_GHC -fno-warn-unused-imports #-}

module Donna.Data.BakedGen
  ( module X
  ) where

import Donna.Data.Baked.EmailAddressGen as X
import Donna.Data.Baked.Import as X
import Donna.Data.Baked.PhoneNumberGen as X
