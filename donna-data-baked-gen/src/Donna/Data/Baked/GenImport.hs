{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE UndecidableInstances #-}
{-# OPTIONS -fno-warn-orphans #-}

module Donna.Data.Baked.GenImport
  ( module X
  ) where
import Donna.Data.Baked.Import as X

import Donna.Data.Baked as X
import Donna.Data.GenImport as X
