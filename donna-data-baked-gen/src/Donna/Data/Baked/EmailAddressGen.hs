{-# LANGUAGE NoImplicitPrelude #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}

module Donna.Data.Baked.EmailAddressGen where

import Donna.Data.Baked.GenImport

instance GenValid EmailAddress where
  genValid = genValidStructurally
  shrinkValid = shrinkValidStructurally
