{-# LANGUAGE NoImplicitPrelude #-}

{-# OPTIONS_GHC -fno-warn-orphans #-}
module Donna.Data.Baked.PhoneNumberGen where

import Donna.Data.Baked.GenImport

instance GenValid PhoneNumber where
  genValid = genValidStructurally
  shrinkValid = shrinkValidStructurally
