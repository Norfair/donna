{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE TypeApplications #-}

module Donna.Data.Baked.EmailAddressSpec
  ( spec
  ) where

import Donna.Data.Baked.TestImport

spec :: Spec
spec = do
  describe "EmailAddressSpec" $ do
    genValidSpec @EmailAddress
    eqSpecOnValid @EmailAddress
    ordSpecOnValid @EmailAddress
    jsonSpecOnValid @EmailAddress
  describe "emailAddress" $ it "produces valid phone numbers" $ producesValidsOnValids emailAddress
