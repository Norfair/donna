{-# LANGUAGE GADTs #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE LiberalTypeSynonyms #-}
{-# LANGUAGE RankNTypes #-}

module Donna.Data.Baked.TestImport
  ( module Donna.Data.Baked.TestImport
  , module X
  ) where

import Donna.Data.Baked.GenImport as X

import Test.Hspec as X
import Test.Validity as X
import Test.Validity.Aeson as X
import Test.Validity.Persist as X

import Donna.Data.BakedGen as X ()

failure :: String -> IO a
failure s = do
  expectationFailure s
  undefined -- Won't get here anyway
