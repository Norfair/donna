{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE TypeApplications #-}

module Donna.Data.Baked.PhoneNumberSpec
  ( spec
  ) where

import Donna.Data.Baked.TestImport

spec :: Spec
spec = do
  describe "PhoneNumberSpec" $ do
    genValidSpec @PhoneNumber
    eqSpecOnValid @PhoneNumber
    ordSpecOnValid @PhoneNumber
    jsonSpecOnValid @PhoneNumber
  describe "phoneNumber" $ it "produces valid phone numbers" $ producesValidsOnValids phoneNumber
