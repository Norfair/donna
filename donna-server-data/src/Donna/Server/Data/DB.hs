{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}

module Donna.Server.Data.DB
  ( module Donna.Server.Data.DB,
    module Donna.Data,
    SqlPersistT,
    module Database.Persist,
  )
where

import Data.Mergeful.Persistent ()
import qualified Data.Mergeful.Timed as Mergeful
import Database.Persist
import Database.Persist.Sql
import Database.Persist.TH
import Donna.Data

share
  [mkPersist sqlSettings, mkMigrate "migrateAll"]
  [persistLowerCase|

User
    username Username
    hashedPassword HashedPassword
    created UTCTime

    UniqueUsername username

    deriving Show
    deriving Eq
    deriving Generic


PersonAlias
    user UserId
    name Alias
    UniqueAlias user name
    person PersonUuid sqltype=uuid
    created UTCTime
    deriving Show
    deriving Eq
    deriving Generic

Entry
    user UserId
    person PersonUuid sqltype=uuid
    UniqueEntry user person
    contents PersonEntry
    created UTCTime
    serverTime Mergeful.ServerTime
    deriving Show
    deriving Eq
    deriving Generic

Note -- TODO Make content addressable
    user UserId
    uuid NoteUuid sqltype=uuid
    UniqueNote user uuid
    contents Text
    created UTCTime
    deriving Show
    deriving Eq
    deriving Generic

NoteRelevancy
    user UserId
    note NoteUuid sqltype=uuid
    person PersonUuid sqltype=uuid
    UniqueNoteRelevancy user note person
    deriving Show
    deriving Eq
    deriving Generic

Blob -- TODO Make content addressable
    user UserId
    uuid BlobUuid sqltype=uuid
    UniqueBlob user uuid
    name BlobName
    format BlobFormat
    contents ByteString
    created UTCTime
    deriving Show
    deriving Eq
    deriving Generic

BlobRelevancy
    user UserId
    blob BlobUuid sqltype=uuid
    person PersonUuid sqltype=uuid
    UniqueBlobRelevancy user blob person
    deriving Show
    deriving Eq
    deriving Generic
|]

instance Validity PersonAlias

instance Validity Entry

instance Validity Note

instance Validity NoteRelevancy

instance Validity Blob

instance Validity BlobRelevancy
