{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TypeOperators #-}

module Donna.API
  ( module Donna.API
  , module Donna.API.Endpoint
  ) where

import Donna.API.Import

import Servant.API.Generic

import Donna.API.Endpoint

type DonnaAPI = ToServantApi DonnaAPIRoutes

donnaAPI :: Proxy DonnaAPI
donnaAPI = Proxy

data DonnaAPIRoutes route =
  DonnaAPIRoutes
    { unprotecedRoutes :: route :- DonnaUnprotectedAPI
    , protectedRoutes :: route :- DonnaProtectedAPI
    }
  deriving (Generic)

type DonnaUnprotectedAPI = ToServantApi DonnaUnprotectedRoutes

donnaUnprotectedAPI :: Proxy DonnaUnprotectedAPI
donnaUnprotectedAPI = Proxy

data DonnaUnprotectedRoutes route =
  DonnaUnprotectedRoutes
    { postRegister :: !(route :- PostRegister)
    , postLogin :: !(route :- PostLogin)
    }
  deriving (Generic)

type DonnaProtectedAPI = ToServantApi DonnaProtectedRoutes

donnaProtectedAPI :: Proxy DonnaProtectedAPI
donnaProtectedAPI = Proxy

data DonnaProtectedRoutes route =
  DonnaProtectedRoutes
    { getAliases :: !(route :- ProtectAPI :> GetAliases)
    , getPersonAliases :: !(route :- ProtectAPI :> GetPersonAliases)
    , postAlias :: !(route :- ProtectAPI :> PostAlias)
    , deleteAlias :: !(route :- ProtectAPI :> DeleteAlias)
    , deletePersonAlias :: !(route :- ProtectAPI :> DeletePersonAlias)
    , getPerson :: !(route :- ProtectAPI :> GetPerson)
    , postPersonWithAliases :: !(route :- ProtectAPI :> PostPersonWithAliases)
    , getPeopleProperties :: !(route :- ProtectAPI :> GetPeopleProperties)
    , postNote :: !(route :- ProtectAPI :> PostNote)
    , putEntry :: !(route :- ProtectAPI :> PutEntry)
    , postSync :: !(route :- ProtectAPI :> PostSync)
    , getGraph :: !(route :- ProtectAPI :> GetGraph)
    }
  deriving (Generic)
