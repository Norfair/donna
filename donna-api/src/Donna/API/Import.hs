{-# LANGUAGE NoImplicitPrelude #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}

module Donna.API.Import
  ( module X,
  )
where

import Data.Aeson as X
import Donna.Server.Data as X
import Servant.API as X
import Web.Cookie as X

instance Validity NoContent where
  validate = trivialValidation
