{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}

module Donna.API.Endpoint.Person where

import Donna.API.Import

type GetPersonNotes = "person" :> Capture "uuid" PersonUuid :> Get '[ JSON] (Set NoteUuid)

type GetPerson = "person" :> Capture "uuid" PersonUuid :> Get '[ JSON] PersonInfo

data PersonInfo =
  PersonInfo
    { personInfoAliases :: !(Set Alias)
    , personInfoEntry :: !(Maybe PersonEntry)
    , personInfoNotes :: ![PersonNote]
    }
  deriving (Show, Eq, Generic)

instance Validity PersonInfo

instance FromJSON PersonInfo where
  parseJSON =
    withObject "PersonInfo" $ \o -> PersonInfo <$> o .: "aliases" <*> o .: "entry" <*> o .: "notes"

instance ToJSON PersonInfo where
  toJSON PersonInfo {..} =
    object ["aliases" .= personInfoAliases, "entry" .= personInfoEntry, "notes" .= personInfoNotes]

data PersonNote =
  PersonNote
    { personNoteContents :: !Text
    , personNoteOtherRelevantPeople :: !(Map PersonUuid (Set Alias))
    , personNoteTimestamp :: !UTCTime
    }
  deriving (Show, Eq, Ord, Generic)

instance Validity PersonNote

instance FromJSON PersonNote where
  parseJSON =
    withObject "PersonNote" $ \o ->
      PersonNote <$> o .: "contents" <*> o .: "other-relevant-people" <*> o .: "timestamp"

instance ToJSON PersonNote where
  toJSON PersonNote {..} =
    object
      [ "contents" .= personNoteContents
      , "other-relevant-people" .= personNoteOtherRelevantPeople
      , "timestamp" .= personNoteTimestamp
      ]

type PostPersonWithAliases
   = "person" :> ReqBody '[ JSON] (Set Alias) :> Post '[ JSON] PersonUuid
