{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}

module Donna.API.Endpoint.People where

import Donna.API.Import

type GetPeopleProperties
   = "people" :> "properties" :> Get '[ JSON] (Map PersonUuid PersonPropertyInfo)

data PersonPropertyInfo =
  PersonPropertyInfo
    { personPropertyInfoAliases :: !(Set Alias)
    , personPropertyInfoEntry :: !(Maybe PersonEntry)
    }
  deriving (Show, Eq, Ord, Generic)

instance Validity PersonPropertyInfo

instance FromJSON PersonPropertyInfo where
  parseJSON =
    withObject "PersonPropertyInfo" $ \o -> PersonPropertyInfo <$> o .: "aliases" <*> o .: "entry"

instance ToJSON PersonPropertyInfo where
  toJSON PersonPropertyInfo {..} =
    object ["aliases" .= personPropertyInfoAliases, "entry" .= personPropertyInfoEntry]
