{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TypeOperators #-}

module Donna.API.Endpoint.Graph where

import Donna.API.Import

type GetGraph = "graph" :> Get '[ JSON] SocialGraph

data SocialGraph =
  SocialGraph
    { socialGraphPeople :: Map PersonUuid Alias
    , socialGraphNotes :: Map NoteUuid (Set PersonUuid)
    }
  deriving (Show, Eq, Generic)

instance Validity SocialGraph

instance FromJSON SocialGraph where
  parseJSON = withObject "SocialGraph" $ \o -> SocialGraph <$> o .: "people" <*> o .: "notes"

instance ToJSON SocialGraph where
  toJSON SocialGraph {..} = object ["people" .= socialGraphPeople, "notes" .= socialGraphNotes]
