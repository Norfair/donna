{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TypeOperators #-}

module Donna.API.Endpoint.Note where

import Donna.API.Import

type GetNote = "note" :> Capture "note" NoteUuid :> Get '[ JSON] NoteInfo

data NoteInfo =
  NoteInfo
    { noteInfoContents :: !Text
    , noteInfoRelevantPeople :: !(Set PersonUuid)
    , noteInfoTimestamp :: !UTCTime
    }
  deriving (Show, Eq, Ord, Generic)

instance Validity NoteInfo

instance FromJSON NoteInfo where
  parseJSON =
    withObject "NoteInfo" $ \o ->
      NoteInfo <$> o .: "contents" <*> o .: "relevant-people" <*> o .: "timestamp"

instance ToJSON NoteInfo where
  toJSON NoteInfo {..} =
    object
      [ "contents" .= noteInfoContents
      , "relevant-people" .= noteInfoRelevantPeople
      , "timestamp" .= noteInfoTimestamp
      ]

type PostNote = "note" :> ReqBody '[ JSON] NewNote :> PostNoContent '[ JSON] NoContent

data NewNote =
  NewNote
    { newNoteRelevantPeople :: Set Alias
    , newNoteContents :: Text
    }
  deriving (Show, Eq, Generic)

instance Validity NewNote

instance FromJSON NewNote where
  parseJSON = withObject "NewNote" $ \o -> NewNote <$> o .: "relevant-people" <*> o .: "contents"

instance ToJSON NewNote where
  toJSON NewNote {..} =
    object ["relevant-people" .= newNoteRelevantPeople, "contents" .= newNoteContents]
