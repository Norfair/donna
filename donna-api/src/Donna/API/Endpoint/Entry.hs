{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TypeOperators #-}

module Donna.API.Endpoint.Entry where

import Donna.API.Import

type GetEntry = "entry" :> Capture "person" PersonUuid :> Get '[ JSON] (Maybe PersonEntry)

type PutEntry
   = "entry" :> Capture "person" PersonUuid :> ReqBody '[ JSON] PersonEntry :> Put '[ JSON] NoContent
