{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}

module Donna.API.Endpoint.Alias where

import Donna.API.Import

type GetAliases = "aliases" :> Get '[ JSON] (Map Alias PersonUuid)

type GetPersonAliases = "aliases" :> Capture "person" PersonUuid :> Get '[ JSON] (Set Alias)

type PostAlias
   = "alias" :> Capture "person" PersonUuid :> ReqBody '[ JSON] Alias :> PostNoContent '[ JSON] NoContent

type DeleteAlias = "alias" :> ReqBody '[ JSON] Alias :> Delete '[ JSON] NoContent

type DeletePersonAlias
   = "alias" :> Capture "person" PersonUuid :> ReqBody '[ JSON] Alias :> Delete '[ JSON] NoContent
