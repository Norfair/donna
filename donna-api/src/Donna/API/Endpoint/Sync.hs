{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE NoImplicitPrelude #-}

module Donna.API.Endpoint.Sync where

import Data.ByteString.Base64 as Base64
import qualified Data.Mergeful as Mergeful
import qualified Data.Mergeless as Mergeless
import Data.Text.Encoding as TE
import Donna.API.Import
import qualified Donna.CLI.Data as CLI
import Donna.Server.Data as Server

type PostSync = "sync" :> ReqBody '[JSON] ClientSyncRequest :> Post '[JSON] ServerSyncResponse

data ClientSyncRequest
  = ClientSyncRequest
      { csyncRequestAliases :: !(Mergeless.SyncRequest CLI.PersonAliasId Server.PersonAliasId AddedAlias),
        csyncRequestNotes :: !(Mergeless.SyncRequest CLI.NoteId Server.NoteId AddedNote),
        csyncRequestBlobs :: !(Mergeless.SyncRequest CLI.BlobId Server.BlobId AddedBlob),
        csyncRequestEntries :: !(Mergeful.SyncRequest CLI.EntryId Server.EntryId AddedEntry)
      }
  deriving (Show, Eq, Generic)

instance Validity ClientSyncRequest

instance FromJSON ClientSyncRequest where
  parseJSON =
    withObject "ClientSyncRequest" $ \o ->
      ClientSyncRequest <$> o .: "aliases" <*> o .: "notes" <*> o .: "blobs" <*> o .: "entries"

instance ToJSON ClientSyncRequest where
  toJSON ClientSyncRequest {..} =
    object
      [ "aliases" .= csyncRequestAliases,
        "notes" .= csyncRequestNotes,
        "blobs" .= csyncRequestBlobs,
        "entries" .= csyncRequestEntries
      ]

data ServerSyncResponse
  = ServerSyncResponse
      { ssyncResponseAliases :: !(Mergeless.SyncResponse CLI.PersonAliasId Server.PersonAliasId AddedAlias),
        ssyncResponseNotes :: !(Mergeless.SyncResponse CLI.NoteId Server.NoteId AddedNote),
        ssyncResponseBlobs :: !(Mergeless.SyncResponse CLI.BlobId Server.BlobId AddedBlob),
        ssyncResponseEntries :: !(Mergeful.SyncResponse CLI.EntryId Server.EntryId AddedEntry)
      }
  deriving (Show, Eq, Generic)

instance Validity ServerSyncResponse

instance FromJSON ServerSyncResponse where
  parseJSON =
    withObject "ServerSyncResponse" $ \o ->
      ServerSyncResponse <$> o .: "aliases" <*> o .: "notes" <*> o .: "blobs" <*> o .: "entries"

instance ToJSON ServerSyncResponse where
  toJSON ServerSyncResponse {..} =
    object
      [ "aliases" .= ssyncResponseAliases,
        "notes" .= ssyncResponseNotes,
        "blobs" .= ssyncResponseBlobs,
        "entries" .= ssyncResponseEntries
      ]

data ClientStore
  = ClientStore
      { cstoreAliases :: !(Mergeless.ClientStore CLI.PersonAliasId Server.PersonAliasId AddedAlias),
        cstoreNotes :: !(Mergeless.ClientStore CLI.NoteId Server.NoteId AddedNote),
        cstoreBlobs :: !(Mergeless.ClientStore CLI.BlobId Server.BlobId AddedBlob),
        cstoreEntries :: !(Mergeful.ClientStore CLI.EntryId Server.EntryId AddedEntry)
      }
  deriving (Show, Eq, Generic)

instance Validity ClientStore

data AddedAlias
  = AddedAlias
      { addedAliasName :: !Alias,
        addedAliasPerson :: !PersonUuid,
        addedAliasCreated :: !UTCTime
      }
  deriving (Show, Eq, Ord, Generic)

instance Validity AddedAlias

instance FromJSON AddedAlias where
  parseJSON = withObject "AddedAlias" $ \o -> AddedAlias <$> o .: "name" <*> o .: "person" <*> o .: "created"

instance ToJSON AddedAlias where
  toJSON AddedAlias {..} = object ["name" .= addedAliasName, "person" .= addedAliasPerson, "created" .= addedAliasCreated]

data AddedNote
  = AddedNote
      { addedNoteUuid :: !NoteUuid,
        addedNoteCreated :: !UTCTime,
        addedNoteContents :: !Text,
        addedNoteRelevantPeople :: !(Set PersonUuid)
      }
  deriving (Show, Eq, Ord, Generic)

instance Validity AddedNote

instance FromJSON AddedNote where
  parseJSON =
    withObject "AddedNote" $ \o ->
      AddedNote <$> o .: "uuid" <*> o .: "created" <*> o .: "contents" <*> o .: "relevant-people"

instance ToJSON AddedNote where
  toJSON AddedNote {..} =
    object
      [ "uuid" .= addedNoteUuid,
        "created" .= addedNoteCreated,
        "contents" .= addedNoteContents,
        "relevant-people" .= addedNoteRelevantPeople
      ]

data AddedBlob
  = AddedBlob
      { addedBlobUuid :: !BlobUuid,
        addedBlobCreated :: !UTCTime,
        addedBlobName :: !BlobName,
        addedBlobFormat :: !BlobFormat,
        addedBlobContents :: !ByteString,
        addedBlobRelevantPeople :: !(Set PersonUuid)
      }
  deriving (Show, Eq, Ord, Generic)

instance Validity AddedBlob

instance FromJSON AddedBlob where
  parseJSON =
    withObject "AddedBlob" $ \o ->
      AddedBlob <$> o .: "uuid" <*> o .: "created" <*> o .: "name" <*> o .: "format"
        <*> ( do
                t <- o .: "contents"
                case Base64.decode (TE.encodeUtf8 t) of
                  Left err -> fail err
                  Right bs -> pure bs
            )
        <*> o
        .: "relevant-people"

instance ToJSON AddedBlob where
  toJSON AddedBlob {..} =
    object
      [ "uuid" .= addedBlobUuid,
        "created" .= addedBlobCreated,
        "name" .= addedBlobName,
        "format" .= addedBlobFormat,
        "contents" .= TE.decodeUtf8 (Base64.encode addedBlobContents), -- Safe because it is base64
        "relevant-people" .= addedBlobRelevantPeople
      ]

data AddedEntry
  = AddedEntry
      { addedEntryCreated :: !UTCTime,
        addedEntryPerson :: !PersonUuid,
        addedEntryContents :: !PersonEntry
      }
  deriving (Show, Eq, Ord, Generic)

instance Validity AddedEntry

instance FromJSON AddedEntry where
  parseJSON = withObject "AddedEntry" $ \o -> AddedEntry <$> o .: "created" <*> o .: "person" <*> o .: "contents"

instance ToJSON AddedEntry where
  toJSON AddedEntry {..} = object ["created" .= addedEntryCreated, "person" .= addedEntryPerson, "contents" .= addedEntryContents]
