{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TypeOperators #-}

module Donna.API.Endpoint.Auth where

import Donna.API.Import

import Servant.Auth
import Servant.Auth.Server

type Protected = Auth '[ JWT]

type PostRegister = "register" :> ReqBody '[ JSON] Register :> PostNoContent '[ JSON] NoContent

data Register =
  Register
    { registerUsername :: Username
    , registerPassword :: Text
    }
  deriving (Show, Eq, Generic)

instance Validity Register

instance ToJSON Register

instance FromJSON Register

type PostLogin
   = "login" :> ReqBody '[ JSON] Login :> PostNoContent '[ JSON] (Headers '[ Header "Set-Cookie" SetCookie, Header "Set-Cookie" SetCookie] NoContent)

data Login =
  Login
    { loginUsername :: Username
    , loginPassword :: Text
    }
  deriving (Show, Eq, Generic)

instance Validity Login

instance ToJSON Login

instance FromJSON Login

type ProtectAPI = Auth '[ JWT] AuthCookie

newtype AuthCookie =
  AuthCookie
    { authCookieId :: UserId
    }
  deriving (Show, Eq, Ord, Generic)

instance FromJSON AuthCookie

instance ToJSON AuthCookie

instance FromJWT AuthCookie

instance ToJWT AuthCookie
