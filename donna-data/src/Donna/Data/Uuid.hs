{-# LANGUAGE OverloadedStrings #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}

module Donna.Data.Uuid
  ( module Data.UUID.Typed
  , NoteUuid
  , PersonUuid
  , BlobUuid
  ) where

import Donna.Data.Import

import qualified Data.ByteString.Lazy as LB
import qualified Data.Text as T
import qualified Data.UUID as Uuid
import Data.UUID.Typed

import Web.PathPieces

import Database.Persist
import Database.Persist.Sql

instance PersistField (UUID a) where
  toPersistValue (UUID uuid) = PersistByteString $ LB.toStrict $ Uuid.toByteString uuid
  fromPersistValue (PersistByteString bs) =
    case Uuid.fromByteString $ LB.fromStrict bs of
      Nothing -> Left "Invalidy Bytestring to convert to Uuid"
      Just uuid -> Right $ UUID uuid
  fromPersistValue pv = Left $ "Invalid Persist value to parse to Uuid: " <> T.pack (show pv)

instance PersistFieldSql (UUID a) where
  sqlType Proxy = SqlBlob

instance PathPiece (UUID a) where
  fromPathPiece = parseUUID
  toPathPiece = uuidText

data Person

type PersonUuid = UUID Person

data Note

type NoteUuid = UUID Note

data Blob

type BlobUuid = UUID Blob
