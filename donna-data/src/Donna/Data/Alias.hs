{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE ExistentialQuantification #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeFamilies #-}

module Donna.Data.Alias
  ( Alias()
  , parseAlias
  , aliasOrError
  , aliasText
  ) where

import Donna.Data.Import

import Data.Aeson as JSON
import qualified Data.Text as T
import Database.Persist.Sql

newtype Alias =
  Alias
    { aliasText :: Text
    }
  deriving ( Show
           , Eq
           , Ord
           , Generic
           , Hashable
           , PersistField
           , PersistFieldSql
           , FromJSONKey
           , FromJSON
           , ToJSONKey
           , ToJSON
           )

instance Validity Alias where
  validate a@(Alias t) =
    mconcat
      [ genericValidate a
      , declare "The alias does not contain newlines" $ not $ T.any (\c -> c == '\n' && c == '\r') t
      ]

parseAlias :: Text -> Maybe Alias
parseAlias = constructValid . Alias

aliasOrError :: Text -> Either String Alias
aliasOrError = prettyValidate . Alias
