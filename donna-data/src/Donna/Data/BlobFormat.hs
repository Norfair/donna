{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE ExistentialQuantification #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeFamilies #-}

module Donna.Data.BlobFormat where

import Donna.Data.Import

import Data.Aeson

import Database.Persist
import Database.Persist.Sql

data BlobFormat
  = FormatTxt
  | FormatPng
  | FormatJpg
  | FormatUnknown Text
  deriving (Show, Read, Eq, Ord, Generic)

instance Validity BlobFormat

instance PersistField BlobFormat where
  toPersistValue bf =
    PersistText $
    case bf of
      FormatTxt -> "txt"
      FormatPng -> "png"
      FormatJpg -> "jpg"
      FormatUnknown t -> t
  fromPersistValue (PersistText bs) =
    case bs of
      "txt" -> Right FormatTxt
      "png" -> Right FormatPng
      "jpg" -> Right FormatJpg
      t -> Right $ FormatUnknown t
  fromPersistValue _ = Left "Not a valid BlobFormat"

instance PersistFieldSql BlobFormat where
  sqlType Proxy = SqlString

instance FromJSON BlobFormat where
  parseJSON (String t) = pure $ blobFormatFromText t
  parseJSON _ = fail "Not a valid TextItem"

instance ToJSON BlobFormat where
  toJSON bf =
    String $
    case bf of
      FormatTxt -> "txt"
      FormatPng -> "png"
      FormatJpg -> "jpg"
      FormatUnknown t -> t

blobFormatFromText :: Text -> BlobFormat
blobFormatFromText t =
  case t of
    "txt" -> FormatTxt
    "png" -> FormatPng
    "jpg" -> FormatJpg
    _ -> FormatUnknown t
