{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE ExistentialQuantification #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeFamilies #-}

module Donna.Data.BlobName
  ( BlobName()
  , blobNameOrError
  , blobNameText
  ) where

import Donna.Data.Import

import Data.Aeson as JSON
import Database.Persist.Sql

newtype BlobName =
  BlobName
    { blobNameText :: Text
    }
  deriving ( Show
           , Eq
           , Ord
           , Generic
           , Hashable
           , PersistField
           , PersistFieldSql
           , FromJSONKey
           , FromJSON
           , ToJSONKey
           , ToJSON
           )

instance Validity BlobName

blobNameOrError :: Text -> Either String BlobName
blobNameOrError = prettyValidate . BlobName
