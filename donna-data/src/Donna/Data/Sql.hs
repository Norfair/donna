{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE UndecidableInstances #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}

module Donna.Data.Sql
  (
  )
where

import Data.Aeson
import Database.Persist
import Database.Persist.Sql
import Donna.Data.Import

instance Validity (Key a) where
  validate = trivialValidation

instance Validity (Entity a) where
  validate = trivialValidation

instance (PersistEntity a, ToBackendKey SqlBackend a) => ToJSONKey (Key a)

instance (PersistEntity a, ToBackendKey SqlBackend a) => FromJSONKey (Key a)
