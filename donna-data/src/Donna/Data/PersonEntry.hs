{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE ExistentialQuantification #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TypeFamilies #-}

module Donna.Data.PersonEntry
  ( PersonEntry(..)
  , PersonProperty
  , PersonPropertyVal(..)
  , WithLastUpdated(..)
  , newPersonEntry
  , personEntry
  , entryContentsText
  , entryContentsBS
  , updatePersonEntry
  , constructPersonEntry
  , reconstructPersonEntry
  , UpdateResult(..)
  , parsePersonEntry
  , parseEntryFileContents
  , prettyPrintEntryParseException
  , EntryParseException(..)
  , RawYaml(..)
  , ForEditor(..)
  ) where

import Donna.Data.Import

import Conduit
import Control.Exception
import Data.Aeson as JSON
import qualified Data.ByteString as SB
import qualified Data.HashMap.Strict as HM
import qualified Data.Text as T
import qualified Data.Text.Encoding as TE
import Data.Yaml as Yaml (ParseException(..), prettyPrintParseException)
import Data.Yaml.Builder as Yaml
import Data.Yaml.Parser as Yaml
import System.IO.Unsafe (unsafePerformIO)
import Text.Libyaml as Yaml

import Database.Persist
import Database.Persist.Sql

newtype PersonEntry =
  PersonEntry
    { personEntryProperties :: PersonProperty
    }
  deriving (Show, Eq, Ord, Generic)

instance Validity PersonEntry where
  validate pe@(PersonEntry prop) =
    mconcat [genericValidate pe, decorate "The last updated timestamps make sense" $ go prop]
    where
      go :: WithLastUpdated PersonPropertyVal -> Validation
      go (WithLastUpdated ts ppv) =
        case ppv of
          PVal _ -> decorate "PVal" valid
          PList pps -> decorate "PList" $ decorateList pps $ \pp -> go' ts pp
          PMap tups -> decorate "PMap" $ decorateList tups $ \(_, pp) -> go' ts pp
        where
          go' :: UTCTime -> WithLastUpdated PersonPropertyVal -> Validation
          go' upper wlu@(WithLastUpdated lower _) =
            mconcat
              [declare "The lower part is not older than the higher part" $ lower <= upper, go wlu]

instance PersistField PersonEntry where
  toPersistValue = toPersistValueJSON
  fromPersistValue = fromPersistValueJSON

instance PersistFieldSql PersonEntry where
  sqlType Proxy = SqlBlob

instance FromJSON PersonEntry where
  parseJSON v = PersonEntry <$> parseJSON v

instance ToJSON PersonEntry where
  toJSON (PersonEntry ps) = toJSON ps

type PersonProperty = WithLastUpdated PersonPropertyVal

-- NOTE: The order of these constructors is important!
data PersonPropertyVal
  = PVal Text
  | PList [PersonProperty]
  | PMap [(Text, PersonProperty)] -- Unique keys
  deriving (Show, Eq, Ord, Generic)

instance Validity PersonPropertyVal where
  validate ppv =
    mconcat
      [ genericValidate ppv
      , case ppv of
          PVal _ -> valid
          PList _ -> valid
          PMap tups ->
            declare "The keys in the map are distinct" $
            let keys = map fst tups
             in nub keys == keys
      ]

instance FromJSON PersonPropertyVal where
  parseJSON val =
    if val == Object HM.empty
      then pure $ PMap []
      else (PVal <$> parseJSON val) <|> (PList <$> parseJSON val) <|> (PMap <$> parseJSON val)

instance ToJSON PersonPropertyVal where
  toJSON pp =
    case pp of
      PMap [] -> (Object HM.empty)
      PMap list -> toJSON list
      PList list -> toJSON list
      PVal v -> JSON.String v

data WithLastUpdated a =
  WithLastUpdated
    { lastUpdatedTimestamp :: UTCTime
    , lastUpdatedValue :: a
    }
  deriving (Show, Eq, Ord, Generic, Functor)

instance Validity a => Validity (WithLastUpdated a)

instance FromJSON a => FromJSON (WithLastUpdated a) where
  parseJSON =
    withObject "WithLastUpdated" $ \o ->
      WithLastUpdated <$> o JSON..: "last-updated" <*> o JSON..: "value"

instance ToJSON a => ToJSON (WithLastUpdated a) where
  toJSON WithLastUpdated {..} =
    object ["last-updated" JSON..= lastUpdatedTimestamp, "value" JSON..= lastUpdatedValue]

newPersonEntry :: UTCTime -> PersonEntry
newPersonEntry now =
  PersonEntry
    { personEntryProperties =
        WithLastUpdated {lastUpdatedTimestamp = now, lastUpdatedValue = PMap []}
    }

personEntry :: PersonProperty -> Maybe PersonEntry
personEntry = constructValid . PersonEntry

entryContentsText :: PersonEntry -> Text
entryContentsText pe =
  case TE.decodeUtf8' $ entryContentsBS pe of
    Left err ->
      "Error: Unable to transform entry to text, decoding ByteString to Text failed: " <>
      T.pack (show err) -- Should never happen.
    Right t -> t

entryContentsBS :: PersonEntry -> ByteString
entryContentsBS = Yaml.toByteString . ForEditor

updatePersonEntry :: UTCTime -> PersonEntry -> ByteString -> UpdateResult PersonEntry
updatePersonEntry now old bs =
  if SB.null bs
    then UpdateWasDeletion
    else case parseEntryFileContents bs of
           Left ye -> UpdateParseFailure ye
           Right (ForEditor ry) ->
             case reconstructPersonEntry now old ry of
               Nothing -> UpdateValidityFailure
               Just pe ->
                 if pe == old
                   then UpdateUnchanged
                   else UpdateSuccess pe

data UpdateResult a
  = UpdateSuccess a
  | UpdateUnchanged
  | UpdateParseFailure EntryParseException
  | UpdateValidityFailure
  | UpdateWasDeletion
  deriving (Show, Generic)

instance Validity a => Validity (UpdateResult a) where
  validate (UpdateSuccess a) = decorate "UpdateSuccess" $ validate a
  validate UpdateUnchanged = valid
  validate (UpdateParseFailure epe) = trivialValidation epe
  validate UpdateValidityFailure = valid
  validate UpdateWasDeletion = valid

data EntryParseException
  = EntryYamlParseException YamlParseException
  | EntryYamlException YamlException
  | EntryParseException ParseException
  deriving (Show, Generic)

instance Validity EntryParseException where
  validate = trivialValidation -- No need to go deeper

prettyPrintEntryParseException :: EntryParseException -> String
prettyPrintEntryParseException (EntryYamlParseException ype) = show ype
prettyPrintEntryParseException (EntryYamlException ye) = show ye
prettyPrintEntryParseException (EntryParseException ye) = prettyPrintParseException ye

constructPersonEntry :: UTCTime -> RawYaml -> PersonEntry
constructPersonEntry now = PersonEntry . go
  where
    go :: RawYaml -> PersonProperty
    go ry =
      WithLastUpdated now $
      case ry of
        RVal t -> PVal t
        RList ns -> PList $ map go ns
        RMap tups -> PMap $ nubBy ((==) `on` fst) $ map (second go) tups

reconstructPersonEntry :: UTCTime -> PersonEntry -> RawYaml -> Maybe PersonEntry
reconstructPersonEntry now old new =
  let oldProp = personEntryProperties old
   in personEntry $ go oldProp new
  where
    fillWithNow :: RawYaml -> PersonProperty
    fillWithNow ry =
      WithLastUpdated now $
      case ry of
        RVal t -> PVal t
        RList ns -> PList $ map fillWithNow ns
        RMap tups -> PMap $ map (second fillWithNow) tups
    go :: PersonProperty -> RawYaml -> PersonProperty
    go (WithLastUpdated oldtime pval) ry =
      let unlessNewer newVal =
            WithLastUpdated
              (if pval == newVal
                 then oldtime
                 else now)
              newVal
       in case (pval, ry) of
            (PVal _, RVal vn) -> unlessNewer $ PVal vn
            (PList ol, RList nl) ->
              unlessNewer $ PList $ zipWith f (map Just ol ++ repeat Nothing) nl
              where f :: Maybe PersonProperty -> RawYaml -> PersonProperty
                    f mov nv =
                      case mov of
                        Nothing -> fillWithNow nv
                        Just ov -> go ov nv
            (PMap om, RMap nm) ->
              unlessNewer $ PMap $ map (\(k, v) -> (k, foo k v)) $ nubBy ((==) `on` fst) nm
              where foo :: Text -> RawYaml -> PersonProperty
                    foo key value =
                      case lookup key om of
                        Nothing -- Key did not exist before, therefore it was created here.
                         -> fillWithNow value
                        Just oldValue -- Key existed before, have to recurse.
                         -> go oldValue value
            (_, r) -> fillWithNow r

newtype ForEditor a =
  ForEditor
    { unForEditor :: a
    }
  deriving (Show, Eq, Generic)

instance Validity a => Validity (ForEditor a)

instance ToYaml (ForEditor PersonEntry) where
  toYaml (ForEditor pe) = go (personEntryProperties pe)
    where
      go :: PersonProperty -> YamlBuilder
      go (WithLastUpdated _ v) = goV v
      goV :: PersonPropertyVal -> YamlBuilder
      goV (PVal ppv) = Yaml.string ppv
      goV (PList ls) = Yaml.array $ map go ls
      goV (PMap tups) = Yaml.mapping $ map (second go) tups

parsePersonEntry :: UTCTime -> Text -> Either EntryParseException PersonEntry
parsePersonEntry now t =
  (constructPersonEntry now . unForEditor) <$> parseEntryFileContents (TE.encodeUtf8 t)

parseEntryFileContents :: ByteString -> Either EntryParseException (ForEditor RawYaml)
parseEntryFileContents bs =
  unsafePerformIO $
  let parse = do
        rawDoc <- runResourceT $ runConduit $ Yaml.decode bs .| Yaml.sinkRawDoc
        parseRawDoc rawDoc >>= (evaluate . force)
   in (Right `liftM` parse) `catches`
      [ Handler $ pure . Left . EntryYamlParseException
      , Handler $ pure . Left . EntryYamlException
      , Handler $ pure . Left . EntryParseException
      ]

{-# NOINLINE parseEntryFileContents #-}
data RawYaml
  = RVal Text
  | RList [RawYaml]
  | RMap [(Text, RawYaml)]
  deriving (Show, Eq, Generic)

instance Validity RawYaml

instance NFData RawYaml

instance NFData a => NFData (ForEditor a)

instance FromYaml (ForEditor RawYaml) where
  fromYaml yv = ForEditor <$> (parseRVal yv <|> parseRList yv <|> parseRMap yv)
    where
      parseRVal = Yaml.withText "RVal" (pure . RVal)
      parseRList =
        withSequence "RList" $ \vs ->
          fmap RList $
          forM vs $ \y -> do
            ForEditor ry <- fromYaml y
            pure ry
      parseRMap =
        withMapping "RMap" $ \tups ->
          fmap RMap $
          forM tups $ \(k, v) -> do
            (ForEditor ry) <- fromYaml v
            pure (k, ry)
