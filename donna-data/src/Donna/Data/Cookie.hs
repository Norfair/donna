{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE ExistentialQuantification #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TypeFamilies #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}

module Donna.Data.Cookie
  ( SetCookie(..)
  , Token(..)
  ) where

import Donna.Data.Import

import qualified Data.ByteString.Builder as SBB
import qualified Data.ByteString.Lazy as LB
import Database.Persist.Sql
import Servant.Auth.Client
import Web.Cookie

instance PersistField SetCookie where
  toPersistValue = toPersistValue . LB.toStrict . SBB.toLazyByteString . renderSetCookie
  fromPersistValue = fmap parseSetCookie . fromPersistValue

instance PersistFieldSql SetCookie where
  sqlType Proxy = SqlBlob
