#!/usr/bin/env bash

function front () {
  local f="$1"
  elm make "src/$f.elm" --output "out/$f.js"
}

front Note
front Search
front SocialGraph
