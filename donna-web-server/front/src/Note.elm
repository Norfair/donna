module Note exposing (main)

import Bootstrap.Badge as Badge
import Bootstrap.Button as Button
import Bootstrap.Form as Form
import Bootstrap.Form.Input as Input
import Bootstrap.Form.Select as Select
import Bootstrap.Form.Textarea as Textarea
import Bootstrap.Utilities.Spacing exposing (..)
import Browser exposing (..)
import Browser.Events
import Data.Alias exposing (..)
import Data.CSRF exposing (..)
import Data.Uuid exposing (..)
import Debug
import Dict exposing (Dict)
import Html exposing (Attribute, Html, div, text)
import Html.Attributes as Html exposing (class)
import Html.Events exposing (..)
import Json.Decode as Decode exposing (Decoder)
import Set exposing (Set)


main : Program Flags Model Msg
main =
    Browser.element
        { init = init
        , view = view
        , update = \msg model -> ( update msg model, Cmd.none )
        , subscriptions = subscriptions
        }


type alias Flags =
    { actionUrl : String
    , csrf : String
    , aliases : Decode.Value
    }


type alias Model =
    { actionUrl : String
    , csrf : String
    , contents : String
    , availableAliases : Set Alias
    , chosenSuggestions : Set Alias
    }


type Msg
    = ChangeContents String
    | ChooseSuggestion Alias
    | RemoveChosen Alias
    | TryToSelectUsingKeyboard
    | AddUnknownAlias
    | NoOp


init : Flags -> ( Model, Cmd Msg )
init flags =
    let
        available =
            Set.fromList
                (case Decode.decodeValue (Decode.dict Decode.string) flags.aliases of
                    Ok aliases ->
                        Dict.keys aliases

                    Err _ ->
                        []
                )

        m =
            { actionUrl = flags.actionUrl
            , csrf = flags.csrf
            , contents = ""
            , availableAliases = available
            , chosenSuggestions = Set.fromList []
            }
    in
    ( m, Cmd.none )


view : Model -> Html Msg
view model =
    Form.form
        [ Html.action model.actionUrl, Html.method "POST" ]
        [ Form.group []
            [ Form.label [] [ text "Find Relevant People" ]
            , Input.text
                [ Input.placeholder "Look for a person"
                , Input.value model.contents
                , Input.onInput ChangeContents
                , Input.attrs
                    [ onSearchKey
                    , Html.autofocus True
                    ]
                ]
            , viewCurrentSuggestions (findCurrentSuggestionsM model)
            ]
        , Form.group []
            [ Form.label [] [ text "Chosen People" ]
            , if Set.isEmpty model.chosenSuggestions then
                Html.small [ class "form-text form-muted" ]
                    [ text "No one yet" ]

              else
                viewChosenSuggestions model.chosenSuggestions
            , hiddenChosenSuggestionsFields model.chosenSuggestions
            ]
        , Form.group []
            [ Form.label []
                [ text "Note" ]
            , Textarea.textarea [ Textarea.attrs [ Html.name "contents" ] ]
            ]
        , tokenField model.csrf
        , Form.group []
            [ Button.button
                [ Button.primary
                , Button.attrs [ Html.type_ "submit" ]
                ]
                [ text "Submit Note" ]
            ]
        ]


viewCurrentSuggestions : List Alias -> Html Msg
viewCurrentSuggestions aliases =
    div []
        (List.map
            (\a -> viewCurrentSuggestion a)
            (List.take 5 aliases)
        )


viewCurrentSuggestion : Alias -> Html Msg
viewCurrentSuggestion a =
    Badge.pillSecondary
        [ mx1
        , onClick (ChooseSuggestion a)
        ]
        [ text a ]


viewChosenSuggestions : Set Alias -> Html Msg
viewChosenSuggestions aliases =
    div []
        (List.map
            (\a -> viewChosenSuggestion a)
            (Set.toList aliases)
        )


viewChosenSuggestion : Alias -> Html Msg
viewChosenSuggestion a =
    Badge.pillPrimary
        [ mx1
        , onClick (RemoveChosen a)
        ]
        [ text a
        ]


hiddenChosenSuggestionsFields : Set Alias -> Html Msg
hiddenChosenSuggestionsFields aliases =
    Select.select
        [ Select.attrs
            [ Html.multiple True
            , Html.name "people"
            , Html.hidden True
            ]
        ]
        (List.map
            (\a ->
                Select.item
                    [ Html.value a
                    , Html.selected True
                    ]
                    [ text a ]
            )
            (Set.toList aliases)
        )


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.none


onSearchKey : Attribute Msg
onSearchKey =
    preventDefaultOn "keydown"
        (Decode.map
            (\m -> ( m, True ))
            tabKeyDecoder
        )


tabKeyDecoder : Decoder Msg
tabKeyDecoder =
    let
        decodeTab s =
            case s of
                "Tab" ->
                    Decode.succeed TryToSelectUsingKeyboard

                "Enter" ->
                    Decode.succeed AddUnknownAlias

                _ ->
                    Decode.fail "not tab"
    in
    Decode.andThen decodeTab (Decode.field "key" Decode.string)


update : Msg -> Model -> Model
update msg model =
    case msg of
        ChangeContents c ->
            resetModelBasedOnNewContents c model

        ChooseSuggestion a ->
            resetModelBasedOnNewContents ""
                { model
                    | chosenSuggestions = Set.insert a model.chosenSuggestions
                }

        RemoveChosen a ->
            { model | chosenSuggestions = Set.remove a model.chosenSuggestions }

        TryToSelectUsingKeyboard ->
            case
                findCurrentSuggestionsM
                    model
            of
                a :: _ ->
                    { model
                        | contents = ""
                        , chosenSuggestions =
                            Set.insert a model.chosenSuggestions
                    }

                _ ->
                    model

        AddUnknownAlias ->
            { model | contents = "", chosenSuggestions = Set.insert model.contents model.chosenSuggestions }

        NoOp ->
            model


resetModelBasedOnNewContents : String -> Model -> Model
resetModelBasedOnNewContents newContents model =
    { model
        | contents = newContents
    }


findCurrentSuggestionsM : Model -> List Alias
findCurrentSuggestionsM model =
    findCurrentSuggestions model.availableAliases model.chosenSuggestions model.contents


findCurrentSuggestions : Set Alias -> Set Alias -> String -> List Alias
findCurrentSuggestions available chosen s =
    let
        aliases =
            Set.diff available chosen

        prefixeds =
            findPrefixeds aliases s

        containses =
            findContainses aliases s
    in
    Set.toList prefixeds ++ Set.toList (Set.diff containses prefixeds)


findPrefixeds : Set Alias -> String -> Set Alias
findPrefixeds aliases s =
    Set.filter
        (\a -> String.startsWith (String.toLower s) (String.toLower a))
        aliases


findContainses : Set Alias -> String -> Set Alias
findContainses aliases s =
    Set.filter
        (\a -> String.contains (String.toLower s) (String.toLower a))
        aliases
