module Data.CSRF exposing (CSRFToken, tokenField)

import Html exposing (Html, input)
import Html.Attributes exposing (name, type_, value)


type alias CSRFToken =
    String


tokenField : CSRFToken -> Html msg
tokenField t =
    input [ type_ "hidden", name "_token", value t ] []
