module SocialGraph exposing (main)

import Browser
import Browser.Events
import Canvas exposing (..)
import Color
import Data.Uuid exposing (..)
import Dict exposing (Dict)
import Force exposing (State)
import Graph exposing (Edge, Graph, Node, NodeContext, NodeId)
import Html exposing (Html)
import Html.Attributes exposing (align, size)
import Html.Events exposing (on)
import Html.Events.Extra.Mouse as Mouse
import Http
import Json.Decode as Decode
import Time


w : Float
w =
    1750


h : Float
h =
    1000


type Msg
    = Tick Time.Posix
    | GotGraph (Result Http.Error SocialGraph)
    | DidScale ScaleDirection
    | DidMove MoveDirection


type alias SocialGraph =
    { people : Dict Uuid String, notes : Dict Uuid (List Uuid) }


type Model
    = SimulationPending
    | SimulationFetchError Http.Error
    | SimulationStarted SimulationModel


type alias SimulationModel =
    { graph : Graph Entity ()
    , simulation : Force.State NodeId
    , zoom : Zoom
    }


type alias Zoom =
    { scale : Float, xOffset : Float, yOffset : Float }


type alias Entity =
    Force.Entity NodeId { value : DonnaNode }


initializeNode : NodeContext DonnaNode () -> NodeContext Entity ()
initializeNode ctx =
    { node = { label = Force.entity ctx.node.id ctx.node.label, id = ctx.node.id }
    , incoming = ctx.incoming
    , outgoing = ctx.outgoing
    }


type alias Flags =
    { apiBurl : String, token : String }


init : Flags -> ( Model, Cmd Msg )
init s =
    ( SimulationPending, fetchGraph s.apiBurl s.token )


fetchGraph : String -> String -> Cmd Msg
fetchGraph burl token =
    Http.request
        { method = "GET"
        , headers = [ Http.header "Authorization" ("Bearer " ++ token) ]
        , url = burl ++ "/graph"
        , body = Http.emptyBody
        , expect = Http.expectJson GotGraph socialGraphDecoder
        , timeout = Nothing
        , tracker = Nothing
        }


socialGraphDecoder : Decode.Decoder SocialGraph
socialGraphDecoder =
    Decode.map2 SocialGraph (Decode.field "people" (Decode.dict Decode.string)) (Decode.field "notes" (Decode.dict (Decode.list Decode.string)))



-- Todo fill in the correct thing


updateNode : ( Float, Float ) -> NodeContext Entity () -> NodeContext Entity ()
updateNode ( x, y ) nodeCtx =
    let
        nodeValue =
            nodeCtx.node.label
    in
    updateContextWithValue nodeCtx { nodeValue | x = x, y = y }


updateContextWithValue : NodeContext Entity () -> Entity -> NodeContext Entity ()
updateContextWithValue nodeCtx value =
    let
        node =
            nodeCtx.node
    in
    { nodeCtx | node = { node | label = value } }


updateGraphWithList : Graph Entity () -> List Entity -> Graph Entity ()
updateGraphWithList =
    let
        graphUpdater value =
            Maybe.map (\ctx -> updateContextWithValue ctx value)
    in
    List.foldr (\node graph -> Graph.update node.id (graphUpdater node) graph)


update : Msg -> Model -> Model
update msg model =
    case model of
        SimulationPending ->
            case msg of
                GotGraph errOrGraph ->
                    case errOrGraph of
                        Ok socialGraph ->
                            let
                                graph =
                                    Graph.mapContexts initializeNode (makePeopleGraph socialGraph)

                                link { from, to } =
                                    { source = from, target = to, distance = 100, strength = Nothing }

                                forces =
                                    [ Force.customLinks 1 <| List.map link <| Graph.edges graph
                                    , Force.manyBody <| List.map .id <| Graph.nodes graph
                                    , Force.center (w / 2) (h / 2)
                                    ]

                                zoom =
                                    { scale = 0.5, xOffset = 0, yOffset = 0 }
                            in
                            SimulationStarted (SimulationModel graph (Force.simulation forces) zoom)

                        Err err ->
                            SimulationFetchError err

                _ ->
                    model

        SimulationFetchError _ ->
            model

        SimulationStarted { graph, simulation, zoom } ->
            case msg of
                Tick t ->
                    SimulationStarted <|
                        let
                            ( newState, list ) =
                                Force.tick simulation <| List.map .label <| Graph.nodes graph
                        in
                        SimulationModel (updateGraphWithList graph list) newState zoom

                DidScale direction ->
                    SimulationStarted <|
                        let
                            newZoom =
                                { zoom
                                    | scale =
                                        case direction of
                                            ScaleUp ->
                                                min (zoom.scale * 1.1) 10

                                            ScaleDown ->
                                                max (zoom.scale * 0.9) 0.1

                                            ScaleOther ->
                                                zoom.scale
                                }
                        in
                        SimulationModel graph simulation newZoom

                DidMove m ->
                    SimulationStarted <|
                        let
                            increment =
                                50

                            newZoom =
                                case m of
                                    MoveUp ->
                                        { zoom | yOffset = zoom.yOffset + increment }

                                    MoveDown ->
                                        { zoom | yOffset = zoom.yOffset - increment }

                                    MoveLeft ->
                                        { zoom | xOffset = zoom.xOffset + increment }

                                    MoveRight ->
                                        { zoom | xOffset = zoom.xOffset - increment }

                                    MoveOther ->
                                        zoom
                        in
                        SimulationModel graph simulation newZoom

                _ ->
                    model


subscriptions : Model -> Sub Msg
subscriptions model =
    case model of
        SimulationPending ->
            Sub.none

        SimulationFetchError _ ->
            Sub.none

        SimulationStarted simulationModel ->
            Sub.batch
                [ Browser.Events.onKeyPress scaleKeyDecoder
                , Browser.Events.onKeyPress moveKeyDecoder
                , Browser.Events.onAnimationFrame Tick
                ]


type ScaleDirection
    = ScaleUp
    | ScaleDown
    | ScaleOther


scaleKeyDecoder : Decode.Decoder Msg
scaleKeyDecoder =
    Decode.map (DidScale << toScaleDirection) (Decode.field "key" Decode.string)


toScaleDirection : String -> ScaleDirection
toScaleDirection string =
    case string of
        "u" ->
            ScaleUp

        "d" ->
            ScaleDown

        _ ->
            ScaleOther


moveKeyDecoder : Decode.Decoder Msg
moveKeyDecoder =
    Decode.map (DidMove << toMoveDirection) (Decode.field "key" Decode.string)


type MoveDirection
    = MoveUp
    | MoveDown
    | MoveLeft
    | MoveRight
    | MoveOther


toMoveDirection : String -> MoveDirection
toMoveDirection string =
    case string of
        "h" ->
            MoveLeft

        "l" ->
            MoveRight

        "j" ->
            MoveDown

        "k" ->
            MoveUp

        _ ->
            MoveOther


view : Model -> Html Msg
view model =
    case model of
        SimulationPending ->
            Html.text "Your social graph is being fetched."

        SimulationFetchError _ ->
            Html.text "An error occurred while fetching your social graph."

        SimulationStarted simulationModel ->
            viewSimulationModel simulationModel


viewSimulationModel : SimulationModel -> Html Msg
viewSimulationModel simulationModel =
    Canvas.toHtml ( round w, round h )
        --  round (w * simulationModel.zoom), round (h * simulationModel.zoom) )
        []
        ([ Canvas.shapes [ fill Color.white ] [ Canvas.rect ( 0, 0 ) w h ] ]
            ++ --  Otherwise the old stuff doesn't get emptied
               (Graph.edges simulationModel.graph
                    |> List.map (linkElement simulationModel.zoom simulationModel.graph)
               )
            ++ (Graph.nodes simulationModel.graph
                    |> List.concatMap (nodeElement simulationModel.zoom)
               )
        )


linkElement : Zoom -> Graph Entity () -> Edge () -> Canvas.Renderable
linkElement zoom graph edge =
    Canvas.shapes [ stroke Color.blue ] <|
        let
            line from to =
                Canvas.path from [ Canvas.lineTo to ]

            source =
                Maybe.withDefault (Force.entity 0 NoteNode) <| Maybe.map (.node >> .label) <| Graph.get edge.from graph

            target =
                Maybe.withDefault (Force.entity 0 NoteNode) <| Maybe.map (.node >> .label) <| Graph.get edge.to graph
        in
        [ line (toPoint zoom source) (toPoint zoom target) ]


toPoint : Zoom -> Entity -> Point
toPoint zoom e =
    let
        translate ( x, y ) =
            ( x - w / 2, y - h / 2 )

        translateBack ( x, y ) =
            ( x + w / 2, y + h / 2 )

        scale ( x, y ) =
            ( x * zoom.scale, y * zoom.scale )

        offset ( x, y ) =
            ( x + zoom.xOffset, y + zoom.yOffset )
    in
    ( e.x, e.y ) |> translate |> scale |> offset |> translateBack


nodeElement : Zoom -> Node Entity -> List Canvas.Renderable
nodeElement zoom node =
    case node.label.value of
        PersonNode alias ->
            let
                point =
                    toPoint zoom node.label
            in
            [ Canvas.shapes [ fill Color.red ]
                [ circle point 10
                ]
            , Canvas.text [] point alias
            ]

        NoteNode ->
            let
                point =
                    toPoint zoom node.label
            in
            [ Canvas.shapes [ fill Color.green ]
                [ circle point 5
                ]
            ]


main : Program Flags Model Msg
main =
    Browser.element
        { init = init
        , view = view
        , update = \msg model -> ( update msg model, Cmd.none )
        , subscriptions = subscriptions
        }


makePeopleGraph : SocialGraph -> Graph DonnaNode ()
makePeopleGraph graph =
    let
        idDict =
            Dict.fromList (List.indexedMap (\index uuid -> ( uuid, index )) (List.map Tuple.first (Dict.toList graph.people) ++ List.map Tuple.first (Dict.toList graph.notes)))

        personNodes =
            List.filterMap
                (\( puuid, alias ) ->
                    Maybe.map (\id -> Node id (PersonNode alias)) (Dict.get puuid idDict)
                )
                (Dict.toList graph.people)

        noteNodes =
            List.filterMap
                (\( nuuid, _ ) -> Maybe.map (\id -> Node id NoteNode) (Dict.get nuuid idDict))
                (Dict.toList graph.notes)

        nodes =
            personNodes ++ noteNodes

        edge fromuuid touuid =
            Maybe.map2 (\from to -> Edge from to ()) (Dict.get fromuuid idDict) (Dict.get touuid idDict)

        edges =
            List.concatMap
                (\( nuuid, puuids ) -> List.filterMap (\puuid -> edge nuuid puuid) puuids)
                (Dict.toList graph.notes)
    in
    Graph.fromNodesAndEdges nodes edges


type DonnaNode
    = PersonNode String
    | NoteNode
