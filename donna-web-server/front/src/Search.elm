module Search exposing (main)

import Bootstrap.Badge as Badge
import Bootstrap.Button as Button
import Bootstrap.Form as Form
import Bootstrap.Form.Input as Input
import Bootstrap.Form.Select as Select
import Bootstrap.Form.Textarea as Textarea
import Bootstrap.Utilities.Spacing exposing (..)
import Browser exposing (..)
import Browser.Events
import Data.Alias exposing (..)
import Data.CSRF exposing (..)
import Data.Uuid exposing (..)
import Debug
import Dict exposing (Dict)
import Html exposing (Attribute, Html, div, text)
import Html.Attributes as Html exposing (class)
import Html.Events exposing (..)
import Json.Decode as Decode exposing (Decoder)
import Set exposing (Set)


main : Program Flags Model Msg
main =
    Browser.element
        { init = init
        , view = view
        , update = \msg model -> ( update msg model, Cmd.none )
        , subscriptions = subscriptions
        }


type alias Flags =
    { aliases : Decode.Value
    }


type alias Model =
    { contents : String
    , availableAliases : Dict Alias Uuid
    }


type alias Option =
    { alias : Alias, uuid : Uuid }


type Msg
    = ChangeContents String


init : Flags -> ( Model, Cmd Msg )
init flags =
    let
        available =
            case Decode.decodeValue (Decode.dict Decode.string) flags.aliases of
                Ok aliases ->
                    aliases

                Err _ ->
                    Dict.empty

        m =
            { contents = ""
            , availableAliases = available
            }
    in
    ( m, Cmd.none )


view : Model -> Html Msg
view model =
    div []
        [ Html.input
            [ Html.placeholder "Look for a person"
            , Html.value model.contents
            , onInput ChangeContents
            , Html.autofocus True
            ]
            []
        , viewCurrentSuggestions (findCurrentSuggestionsM model)
        ]


viewCurrentSuggestions : List Option -> Html Msg
viewCurrentSuggestions aliases =
    div []
        [ Html.ul [ class ".list-group" ]
            (List.map
                (\a -> viewCurrentSuggestion a)
                (List.take 25 aliases)
            )
        ]


viewCurrentSuggestion : Option -> Html Msg
viewCurrentSuggestion a =
    Html.li [ class "list-group-item" ]
        [ Html.a
            [ Html.href ("/person/" ++ a.uuid) ]
            [ text a.alias ]
        ]


viewChosenSuggestions : Set Alias -> Html Msg
viewChosenSuggestions aliases =
    div []
        (List.map
            (\a -> viewChosenSuggestion a)
            (Set.toList aliases)
        )


viewChosenSuggestion : Alias -> Html Msg
viewChosenSuggestion a =
    Badge.pillPrimary
        [ mx1
        ]
        [ text a
        ]


hiddenChosenSuggestionsFields : Set Alias -> Html Msg
hiddenChosenSuggestionsFields aliases =
    Select.select
        [ Select.attrs [ Html.name "people", Html.hidden True ]
        ]
        (List.map
            (\a -> Select.item [ Html.value a ] [ text a ])
            (Set.toList aliases)
        )


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.none


update : Msg -> Model -> Model
update msg model =
    case msg of
        ChangeContents c ->
            { model
                | contents = c
            }


findCurrentSuggestionsM : Model -> List Option
findCurrentSuggestionsM model =
    findCurrentSuggestions model.availableAliases model.contents


findCurrentSuggestions : Dict Alias Uuid -> String -> List Option
findCurrentSuggestions aliases s =
    let
        prefixeds =
            findPrefixeds aliases s

        containses =
            findContainses aliases s
    in
    toOptions prefixeds ++ toOptions (Dict.diff containses prefixeds)


findPrefixeds : Dict Alias Uuid -> String -> Dict Alias Uuid
findPrefixeds aliases s =
    Dict.filter
        (\a _ ->
            String.startsWith
                (String.toLower s)
                (String.toLower a)
        )
        aliases


findContainses : Dict Alias Uuid -> String -> Dict Alias Uuid
findContainses aliases s =
    Dict.filter
        (\a _ ->
            String.contains
                (String.toLower s)
                (String.toLower a)
        )
        aliases


toOptions : Dict Alias Uuid -> List Option
toOptions d =
    List.map (\( a, u ) -> { alias = a, uuid = u }) (Dict.toList d)
