# Donna web-server front-end

Fixing the nix build for this elm code after you've updated the dependencies:

```
elm2nix convert > nix/elm-srcs.nix
elm2nix snapshot > nix/versions.dat
nix-build
```
