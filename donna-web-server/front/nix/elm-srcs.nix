{

      "gampleman/elm-visualization" = {
        sha256 = "0rgbrn3njcrs0mzqlqf92qpwidz99dav0k9gyl5r5la4w7cw1xqy";
        version = "2.0.0";
      };

      "mpizenberg/elm-pointer-events" = {
        sha256 = "0dfcqwbx4lpspfpq78215nsn3mxm6y3z3lfylj7kls8qvx6sbpvj";
        version = "4.0.1";
      };

      "elm/json" = {
        sha256 = "0kjwrz195z84kwywaxhhlnpl3p251qlbm5iz6byd6jky2crmyqyh";
        version = "1.1.3";
      };

      "rundis/elm-bootstrap" = {
        sha256 = "0jn864353vbq6q73gmycbcncm26a9v0mkb6ba75ab611sq7pc5kb";
        version = "5.1.0";
      };

      "elm-community/typed-svg" = {
        sha256 = "0v35jcr3p2r4gragap8p94md8rbch5h9swaqx1dhsw7lvgf01nxr";
        version = "4.0.0";
      };

      "elm/html" = {
        sha256 = "1n3gpzmpqqdsldys4ipgyl1zacn0kbpc3g4v3hdpiyfjlgh8bf3k";
        version = "1.0.0";
      };

      "elm/browser" = {
        sha256 = "1zlmx672glg7fdgkvh5jm47y85pv7pdfr5mkhg6x7ar6k000vyka";
        version = "1.0.1";
      };

      "avh4/elm-color" = {
        sha256 = "0n16wnvp87x9az3m5qjrl6smsg7051m719xn5d244painx8xmpzq";
        version = "1.0.0";
      };

      "elm/core" = {
        sha256 = "1l0qdbczw91kzz8sx5d5zwz9x662bspy7p21dsr3f2rigxiix2as";
        version = "1.0.2";
      };

      "elm/http" = {
        sha256 = "008bs76mnp48b4dw8qwjj4fyvzbxvlrl4xpa2qh1gg2kfwyw56v1";
        version = "2.0.0";
      };

      "elm/time" = {
        sha256 = "0vch7i86vn0x8b850w1p69vplll1bnbkp8s383z7pinyg94cm2z1";
        version = "1.0.0";
      };

      "joakin/elm-canvas" = {
        sha256 = "1llnvr05f9w6z6r44alhhgai3yppfsjgsll8yzrb61f2a22jbjh9";
        version = "3.0.5";
      };

      "elm-community/graph" = {
        sha256 = "1rwsq2126q0rb4vmy95ajxfm3m063d6lw0p90d510nzcrbm9bxbc";
        version = "6.0.0";
      };

      "folkertdev/elm-deque" = {
        sha256 = "1m87q2py0hmhwg86618a6bzgxhgbay55d7y384hn3y0s338jyhkp";
        version = "3.0.1";
      };

      "ianmackenzie/elm-triangular-mesh" = {
        sha256 = "0vc2aix77zcc20av88hvyziin25163jpahypzxp7n81vymzzgqg9";
        version = "1.0.2";
      };

      "elm/bytes" = {
        sha256 = "02ywbf52akvxclpxwj9n04jydajcbsbcbsnjs53yjc5lwck3abwj";
        version = "1.0.8";
      };

      "elm/file" = {
        sha256 = "1rljcb41dl97myidyjih2yliyzddkr2m7n74x7gg46rcw4jl0ny8";
        version = "1.0.5";
      };

      "avh4/elm-fifo" = {
        sha256 = "1ka0iz2psr75h4qz7hh5z1prclah1nais9aaycaxapfd7inqmrrc";
        version = "1.0.4";
      };

      "ianmackenzie/elm-interval" = {
        sha256 = "104lx0qaddh3bmx4jf0lr7ajixjkb2g0m8m6dpmczs3a50rwcng6";
        version = "1.0.1";
      };

      "folkertdev/one-true-path-experiment" = {
        sha256 = "0kp7yl4nn346xwnri7i5imd4bp8ywz6gifgpbcr0hp43xvyrmn3k";
        version = "4.0.3";
      };

      "ianmackenzie/elm-float-extra" = {
        sha256 = "081yh7011vbb3xbc96aqav8byfn9f8qx6hdg2s7mqjyzfriw143j";
        version = "1.0.1";
      };

      "elm/svg" = {
        sha256 = "1cwcj73p61q45wqwgqvrvz3aypjyy3fw732xyxdyj6s256hwkn0k";
        version = "1.0.1";
      };

      "justinmimbs/date" = {
        sha256 = "1s5a1yd08r2f6lgg539rlkim68lg366hahcfky4gdq37050j6jks";
        version = "3.1.2";
      };

      "elm-community/intdict" = {
        sha256 = "09i1fk63gp6sr6kc6ccs8g0kxvqhw5czghi9cl8flizanrgcmva1";
        version = "3.0.0";
      };

      "elm/parser" = {
        sha256 = "0a3cxrvbm7mwg9ykynhp7vjid58zsw03r63qxipxp3z09qks7512";
        version = "1.1.0";
      };

      "justinmimbs/time-extra" = {
        sha256 = "0s5r0w5dfyrwpkgx594p3z39zrvz2gnxfzb6qlddwfibiaj2nfz9";
        version = "1.1.0";
      };

      "elm/url" = {
        sha256 = "0av8x5syid40sgpl5vd7pry2rq0q4pga28b4yykn9gd9v12rs3l4";
        version = "1.0.0";
      };

      "ryannhg/date-format" = {
        sha256 = "1hjic1nrfiss5wjqsqvh0szb85ivjglmarcq1j8xl67z3ygv3lvi";
        version = "2.3.0";
      };

      "folkertdev/svg-path-lowlevel" = {
        sha256 = "0m52y58vv4qm5xxi456ikcshvr6d1q8mq3si1wq7yixdaczpgvjh";
        version = "3.0.0";
      };

      "elm-community/list-extra" = {
        sha256 = "0rsfn0074lldxwp1hs71z1p6rwxh7rli5sfyllgdkdn7hcpng47p";
        version = "8.2.0";
      };

      "ianmackenzie/elm-geometry" = {
        sha256 = "0sdmvjs2x355lff4f8r9r2mmwkw7swak1s63660xk2admzgj132f";
        version = "1.2.1";
      };

      "elm/virtual-dom" = {
        sha256 = "0q1v5gi4g336bzz1lgwpn5b1639lrn63d8y6k6pimcyismp2i1yg";
        version = "1.0.2";
      };
}
