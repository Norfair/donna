final:
  previous: 
    {
      donna-web-server-front = 
      let mkElmDerivation = import ./mkElmDerivation.nix;
      in mkElmDerivation {
              pkgs = final;
              name = "donna-web-server-front";
              srcs = ./elm-srcs.nix;
              src = final.gitignoreSource ../.;
              targets = [
                "Note"
                "Search"
                "SocialGraph"
              ];
              srcdir = "./src";
              outputJavaScript = true;
            };
    }
