{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE OverloadedStrings #-}

module Donna.Web.Server
  ( donnaWebServer
  ) where

import Donna.Web.Server.Handler.Import

import qualified Data.Map as M
import qualified Data.Text as T

import Control.Concurrent.Async
import Control.Concurrent.STM
import Control.Monad.Logger

import Lens.Micro

import qualified Database.Persist.Sqlite as DB

import qualified Network.HTTP.Client as Http
import qualified Network.HTTP.Client.TLS as Http
import qualified Network.Wai.Handler.Warp as Warp
import qualified Network.Wai.Middleware.RequestLogger as Wai

import qualified Donna.Server as API
import qualified Donna.Server.OptParse as API

import Donna.Web.Server.Application ()
import Donna.Web.Server.OptParse

donnaWebServer :: IO ()
donnaWebServer = do
  Instructions (DispatchServe ss) Settings <- getInstructions
  concurrently_ (runDonnaWebServer ss) (API.runDonnaServer $ deriveAPISettings ss)

deriveAPISettings :: ServeSettings -> API.ServeSettings
deriveAPISettings ServeSettings {..} =
  let serveSetPort = serveSetAPIPort
   in API.ServeSettings {..}

runDonnaWebServer :: ServeSettings -> IO ()
runDonnaWebServer ss@ServeSettings {..} =
  runStderrLoggingT $ do
    DB.withSqlitePoolInfo
      (DB.mkSqliteConnectionInfo (T.pack $ toFilePath serveSetWebDBFile) & DB.walEnabled .~ False &
       DB.fkEnabled .~ False)
      1 $ \pool -> do
      liftIO $ ensureDir $ parent serveSetDBFile
      man <- liftIO $ Http.newManager Http.tlsManagerSettings
      loginVar <- liftIO $ newTVarIO M.empty
      let a =
            App
              { appHttpManager = man
              , appStatic = myStatic
              , appAPIBaseUrl = serveSetAPIBaseUrl
              , appLoginTokens = loginVar
              , appConnectionPool = pool
              , appGoogleAnalyticsTracking = serveSetGoogleAnalyticsTracking
              , appGoogleSearchConsoleVerification = serveSetGoogleSearchConsoleVerification
              , appGoogleSettings = serveSetGoogleSettings
              }
      let defMiddles = defaultMiddlewaresNoLogging
      let extraMiddles =
            if development
              then Wai.logStdoutDev
              else Wai.logStdout
      let middle = extraMiddles . defMiddles
      plainApp <- liftIO $ toWaiAppPlain a
      let app_ = middle plainApp
      when development $
        logInfoN $
        T.unlines
          [ "Serving the donna web service at http://" <> serveSetWebHost <> ":" <>
            T.pack (show serveSetWebPort)
          , "with settings"
          , T.pack (ppShow ss)
          ]
      DB.runSqlPool setupDatabase pool
      (liftIO $ Warp.run serveSetWebPort app_)

setupDatabase :: MonadIO m => ReaderT DB.SqlBackend m ()
setupDatabase = DB.runMigration migrateAll
