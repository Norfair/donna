{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE ViewPatterns #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}

module Donna.Web.Server.Application where

import Donna.Web.Server.Handler.Import

import Donna.Web.Server.Handler
import Donna.Web.Server.Handler.Import.Menu

mkYesodDispatch "App" resourcesApp
