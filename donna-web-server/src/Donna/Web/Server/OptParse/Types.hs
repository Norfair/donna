module Donna.Web.Server.OptParse.Types where

import Donna.Web.Server.Import

data Instructions =
  Instructions Dispatch Settings

newtype Dispatch =
  DispatchServe ServeSettings
  deriving (Show, Eq)

data ServeSettings =
  ServeSettings
    { serveSetWebHost :: Text
    , serveSetWebPort :: Int
    , serveSetAPIBaseUrl :: BaseUrl
    , serveSetAPIPort :: Int
    , serveSetDBFile :: Path Abs File
    , serveSetWebDBFile :: Path Abs File
    , serveSetGoogleAnalyticsTracking :: Maybe Text
    , serveSetGoogleSearchConsoleVerification :: Maybe Text
    , serveSetGoogleSettings :: Maybe GoogleSettings
    }
  deriving (Show, Eq)

data GoogleSettings =
  GoogleSettings
    { googleSetClientId :: Text
    , googleSetClientSecret :: Text
    }
  deriving (Show, Eq)

data Settings =
  Settings
  deriving (Show, Eq)

data Arguments =
  Arguments Command Flags

newtype Command =
  CommandServe ServeFlags
  deriving (Show, Eq)

data ServeFlags =
  ServeFlags
    { serveFlagWebHost :: Maybe Text
    , serveFlagWebPort :: Maybe Int
    , serveFlagAPIBaseUrl :: Maybe BaseUrl
    , serveFlagAPIPort :: Maybe Int
    , serveFlagDBFile :: Maybe FilePath
    , serveFlagWebDBFile :: Maybe FilePath
    , serveFlagGoogleAnalyticsTracking :: Maybe Text
    , serveFlagGoogleSearchConsoleVerification :: Maybe Text
    , serveFlagGoogleFlags :: GoogleFlags
    }
  deriving (Show, Eq)

data GoogleFlags =
  GoogleFlags
    { googleFlagClientId :: Maybe Text
    , googleFlagClientSecret :: Maybe Text
    }
  deriving (Show, Eq)

data Flags =
  Flags
  deriving (Show, Eq)

data Configuration =
  Configuration
  deriving (Show, Eq)

data Environment =
  Environment
    { envWebHost :: Maybe Text
    , envWebPort :: Maybe Int
    , envAPIBaseUrl :: Maybe BaseUrl
    , envAPIPort :: Maybe Int
    , envDBFile :: Maybe FilePath
    , envWebDBFile :: Maybe FilePath
    , envGoogleAnalyticsTracking :: Maybe Text
    , envGoogleSearchConsoleVerification :: Maybe Text
    , envGoogleEnv :: GoogleEnv
    }
  deriving (Show, Eq)

data GoogleEnv =
  GoogleEnv
    { googleEnvClientId :: Maybe Text
    , googleEnvClientSecret :: Maybe Text
    }
  deriving (Show, Eq)
