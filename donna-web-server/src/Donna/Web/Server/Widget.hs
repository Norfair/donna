{-# LANGUAGE NoImplicitPrelude #-}
module Donna.Web.Server.Widget where

import Donna.Web.Server.Import

import Data.Default
import Language.Haskell.TH.Syntax (Exp, Q)

import Yesod.Default.Util
  ( WidgetFileSettings
  , widgetFileNoReload
  , widgetFileReload
  )

import Donna.Web.Server.Constants

widgetFile :: String -> Q Exp
widgetFile =
  if development
    then widgetFileReload widgetFileSettings
    else widgetFileNoReload widgetFileSettings

widgetFileSettings :: WidgetFileSettings
widgetFileSettings = def
