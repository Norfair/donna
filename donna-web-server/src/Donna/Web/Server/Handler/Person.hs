{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE QuasiQuotes #-}

module Donna.Web.Server.Handler.Person where

import Donna.Web.Server.Handler.Import

import Text.Time.Pretty

import qualified Data.Set as S
import qualified Data.Map as M

getPersonR :: PersonUuid -> Handler Html
getPersonR puuid =
  withLogin $ \token -> do
    PersonInfo {..} <- runClientOrErr $ clientGetPerson token puuid
    let alias = lookupDisplayName puuid personInfoAliases
    now <- liftIO getCurrentTime
    defaultLayout $ do
      setTitle "Person"
      $(widgetFile "person")

formatTimePretty :: UTCTime -> String
formatTimePretty = formatTime defaultTimeLocale "%A %F %R"

lookupDisplayName :: PersonUuid -> Set Alias  -> Text
lookupDisplayName puuid as  = case S.toList as of
  [] ->  uuidText puuid
  (a:_) -> aliasText a
