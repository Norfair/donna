{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE QuasiQuotes #-}

module Donna.Web.Server.Handler.People where

import Donna.Web.Server.Handler.Import

import qualified Data.Map as M

getPeopleR :: Handler Html
getPeopleR =
  withLogin $ \token -> do
    aliasMap <- runClientOrErr $ clientGetAliases token
    defaultLayout $ do
      setTitle "People"
      $(widgetFile "people")


reverseMapFirst :: (Ord a, Ord b) => Map a b -> Map b a
reverseMapFirst = foldl go M.empty . M.toList
  where
    go m (a, b) = M.alter go' b m
      where
        go' ms =
          Just $
          case ms of
            Nothing -> a
            Just s -> s
