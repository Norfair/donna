{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE QuasiQuotes #-}

module Donna.Web.Server.Handler.Graph where

import Donna.Web.Server.Handler.Import

getGraphR :: Handler Html
getGraphR =
  withLogin $ \token -> do
    url <- getApiUrl
    defaultLayout $ do
      setTitle "Social Graph"
      $(widgetFile "graph")
