{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE QuasiQuotes #-}

module Donna.Web.Server.Handler.Home where

import Donna.Web.Server.Handler.Import

import qualified Yesod.Core (setTitle)

getHomeR :: Handler Html
getHomeR =
  defaultLayout $ do
    Yesod.Core.setTitle "Donna"
    $(widgetFile "home")
