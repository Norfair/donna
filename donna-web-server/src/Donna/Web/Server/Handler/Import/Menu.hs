{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE QuasiQuotes #-}

module Donna.Web.Server.Handler.Import.Menu
  ( getImportR
  , module X
  ) where

import Donna.Web.Server.Handler.Import

import Donna.Web.Server.Handler.Import.Source as X

getImportR :: Handler Html
getImportR = do
  App{..} <- getYesod
  defaultLayout $ do
    setTitle "Import"
    $(widgetFile "import")
