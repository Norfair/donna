{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}

module Donna.Web.Server.Handler.Import.Source.Google.Contacts.Similar where

import Donna.Web.Server.Handler.Import

import qualified Data.Map as M
import qualified Data.Set as S
import Lens.Micro

import qualified Network.Google.People as Google
import qualified Network.Google.People as G

import qualified Network.Google.Data.JSON as Google
import qualified Network.Google.Data.Time as Google

findSimilarPeople ::
     Map PersonUuid PersonPropertyInfo
  -> Set GatheredPerson
  -> Map GatheredPerson (Map PersonUuid (Set Similarity))
findSimilarPeople properties = M.fromSet (findSimilarPerson properties)

findSimilarPerson ::
     Map PersonUuid PersonPropertyInfo -> GatheredPerson -> Map PersonUuid (Set Similarity)
findSimilarPerson m per =
  M.filter (not . S.null) $
  M.unionsWith S.union $
  [ flip M.map m $ \ppi ->
      case fromEntry <$> personPropertyInfoEntry ppi of
        Nothing -> S.empty
        Just phoneNumbers ->
          let gPhoneNumebers = gatheredPersonPhoneNumbers per
           in S.map SamePhoneNumber $ S.intersection phoneNumbers gPhoneNumebers
  , flip M.map m $ \ppi ->
      case fromEntry <$> personPropertyInfoEntry ppi of
        Nothing -> S.empty
        Just emailAddresses ->
          let gEmailAddresses = gatheredPersonEmailAddresses per
           in S.map SameEmailAddress $ S.intersection emailAddresses gEmailAddresses
  ]

data Similarity
  = SamePhoneNumber PhoneNumber
  | SameEmailAddress EmailAddress
  deriving (Show, Eq, Ord, Generic)

instance Validity Similarity

gatherPerson :: Google.Person -> GatheredPerson
gatherPerson p =
  GatheredPerson
    { gatheredPersonNames = gatherNames p
    , gatheredPersonEmailAddresses = gatherEmailAddresses p
    , gatheredPersonPhoneNumbers = gatherPhoneNumbers p
    }

gatherEmailAddresses :: Google.Person -> Set EmailAddress
gatherEmailAddresses p =
  S.fromList $ flip mapMaybe (p ^. G.perEmailAddresses) $ \ea -> ea ^. G.eaValue >>= emailAddress

gatherPhoneNumbers :: Google.Person -> Set PhoneNumber
gatherPhoneNumbers p =
  S.fromList $
  flip mapMaybe (p ^. G.perPhoneNumbers) $ \pn -> phoneNumber <$> pn ^. G.pnCanonicalForm

data GatheredPerson =
  GatheredPerson
    { gatheredPersonNames :: !(Set Text)
    , gatheredPersonEmailAddresses :: !(Set EmailAddress)
    , gatheredPersonPhoneNumbers :: !(Set PhoneNumber)
    }
  deriving (Show, Eq, Ord, Generic)

instance Validity GatheredPerson

instance FromJSON GatheredPerson

instance ToJSON GatheredPerson

instance Semigroup GatheredPerson where
  gp1 <> gp2 =
    GatheredPerson
      { gatheredPersonNames = (S.union `on` gatheredPersonNames) gp1 gp2
      , gatheredPersonEmailAddresses = (S.union `on` gatheredPersonEmailAddresses) gp1 gp2
      , gatheredPersonPhoneNumbers = (S.union `on` gatheredPersonPhoneNumbers) gp1 gp2
      }

instance Monoid GatheredPerson where
  mempty =
    GatheredPerson
      { gatheredPersonNames = S.empty
      , gatheredPersonEmailAddresses = S.empty
      , gatheredPersonPhoneNumbers = S.empty
      }
  mappend = (<>)

gatherNames :: G.Person -> Set Text
gatherNames p = S.fromList $ flip mapMaybe (p ^. G.perNames) $ \n -> n ^. G.nDisplayName

applyGatheredPerson :: UTCTime -> GatheredPerson -> Maybe PersonEntry -> Maybe PersonEntry
applyGatheredPerson now gp@GatheredPerson {..} mpe =
  let wlu = WithLastUpdated now
      listOrSingle :: Text -> Set Text -> Maybe (Text, PersonProperty)
      listOrSingle k s =
        (,) k <$>
        case S.toList s of
          [] -> Nothing
          [v] -> Just $ wlu $ PVal v
          vs -> Just $ wlu $ PList $ map (wlu . PVal) vs
      pns = S.map phoneNumberText gatheredPersonPhoneNumbers
      eas = S.map emailAddressText gatheredPersonEmailAddresses
      listFromNothing :: [(Text, PersonProperty)]
      listFromNothing = catMaybes [listOrSingle "phone number" pns, listOrSingle "email" eas]
      withOld :: PersonProperty -> [(Text, PersonProperty)] -> PersonPropertyVal
      withOld old new = PMap $ ("old", old) : new
      lookupOrAppendList :: Text -> Set Text -> [(Text, PersonProperty)] -> [(Text, PersonProperty)]
      lookupOrAppendList k vals tups =
        if S.null vals
          then tups
          else go tups
        where
          go :: [(Text, PersonProperty)] -> [(Text, PersonProperty)]
          go [] = maybeToList $ listOrSingle k vals
          go (t@(k', pp):rest) =
            if k /= k'
              then t : go rest
              else (k, mergeVals pp) : rest
          mergeVals :: PersonProperty -> PersonProperty
          mergeVals pp@(WithLastUpdated _ pv) =
            case pv of
              PVal v ->
                case S.toList vals of
                  [v'] ->
                    if v == v'
                      then pp
                      else wlu $ PList $ [pp, wlu (PVal v')]
                  _ -> wlu $ PList $ pp : map (wlu . PVal) (S.toList vals)
              PList l -> wlu $ PList $ l ++ map (wlu . PVal) (S.toList vals)
              PMap tups' -> wlu $ PMap $ lookupOrAppendList "google contacts import" vals tups'
   in fmap PersonEntry $
      case mpe of
        Nothing ->
          if gp == mempty
            then Nothing
            else Just $ wlu $ PMap listFromNothing
        Just (PersonEntry pp@(WithLastUpdated _ pv)) ->
          Just $
          if gp == mempty
            then pp
            else let pv' =
                       case pv of
                         PMap tups ->
                           PMap $
                           appEndo
                             (mconcat
                                (map
                                   Endo
                                   [ lookupOrAppendList "phone number" pns
                                   , lookupOrAppendList "email" eas
                                   ]))
                             tups
                         _ -> withOld pp listFromNothing
                  in if pv == pv'
                       then pp
                       else wlu pv'

instance Validity a => Validity (Google.Textual a) where
  validate (Google.Textual a) = decorate "Textual" $ validate a

instance Validity Google.Date

instance Validity Google.URL

instance Validity Google.DateTime'

instance Validity Google.Address

instance Validity Google.AgeRangeType

instance Validity Google.AgeRangeTypeAgeRange

instance Validity Google.Biography

instance Validity Google.BiographyContentType

instance Validity Google.Birthday

instance Validity Google.BraggingRights

instance Validity Google.ContactGroupMembership

instance Validity Google.CoverPhoto

instance Validity Google.DomainMembership

instance Validity Google.EmailAddress

instance Validity Google.Event

instance Validity Google.FieldMetadata

instance Validity Google.Gender

instance Validity Google.ImClient

instance Validity Google.Interest

instance Validity Google.Locale

instance Validity Google.Membership

instance Validity Google.Name

instance Validity Google.Nickname

instance Validity Google.NicknameType

instance Validity Google.Occupation

instance Validity Google.Organization

instance Validity Google.Person

instance Validity Google.PersonAgeRange

instance Validity Google.PersonMetadata

instance Validity Google.PersonMetadataObjectType

instance Validity Google.PhoneNumber

instance Validity Google.Photo

instance Validity Google.ProFileMetadata

instance Validity Google.ProFileMetadataObjectType

instance Validity Google.Relation

instance Validity Google.RelationshipInterest

instance Validity Google.RelationshipStatus

instance Validity Google.Residence

instance Validity Google.SipAddress

instance Validity Google.Skill

instance Validity Google.Source

instance Validity Google.SourceType

instance Validity Google.Tagline

instance Validity Google.UserDefined
