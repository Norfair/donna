{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE KindSignatures #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}

module Donna.Web.Server.Handler.Import.Source.Google.Contacts where

import Donna.Web.Server.Handler.Import

-- import qualified Data.Map as M
import Data.Proxy (Proxy(..))
import qualified Data.Set as S
import qualified Data.Text as T
import Lens.Micro
import System.IO

import Network.Google
import Network.Google.Auth
import Network.Google.People

import Donna.Web.Server.Handler.Import.Source.Google.Contacts.Similar

getImportGoogleContactsR :: Handler Html
getImportGoogleContactsR =
  withLogin $ \_ ->
    withOauthClient $ \oAuthClient -> do
      un <- requireAuthId
      token <- genToken
      now <- liftIO getCurrentTime
      let authUrl = formURL oAuthClient donnaScopes
      -- ppis <- runClientOrErr $ clientGetPeopleProperties t
      gcs <-
        runDB $
        (S.fromList . map (gatherPerson . googleContactPerson . entityVal)) <$>
        selectList [GoogleContactUser ==. un] []
      -- let simMap = findSimilarPeople ppis gcs
      let cards = makeCards now gcs
      let cardWidget :: Card -> Widget
          cardWidget Card {..} = $(widgetFile "import/google/contacts/card")
      defaultLayout $ do
        setTitle "Import - Google Contacts"
        $(widgetFile "import/google/contacts")

makeCards :: UTCTime -> Set GatheredPerson -> [Card]
makeCards now gps =
  flip map (S.toList gps) $ \gp ->
    Card
      { cardUnmatchedEntry =
          NewOption
            { optionAliases = mapSetMaybe parseAlias $ gatheredPersonNames gp
            , optionEntry = applyGatheredPerson now gp Nothing
            }
      }
  where
    mapSetMaybe func = S.fromList . mapMaybe func . S.toList

data Card =
  Card
    { cardUnmatchedEntry :: NewOption
    }
  deriving (Show, Eq)

data NewOption =
  NewOption
    { optionAliases :: Set Alias
    , optionEntry :: Maybe PersonEntry
    }
  deriving (Show, Eq)

prettySet :: Set Text -> Maybe Text
prettySet = prettyList . S.toList

prettyList :: [Text] -> Maybe Text
prettyList [] = Nothing
prettyList as = Just $ go as
  where
    go [] = ""
    go [a] = a
    go (a:rest) = a <> ", " <> go rest

postImportGoogleContactsR :: Handler Html
postImportGoogleContactsR = do
  withOauthClient $ \oAuthClient -> do
    code <- runInputPost $ OAuthCode <$> ireq textField "code"
    let creds = installedApplication oAuthClient code
    getAndSavePeople creds
    redirect ImportGoogleContactsR

withGoogleSettings :: (GoogleSettings -> Handler a) -> Handler a
withGoogleSettings func = do
  mgs <- getsYesod appGoogleSettings
  case mgs of
    Nothing -> notFound
    Just gs -> func gs

getAndSavePeople :: Credentials DonnaScopes -> Handler ()
getAndSavePeople creds = do
  un <- requireAuthId
  lcrs <- getPeople creds
  now <- liftIO getCurrentTime
  void $
    runDB $
    insertMany_ $
    flip concatMap lcrs $ \lcr ->
      flip map (lcr ^. lcrConnections) $ \p ->
        GoogleContact
          {googleContactUser = un, googleContactPerson = p, googleContactTimestamp = now}

getPeople :: Credentials DonnaScopes -> Handler [ListConnectionsResponse]
getPeople creds =
  liftIO $ do
    lgr <- newLogger Debug stdout
    man <- newManager tlsManagerSettings
    env <- newEnvWith creds lgr man
    runResourceT . runGoogle env $ makeCalls

makeCalls :: Google DonnaScopes [ListConnectionsResponse]
makeCalls = do
  let origReq :: PeopleConnectionsList
      origReq =
        peopleConnectionsList "people/me" &
        pclRequestMaskIncludeField .~ Just "person.names,person.emailAddresses,person.phoneNumbers"
  resp <- send origReq
  let go r =
        case r ^. lcrNextSyncToken of
          Nothing -> pure []
          Just npt -> do
            r' <- send $ origReq & pclPageToken .~ Just npt
            rest <- go r'
            pure $ r' : rest
  rest <- go resp
  pure (resp : rest)

withOauthClient :: (OAuthClient -> Handler a) -> Handler a
withOauthClient func = withGoogleSettings $ func . deriveOauthClient

deriveOauthClient :: GoogleSettings -> OAuthClient
deriveOauthClient GoogleSettings {..} =
  OAuthClient
    {_clientId = ClientId googleSetClientId, _clientSecret = GSecret googleSetClientSecret}

type DonnaScopes = '[ "https://www.googleapis.com/auth/contacts.readonly"]

donnaScopes :: Proxy DonnaScopes
donnaScopes = contactsReadOnlyScope

data ImportUnmatched =
  ImportUnmatched
    { importUnmatchedAliases :: Set Alias
    , importUnmatchedEntry :: Maybe PersonEntry
    }
  deriving (Show, Eq, Generic)

importUnmatchedForm :: FormInput Handler ImportUnmatched
importUnmatchedForm =
  ImportUnmatched <$> ireq aliasesField "aliases" <*> iopt personEntryField "entry"

aliasesField :: Field Handler (Set Alias)
aliasesField =
  convertField S.fromList S.toList $
  multiSelectField (pure OptionList {olOptions = [], olReadExternal = parseAlias})

personEntryField :: Field Handler PersonEntry
personEntryField = checkMMap (go . unTextarea) (Textarea . entryContentsText) $ textareaField
  where
    go :: Text -> Handler (Either Text PersonEntry)
    go t = do
      now <- liftIO getCurrentTime
      pure $ left (T.pack . prettyPrintEntryParseException) $ parsePersonEntry now t

postImportGoogleContactUnmatchedR :: Handler Html
postImportGoogleContactUnmatchedR =
  withLogin $ \t -> do
    ImportUnmatched {..} <- runInputPost importUnmatchedForm
    runClientOrErr $ do
      uuid <- clientPostPersonWithAliases t importUnmatchedAliases
      forM_ importUnmatchedEntry $ \e -> clientPostEntry t uuid e
    -- TODO also remove the person?
    redirect ImportGoogleContactsR
