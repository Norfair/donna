{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE QuasiQuotes #-}

module Donna.Web.Server.Handler.Note where

import Donna.Web.Server.Handler.Import

import Data.Set as S

getNoteR :: Handler Html
getNoteR =
  withLogin $ \token -> do
    aliasMap <- runClientOrErr $ clientGetAliases token
    csrf <- getCSRFToken
    defaultLayout $ do
      setTitle "Note"
      $(widgetFile "note")

noteForm :: FormInput Handler NewNote
noteForm =
  NewNote <$>
  (S.fromList <$>
   ireq (multiSelectField (pure OptionList {olOptions = [], olReadExternal = parseAlias})) "people") <*>
  (unTextarea <$> ireq textareaField "contents")

postNoteR :: Handler Html
postNoteR =
  withLogin $ \token -> do
    nn <- runInputPost noteForm
    NoContent <- runClientOrErr $ clientPostNote token nn
    redirect NoteR
