{-# LANGUAGE OverloadedStrings #-}

module Donna.Web.Server.Handler.Import
  ( module X
  , module Donna.Web.Server.Handler.Import
  ) where

import Donna.Web.Server.Import as X

import Yesod.Auth as X
import Yesod.Persist as X
import Yesod.Core as X hiding (setTitle)
import qualified Yesod.Core as Yesod (setTitle)
import Yesod.Form as X hiding (check, parseTime)

import Servant.API as X hiding (Header, addHeader)
import Servant.Client as X

import Donna.Client as X hiding (runClient)

import Donna.Web.Server.Foundation as X

import Data.Aeson as JSON
import qualified Data.ByteString.Char8 as SB8
import qualified Data.ByteString.Lazy as LB
import Text.Julius (RawJS(..), RawJavascript)

setTitle :: MonadWidget m => Html -> m ()
setTitle t = Yesod.setTitle $ "Donna: " <> t

tokenJS :: Token -> String
tokenJS (Token token) = SB8.unpack token

burlJS :: BaseUrl -> RawJavascript
burlJS = rawJS . showBaseUrl

getApiUrl :: Handler BaseUrl
getApiUrl = appAPIBaseUrl <$> getYesod

jsonJS :: ToJSON a => a -> RawJavascript
jsonJS = rawJS . SB8.unpack . LB.toStrict . JSON.encode
