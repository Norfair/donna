{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE NoImplicitPrelude #-}

module Donna.Web.Server.OptParse
  ( getInstructions
  , Instructions(..)
  , Dispatch(..)
  , Settings(..)
  , ServeSettings(..)
  ) where

import Donna.Web.Server.Import

import Servant.Client
import System.Environment (getArgs, getEnvironment)
import System.Exit
import Text.Read

import Options.Applicative

import qualified Donna.Server.OptParse as API

import Donna.Web.Server.OptParse.Types

getInstructions :: IO Instructions
getInstructions = do
  Arguments cmd flags <- getArguments
  config <- getConfiguration cmd flags
  env <- getEnv
  combineToInstructions cmd flags config env

combineToInstructions :: Command -> Flags -> Configuration -> Environment -> IO Instructions
combineToInstructions (CommandServe ServeFlags {..}) Flags Configuration Environment {..} = do
  let serveSetWebHost = fromMaybe "localhost" $ serveFlagWebHost <|> envWebHost
  let serveSetWebPort = fromMaybe defaultWebPort $ serveFlagWebPort <|> envWebPort
  let serveSetAPIPort = fromMaybe defaultAPIPort $ serveFlagAPIPort <|> envAPIPort
  serveSetAPIBaseUrl <-
    case serveFlagAPIBaseUrl <|> envAPIBaseUrl <|>
         (parseBaseUrl $ "http://localhost:" <> show serveSetAPIPort) of
      Nothing -> die "No api base url"
      Just burl -> pure burl
  serveSetDBFile <-
    case serveFlagDBFile `mplus` envDBFile of
      Nothing -> resolveFile' API.defaultDBFile
      Just dbf -> resolveFile' dbf
  serveSetWebDBFile <-
    case serveFlagWebDBFile `mplus` envWebDBFile of
      Nothing -> resolveFile' defaultWebDBFile
      Just dbf -> resolveFile' dbf
  when (serveSetDBFile == serveSetWebDBFile) $
    die $
    "The DB file for the server db and for the web server db should not be the same, but they are both " <>
    fromAbsFile serveSetDBFile
  let serveSetGoogleAnalyticsTracking =
        serveFlagGoogleAnalyticsTracking <|> envGoogleAnalyticsTracking
  let serveSetGoogleSearchConsoleVerification =
        serveFlagGoogleSearchConsoleVerification <|> envGoogleSearchConsoleVerification
  serveSetGoogleSettings <- deriveGoogleSettings serveFlagGoogleFlags envGoogleEnv
  let disp = DispatchServe ServeSettings {..}
  pure $ Instructions disp Settings

deriveGoogleSettings :: GoogleFlags -> GoogleEnv -> IO (Maybe GoogleSettings)
deriveGoogleSettings GoogleFlags {..} GoogleEnv {..} = do
  let mClientId = googleFlagClientId <|> googleEnvClientId
  let mClientSecret = googleFlagClientSecret <|> googleEnvClientSecret
  case (mClientId, mClientSecret) of
    (Nothing, Nothing) -> pure Nothing
    (Just _, Nothing) -> die "Incomplete google settings, missing client secret."
    (Nothing, Just _) -> die "Incomplete google settings, missing client id."
    (Just cid, Just cs) ->
      pure $ Just $ GoogleSettings {googleSetClientId = cid, googleSetClientSecret = cs}

defaultWebPort :: Int
defaultWebPort = 8000

defaultWebDBFile :: FilePath
defaultWebDBFile = "donna-web-server.sqlite"

defaultAPIPort :: Int
defaultAPIPort = API.defaultPort

getConfiguration :: Command -> Flags -> IO Configuration
getConfiguration _ _ = pure Configuration

getEnv :: IO Environment
getEnv = do
  env <- getEnvironment
  let mv :: IsString s => String -> Maybe s
      mv k = fromString <$> lookup ("DONNA_" <> k) env
      mp :: (String -> Maybe a) -> String -> Maybe a
      mp pf k = mv k >>= pf
      mr :: Read a => String -> Maybe a
      mr = mp readMaybe
      mgv :: IsString s => String -> Maybe s
      mgv k = mv ("GOOGLE_" <> k)
      googleEnv :: GoogleEnv
      googleEnv =
        GoogleEnv {googleEnvClientId = mgv "CLIENT_ID", googleEnvClientSecret = mgv "CLIENT_SECRET"}
  pure
    Environment
      { envWebHost = mv "WEB_HOST"
      , envWebPort = mr "WEB_PORT"
      , envAPIBaseUrl = mp parseBaseUrl "API_URL"
      , envAPIPort = mr "API_PORT"
      , envDBFile = mv "DATABASE_FILE" <|> mv "DB_FILE"
      , envWebDBFile = mv "WEB_DATABASE_FILE" <|> mv "WEB_DB_FILE"
      , envGoogleAnalyticsTracking = mgv "ANALYTICS_TRACKING"
      , envGoogleSearchConsoleVerification = mgv "SEARCH_CONSOLE_VERIFICATION"
      , envGoogleEnv = googleEnv
      }

getArguments :: IO Arguments
getArguments = do
  args <- getArgs
  let result = runArgumentsParser args
  handleParseResult result

runArgumentsParser :: [String] -> ParserResult Arguments
runArgumentsParser = execParserPure prefs_ argParser
  where
    prefs_ =
      ParserPrefs
        { prefMultiSuffix = ""
        , prefDisambiguate = True
        , prefShowHelpOnError = True
        , prefShowHelpOnEmpty = True
        , prefBacktrack = True
        , prefColumns = 80
        }

argParser :: ParserInfo Arguments
argParser = info (helper <*> parseArgs) help_
  where
    help_ = fullDesc <> progDesc description
    description = "Donna web server"

parseArgs :: Parser Arguments
parseArgs = Arguments <$> parseCommand <*> parseFlags

parseCommand :: Parser Command
parseCommand = hsubparser $ mconcat [command "serve" parseCommandServe]

parseCommandServe :: ParserInfo Command
parseCommandServe = info parser modifier
  where
    parser =
      CommandServe <$>
      (ServeFlags <$>
       option
         (Just <$> str)
         (mconcat
            [long "web-host", metavar "Host", value Nothing, help "the host to serve web server on"]) <*>
       option
         (Just <$> auto)
         (mconcat
            [long "web-port", metavar "PORT", value Nothing, help "the port to serve web server on"]) <*>
       option
         (Just <$> maybeReader parseBaseUrl)
         (mconcat
            [long "api-url", metavar "URl", value Nothing, help "the url to call the API server on"]) <*>
       option
         (Just <$> auto)
         (mconcat
            [ long "api-port"
            , metavar "PORt"
            , value Nothing
            , help "the port to serve the API server on"
            ]) <*>
       option
         (Just <$> str)
         (mconcat
            [ long "db-file"
            , metavar "FILE"
            , value Nothing
            , help "the database file to use for the api server"
            ]) <*>
       option
         (Just <$> str)
         (mconcat
            [ long "web-db-file"
            , metavar "FILE"
            , value Nothing
            , help "the database file to use for the web server"
            ]) <*>
       option
         (Just <$> str)
         (mconcat
            [ long "google-analytics-tracking"
            , metavar "GOOGLE_ANALYTICS_TRACKING_CODE"
            , value Nothing
            , help "the google analytics tracking code"
            ]) <*>
       option
         (Just <$> str)
         (mconcat
            [ long "google-search-console-verification"
            , metavar "GOOGLE_SEARCH_CONSOLE_VERIFICATION_CODE"
            , value Nothing
            , help "the google search console verification code"
            ]) <*>
       parseGoogleFlags)
    modifier = fullDesc <> progDesc "Serve."

parseGoogleFlags :: Parser GoogleFlags
parseGoogleFlags =
  GoogleFlags <$>
  option
    (Just <$> str)
    (mconcat
       [ long "google-client-id"
       , metavar "CLIENT_ID"
       , value Nothing
       , help "The google project client id"
       ]) <*>
  option
    (Just <$> str)
    (mconcat
       [ long "google-client-secret"
       , metavar "CLIENT_SECRET"
       , value Nothing
       , help "The google project client secret"
       ])

parseFlags :: Parser Flags
parseFlags = pure Flags
