{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE CPP #-}

module Donna.Web.Server.Static.TH
  ( mkFrontendStatic
  ) where

import Donna.Web.Server.Import
import System.Environment

import Yesod.EmbeddedStatic
import Yesod.EmbeddedStatic.Remote

import Donna.Web.Server.Constants

import Language.Haskell.TH


mkFrontendStatic :: Q [Dec]
mkFrontendStatic = do
  env <- runIO getEnvironment
  let mFront pathLocal pathNix =
        let var = "FRONT_CODE"
         in case lookup var env of
              Nothing -> pathLocal
              Just d -> d ++ "/" ++ pathNix
  let mFrontF fp = mFront ("front/out/" ++ fp ++ ".js") (fp ++ ".js")
  mkEmbeddedStatic
    development
    "myStatic"
    [ embedFileAt "social-graph.js" $ mFrontF "SocialGraph"
    , embedFileAt "note.js" $ mFrontF "Note"
    , embedFileAt "search.js" $ mFrontF "Search"
    , embedRemoteFileAt
        "static/bootstrap.css"
        "https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
    , embedRemoteFileAt "static/jquery.js" "https://code.jquery.com/jquery-3.3.1.slim.min.js"
    , embedRemoteFileAt
        "static/popper.js"
        "https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
    , embedRemoteFileAt
        "static/bootstrap.js"
        "https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
    , embedRemoteFileAt "elm-canvas.js" "https://unpkg.com/elm-canvas/elm-canvas.js"
    ]
