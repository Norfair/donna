{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE CPP #-}

module Donna.Web.Server.Constants where

import Donna.Web.Server.Import

development :: Bool
#ifdef DEVELOPMENT
development = True
#else
development = False
#endif
