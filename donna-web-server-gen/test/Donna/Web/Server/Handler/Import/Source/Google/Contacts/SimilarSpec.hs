{-# LANGUAGE NoImplicitPrelude #-}

module Donna.Web.Server.Handler.Import.Source.Google.Contacts.SimilarSpec
  ( spec
  ) where

import Donna.Web.Server.TestImport

import Donna.Web.Server.Handler.Import.Source.Google.Contacts.Similar

spec :: Spec
spec =
  describe "findSimilarPeople" $
  it "produces valid results" $ producesValidsOnValids2 findSimilarPeople
