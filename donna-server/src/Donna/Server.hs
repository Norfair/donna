{-# LANGUAGE DataKinds #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}

module Donna.Server
  ( donnaServer
  , runDonnaServer
  , makeDonnaServerApp
  ) where

import Donna.Server.Import

import Network.Wai as Wai
import qualified Network.Wai.Handler.Warp as Warp
import Network.Wai.Middleware.Cors
import qualified Network.Wai.Middleware.RequestLogger as Wai
import Servant.Auth.Server as Servant
import Servant.Server as Servant

import Donna.API

import Donna.Server.OptParse

import Donna.Server.Constants
import Donna.Server.Env

import Donna.Server.Serve

donnaServer :: IO ()
donnaServer = do
  Instructions (DispatchServe ss) Settings <- getInstructions
  runDonnaServer ss

runDonnaServer :: ServeSettings -> IO ()
runDonnaServer ss@ServeSettings {..} =
  withServerEnv ss $ \se -> do
    let serverApp = makeDonnaServerApp se
    let extraMiddles =
          if development
            then Wai.logStdoutDev
            else Wai.logStdout
    let policy = simpleCorsResourcePolicy {corsRequestHeaders = "Authorization" : simpleHeaders}
    let middle = extraMiddles . cors (\_ -> Just policy)
    let app_ = middle serverApp
    Warp.run serveSetPort app_

makeDonnaServerApp :: ServerEnv -> Wai.Application
makeDonnaServerApp se =
  let cfg = serverEnvCookieSettings se :. serverEnvJWTSettings se :. EmptyContext
   in Servant.serveWithContext donnaAPI cfg $
      Servant.hoistServerWithContext
        donnaAPI
        (Proxy :: Proxy DonnaContext)
        (flip runReaderT se)
        donnaServantServer

type DonnaContext = '[ CookieSettings, JWTSettings]
