{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE NoImplicitPrelude #-}

module Donna.Server.Handler.Sync.Entries
  ( syncEntries,
  )
where

import Data.Mergeful as Mergeful
import Data.Mergeful.Persistent as Mergeful
import Database.Persist
import Database.Persist.Sql
import qualified Donna.CLI.Data as CLI
import Donna.Server.Handler.Import

syncEntries :: UserId -> UTCTime -> Mergeful.SyncRequest CLI.EntryId EntryId AddedEntry -> SqlPersistT IO (Mergeful.SyncResponse CLI.EntryId EntryId AddedEntry)
syncEntries uid now = serverProcessSyncQuery EntryServerTime [EntryUser ==. uid] load add updateEntry
  where
    load :: Entry -> Timed AddedEntry
    load Entry {..} =
      Mergeful.Timed
        { Mergeful.timedValue =
            AddedEntry
              { addedEntryCreated = entryCreated,
                addedEntryPerson = entryPerson,
                addedEntryContents = entryContents
              },
          Mergeful.timedTime = entryServerTime
        }
    add :: AddedEntry -> Entry
    add AddedEntry {..} =
      Entry
        { entryUser = uid,
          entryPerson = addedEntryPerson,
          entryContents = addedEntryContents,
          entryCreated = now,
          entryServerTime = Mergeful.initialServerTime
        }
    updateEntry :: AddedEntry -> [Update Entry]
    updateEntry AddedEntry {..} = [EntryContents =. addedEntryContents] -- The person and the created time are not updated
