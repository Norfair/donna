{-# LANGUAGE DataKinds #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}

module Donna.Server.Handler.Auth where

import qualified Data.ByteString.Lazy as LB
import qualified Data.Text as T
import qualified Data.Text.Encoding as TE

import Servant.Auth.Server

import Donna.Server.Handler.Import

servePostRegister :: Register -> DonnaHandler NoContent
servePostRegister Register {..} = do
  maybeHashedPassword <- liftIO $ passwordHash registerPassword
  case maybeHashedPassword of
    Nothing -> throwError err400 {errBody = "Failed to hash password."}
    Just hashedPassword -> do
      now <- liftIO getCurrentTime
      let user =
            User
              { userUsername = registerUsername
              , userHashedPassword = hashedPassword
              , userCreated = now
              }
      maybeUserEntity <- runDB . getBy $ UniqueUsername $ userUsername user
      case maybeUserEntity of
        Nothing -> runDB $ insert_ user
        Just _ ->
          throwError
            err409
              { errBody =
                  LB.fromStrict $
                  TE.encodeUtf8 $
                  T.unwords
                    ["Account with the username", usernameText registerUsername, "already exists."]
              }
  pure NoContent

servePostLogin ::
     Login
  -> DonnaHandler (Headers '[ Header "Set-Cookie" SetCookie, Header "Set-Cookie" SetCookie] NoContent)
servePostLogin Login {..} = do
  me <- runDB $ getBy $ UniqueUsername loginUsername
  case me of
    Nothing -> throwError err401
    Just (Entity uid user) ->
      if validatePassword (userHashedPassword user) (TE.encodeUtf8 loginPassword)
        then setLoggedIn uid
        else throwError err401
  where
    setLoggedIn uid = do
      let cookie = AuthCookie {authCookieId = uid}
      ServerEnv {..} <- ask
      mApplyCookies <- liftIO $ acceptLogin serverEnvCookieSettings serverEnvJWTSettings cookie
      case mApplyCookies of
        Nothing -> throwError err401
        Just applyCookies -> return $ applyCookies NoContent
