{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}

module Donna.Server.Handler.People where

import Donna.Server.Handler.Import

import qualified Data.Map as M
import qualified Data.Set as S

serveGetPeopleProperties :: AuthCookie -> DonnaHandler (Map PersonUuid PersonPropertyInfo)
serveGetPeopleProperties (AuthCookie uid) =
  runDB $ do
    aliases <- map entityVal <$> selectList [PersonAliasUser ==. uid] []
    entries <- map entityVal <$> selectList [EntryUser ==. uid] []
    pure $ combine aliases entries

combine :: [PersonAlias] -> [Entry] -> Map PersonUuid PersonPropertyInfo
combine aliases entries =
  M.unionsWith go $
  [ M.fromList $
    flip map aliases $ \PersonAlias {..} ->
      let uuid = personAliasPerson
          pp =
            PersonPropertyInfo
              { personPropertyInfoAliases = S.singleton personAliasName
              , personPropertyInfoEntry = Nothing
              }
       in (uuid, pp)
  , M.fromList $
    flip map entries $ \Entry {..} ->
      let uuid = entryPerson
          pp =
            PersonPropertyInfo
              {personPropertyInfoAliases = S.empty, personPropertyInfoEntry = Just entryContents}
       in (uuid, pp)
  ]
  where
    go :: PersonPropertyInfo -> PersonPropertyInfo -> PersonPropertyInfo
    go ppi1 ppi2 =
      PersonPropertyInfo
        { personPropertyInfoAliases =
            S.union (personPropertyInfoAliases ppi1) (personPropertyInfoAliases ppi2)
        , personPropertyInfoEntry = personPropertyInfoEntry ppi1 <|> personPropertyInfoEntry ppi2
        }
