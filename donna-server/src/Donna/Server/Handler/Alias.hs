{-# LANGUAGE DataKinds #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE NoImplicitPrelude #-}

module Donna.Server.Handler.Alias where

import qualified Data.Map as M
import qualified Data.Set as S
import Donna.Server.Handler.Import

serveGetAliases :: AuthCookie -> DonnaHandler (Map Alias PersonUuid)
serveGetAliases (AuthCookie uid) =
  runDB $ do
    aliases <- map entityVal <$> selectList [PersonAliasUser ==. uid] []
    pure $ M.fromList $ map (\PersonAlias {..} -> (personAliasName, personAliasPerson)) aliases

serveGetPersonAliases :: AuthCookie -> PersonUuid -> DonnaHandler (Set Alias)
serveGetPersonAliases (AuthCookie uid) person =
  runDB $
    (S.fromList . map (personAliasName . entityVal))
      <$> selectList [PersonAliasUser ==. uid, PersonAliasPerson ==. person] []

servePostAlias :: AuthCookie -> PersonUuid -> Alias -> DonnaHandler NoContent
servePostAlias (AuthCookie uid) person a = do
  now <- liftIO getCurrentTime
  mKey <-
    runDB $
      insertUnique
        PersonAlias
          { personAliasUser = uid,
            personAliasName = a,
            personAliasPerson = person,
            personAliasCreated = now
          }
  case mKey of
    Nothing -> throwError $ err409 {errBody = "Alias already exists for a different person"}
    Just _ -> pure NoContent

serveDeleteAlias :: AuthCookie -> Alias -> DonnaHandler NoContent
serveDeleteAlias (AuthCookie uid) a = do
  runDB $ deleteWhere [PersonAliasUser ==. uid, PersonAliasName ==. a]
  pure NoContent

serveDeletePersonAlias :: AuthCookie -> PersonUuid -> Alias -> DonnaHandler NoContent
serveDeletePersonAlias (AuthCookie uid) person a = do
  runDB $ deleteWhere [PersonAliasUser ==. uid, PersonAliasName ==. a, PersonAliasPerson ==. person]
  pure NoContent
