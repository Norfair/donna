{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}

module Donna.Server.Handler.Graph where

import Donna.Server.Handler.Import

import qualified Data.Map as M
import qualified Data.Set as S

serveGetGraph ::
     AuthCookie -> DonnaHandler SocialGraph
serveGetGraph (AuthCookie uid) =
  runDB $ do
    aliases <- map entityVal <$> selectList [PersonAliasUser ==. uid] []
    noteRelevancies <- map entityVal <$> selectList [NoteRelevancyUser ==. uid] []
    let socialGraphPeople =
          M.fromList $ map (\PersonAlias {..} -> (personAliasPerson, personAliasName)) aliases
        socialGraphNotes = buildRelevancyList noteRelevancies
    pure SocialGraph {..}

buildRelevancyList :: [NoteRelevancy] -> Map NoteUuid (Set PersonUuid)
buildRelevancyList = foldl' go M.empty
  where
    go :: Map NoteUuid (Set PersonUuid) -> NoteRelevancy -> Map NoteUuid (Set PersonUuid)
    go m NoteRelevancy {..} = M.alter go' noteRelevancyNote m
      where
        go' :: Maybe (Set PersonUuid) -> Maybe (Set PersonUuid)
        go' ms =
          case ms of
            Nothing -> Just $ S.singleton noteRelevancyPerson
            Just s -> Just $ S.insert noteRelevancyPerson s
