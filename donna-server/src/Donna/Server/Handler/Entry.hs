{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}

module Donna.Server.Handler.Entry where

import Donna.Server.Handler.Import

servePutEntry :: AuthCookie -> PersonUuid -> PersonEntry -> DonnaHandler NoContent
servePutEntry (AuthCookie uid) puuid entry = do
  void $ runDB $ putPersonEntry uid puuid entry
  pure NoContent
