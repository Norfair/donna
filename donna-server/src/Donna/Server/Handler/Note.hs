{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}

module Donna.Server.Handler.Note where

import Donna.Server.Handler.Import

import qualified Data.Set as S

servePostNote :: AuthCookie -> NewNote -> DonnaHandler NoContent
servePostNote (AuthCookie uid) NewNote {..} = do
  void $
    runDB $ do
      puuids <- getOrCreateAliases uid (S.toList newNoteRelevantPeople)
      createNote uid puuids newNoteContents
  pure NoContent
