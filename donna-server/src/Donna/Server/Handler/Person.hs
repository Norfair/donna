{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}

module Donna.Server.Handler.Person where

import Donna.Server.Handler.Import

import qualified Data.Map as M
import qualified Data.Set as S

serveGetPerson :: AuthCookie -> PersonUuid -> DonnaHandler PersonInfo
serveGetPerson (AuthCookie uid) puuid =
  runDB $ do
    personInfoAliases <-
      S.fromList . map (personAliasName . entityVal) <$>
      selectList [PersonAliasUser ==. uid, PersonAliasPerson ==. puuid] []
    aliasMap <-
      do aliases <-
           map entityVal <$> selectList [PersonAliasUser ==. uid, PersonAliasPerson !=. puuid] []
         pure $
           reverseMap $
           M.fromList $ map (\PersonAlias {..} -> (personAliasName, personAliasPerson)) aliases
    personInfoEntry <- fmap (entryContents . entityVal) <$> getBy (UniqueEntry uid puuid)
    personInfoNotes <-
      do theseRelevancies <-
           map entityVal <$>
           selectList [NoteRelevancyUser ==. uid, NoteRelevancyPerson ==. puuid] []
         notes <-
           map entityVal <$>
           selectList
             [NoteUser ==. uid, NoteUuid <-. map noteRelevancyNote theseRelevancies]
             [Desc NoteCreated]
         relevancies <-
           map entityVal <$>
           selectList
             [ NoteRelevancyUser ==. uid
             , NoteRelevancyPerson !=. puuid
             , NoteRelevancyNote <-. map noteUuid notes
             ]
             []
         let makePersonNote Note {..} =
               PersonNote
                 { personNoteContents = noteContents
                 , personNoteTimestamp = noteCreated
                 , personNoteOtherRelevantPeople =
                     M.fromList $
                     map
                       ((\pu -> (pu, fromMaybe S.empty $ M.lookup pu aliasMap)) .
                        noteRelevancyPerson) $
                     filter ((== noteUuid) . noteRelevancyNote) relevancies
                 }
         pure $ map makePersonNote notes
    pure PersonInfo {..}

reverseMap :: (Ord a, Ord b) => Map a b -> Map b (Set a)
reverseMap = foldl go M.empty . M.toList
  where
    go m (a, b) = M.alter go' b m
      where
        go' ms =
          Just $
          case ms of
            Nothing -> S.singleton a
            Just s -> S.insert a s

servePostPersonWithAliases :: AuthCookie -> Set Alias -> DonnaHandler PersonUuid
servePostPersonWithAliases (AuthCookie uid) as = do
  existent <- runDB $ selectList [PersonAliasUser ==. uid, PersonAliasName <-. S.toList as] []
  if null existent
    then runDB $ createAliases uid as
    else throwError err409 {errBody = "One of the aliases already exists."}
