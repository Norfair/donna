{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE NoImplicitPrelude #-}

module Donna.Server.Handler.Sync
  ( servePostSync,
  )
where

import qualified Data.Map as M
import Data.Mergeless (ServerSyncProcessor (..))
import qualified Data.Mergeless as Mergeless
import qualified Data.Mergeless.Persistent as Mergeless
import qualified Data.Set as S
import qualified Donna.CLI.Data as CLI
import Donna.Server.Handler.Import
import Donna.Server.Handler.Sync.Entries

servePostSync :: AuthCookie -> ClientSyncRequest -> DonnaHandler ServerSyncResponse
servePostSync (AuthCookie uid) ClientSyncRequest {..} = do
  now <- liftIO getCurrentTime
  runDB $ do
    ssyncResponseAliases <- syncAliases uid csyncRequestAliases
    ssyncResponseNotes <- syncNotes uid now csyncRequestNotes
    ssyncResponseBlobs <- syncBlobs uid now csyncRequestBlobs
    ssyncResponseEntries <- syncEntries uid now csyncRequestEntries
    pure ServerSyncResponse {..}

syncAliases ::
  UserId ->
  Mergeless.SyncRequest CLI.PersonAliasId PersonAliasId AddedAlias ->
  SqlPersistT IO (Mergeless.SyncResponse CLI.PersonAliasId PersonAliasId AddedAlias)
syncAliases uid = Mergeless.serverProcessSyncQuery [PersonAliasUser ==. uid] load add
  where
    load :: PersonAlias -> AddedAlias
    load PersonAlias {..} =
      AddedAlias
        { addedAliasName = personAliasName,
          addedAliasPerson = personAliasPerson,
          addedAliasCreated = personAliasCreated
        }
    add :: AddedAlias -> PersonAlias
    add AddedAlias {..} =
      PersonAlias
        { personAliasUser = uid,
          personAliasName = addedAliasName,
          personAliasPerson = addedAliasPerson,
          personAliasCreated = addedAliasCreated
        }

syncNotes ::
  UserId ->
  UTCTime ->
  Mergeless.SyncRequest CLI.NoteId NoteId AddedNote ->
  SqlPersistT IO (Mergeless.SyncResponse CLI.NoteId NoteId AddedNote)
syncNotes uid now = Mergeless.processServerSyncCustom proc
  where
    proc :: Mergeless.ServerSyncProcessor CLI.NoteId NoteId AddedNote (SqlPersistT IO)
    proc =
      Mergeless.ServerSyncProcessor
        { serverSyncProcessorRead = do
            nrs <- selectList [NoteUser ==. uid] []
            fmap M.fromList $ forM nrs $ \(Entity nid Note {..}) -> do
              nrrs <- selectList [NoteRelevancyUser ==. uid, NoteRelevancyNote ==. noteUuid] []
              let addedNoteUuid = noteUuid
              let addedNoteCreated = noteCreated
              let addedNoteContents = noteContents
              let addedNoteRelevantPeople = S.fromList $ map (noteRelevancyPerson . entityVal) nrrs
              pure (nid, AddedNote {..}),
          serverSyncProcessorAddItems = \m -> forM m $ \AddedNote {..} -> do
            uuid <- nextRandomUUID
            nid <-
              insert
                Note
                  { noteUser = uid,
                    noteUuid = uuid,
                    noteContents = addedNoteContents,
                    noteCreated = now
                  }
            forM_ (S.toList addedNoteRelevantPeople) $ \puuid ->
              insert_
                NoteRelevancy
                  { noteRelevancyUser = uid,
                    noteRelevancyNote = uuid,
                    noteRelevancyPerson = puuid
                  }
            pure nid,
          serverSyncProcessorDeleteItems = \s -> fmap (S.fromList . catMaybes) $ forM (S.toList s) $ \nid -> do
            mn <- get nid
            forM mn $ \Note {..} -> do
              deleteWhere [NoteRelevancyNote ==. noteUuid]
              deleteWhere [NoteId ==. nid]
              pure nid
        }

syncBlobs ::
  UserId ->
  UTCTime ->
  Mergeless.SyncRequest CLI.BlobId BlobId AddedBlob ->
  SqlPersistT IO (Mergeless.SyncResponse CLI.BlobId BlobId AddedBlob)
syncBlobs uid now = Mergeless.processServerSyncCustom proc
  where
    proc :: Mergeless.ServerSyncProcessor CLI.BlobId BlobId AddedBlob (SqlPersistT IO)
    proc =
      Mergeless.ServerSyncProcessor
        { serverSyncProcessorRead = do
            nrs <- selectList [BlobUser ==. uid] []
            fmap M.fromList $ forM nrs $ \(Entity nid Blob {..}) -> do
              nrrs <- selectList [BlobRelevancyUser ==. uid, BlobRelevancyBlob ==. blobUuid] []
              let addedBlobUuid = blobUuid
              let addedBlobCreated = blobCreated
              let addedBlobName = blobName
              let addedBlobFormat = blobFormat
              let addedBlobContents = blobContents
              let addedBlobRelevantPeople = S.fromList $ map (blobRelevancyPerson . entityVal) nrrs
              pure (nid, AddedBlob {..}),
          serverSyncProcessorAddItems = \m -> forM m $ \AddedBlob {..} -> do
            uuid <- nextRandomUUID
            nid <-
              insert
                Blob
                  { blobUser = uid,
                    blobUuid = uuid,
                    blobName = addedBlobName,
                    blobFormat = addedBlobFormat,
                    blobContents = addedBlobContents,
                    blobCreated = now
                  }
            forM_ (S.toList addedBlobRelevantPeople) $ \puuid ->
              insert_
                BlobRelevancy
                  { blobRelevancyUser = uid,
                    blobRelevancyBlob = uuid,
                    blobRelevancyPerson = puuid
                  }
            pure nid,
          serverSyncProcessorDeleteItems = \s -> fmap (S.fromList . catMaybes) $ forM (S.toList s) $ \nid -> do
            mn <- get nid
            forM mn $ \Blob {..} -> do
              deleteWhere [BlobRelevancyBlob ==. blobUuid]
              deleteWhere [BlobId ==. nid]
              pure nid
        }
