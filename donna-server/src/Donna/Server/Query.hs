{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE NoImplicitPrelude #-}

module Donna.Server.Query where

import qualified Data.Mergeful.Timed as Mergeful
import qualified Data.Set as S
import Donna.Server.Data.DB

getOrCreateAlias :: MonadIO m => UserId -> Alias -> SqlPersistT m PersonUuid
getOrCreateAlias uid a = do
  mpa <- getBy $ UniqueAlias uid a
  case mpa of
    Nothing -> do
      uuid <- nextRandomUUID
      now <- liftIO getCurrentTime
      void
        $ insertUnique
        $ PersonAlias
          { personAliasUser = uid,
            personAliasName = a,
            personAliasPerson = uuid,
            personAliasCreated = now
          }
      pure uuid
    Just (Entity _ pa) -> pure $ personAliasPerson pa

getOrCreateAliases :: MonadIO m => UserId -> [Alias] -> SqlPersistT m [PersonUuid]
getOrCreateAliases uid as = mapM (getOrCreateAlias uid) as

-- Note: this will go wrong if any of the aliases already exists
createAliases :: MonadIO m => UserId -> Set Alias -> SqlPersistT m PersonUuid
createAliases uid as = do
  uuid <- nextRandomUUID
  now <- liftIO getCurrentTime
  void
    $ insertMany_
    $ flip map (S.toList as)
    $ \a ->
      PersonAlias
        { personAliasUser = uid,
          personAliasName = a,
          personAliasPerson = uuid,
          personAliasCreated = now
        }
  pure uuid

createNote :: MonadIO m => UserId -> [PersonUuid] -> Text -> SqlPersistT m NoteUuid
createNote uid ps c = do
  uuid <- nextRandomUUID
  now <- liftIO getCurrentTime
  void $
    insert
      Note {noteUser = uid, noteUuid = uuid, noteContents = c, noteCreated = now}
  void
    $ insertMany
    $ flip map ps
    $ \p ->
      NoteRelevancy {noteRelevancyUser = uid, noteRelevancyNote = uuid, noteRelevancyPerson = p}
  pure uuid

createBlob ::
  MonadIO m =>
  UserId ->
  [PersonUuid] ->
  BlobName ->
  BlobFormat ->
  ByteString ->
  SqlPersistT m BlobUuid
createBlob uid ps bn bf c = do
  uuid <- nextRandomUUID
  now <- liftIO getCurrentTime
  void $
    insert
      Blob
        { blobUser = uid,
          blobUuid = uuid,
          blobName = bn,
          blobFormat = bf,
          blobContents = c,
          blobCreated = now
        }
  void
    $ insertMany
    $ flip map ps
    $ \p ->
      BlobRelevancy {blobRelevancyUser = uid, blobRelevancyBlob = uuid, blobRelevancyPerson = p}
  pure uuid

getPersonEntry :: MonadIO m => UserId -> PersonUuid -> SqlPersistT m (Maybe PersonEntry)
getPersonEntry uid uuid = fmap (entryContents . entityVal) <$> getBy (UniqueEntry uid uuid)

putPersonEntry :: MonadIO m => UserId -> PersonUuid -> PersonEntry -> SqlPersistT m ()
putPersonEntry uid uuid pe = do
  now <- liftIO getCurrentTime
  me <- getBy (UniqueEntry uid uuid)
  case me of
    Nothing ->
      insert_
        Entry
          { entryUser = uid,
            entryPerson = uuid,
            entryContents = pe,
            entryCreated = now,
            entryServerTime = Mergeful.initialServerTime
          }
    Just (Entity eid Entry {..}) ->
      update eid [EntryContents =. pe, EntryServerTime =. Mergeful.incrementServerTime entryServerTime]
