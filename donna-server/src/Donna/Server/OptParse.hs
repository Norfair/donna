{-# LANGUAGE RecordWildCards #-}

module Donna.Server.OptParse
  ( getInstructions
  , Instructions(..)
  , Dispatch(..)
  , Settings(..)
  , ServeSettings(..)
  , defaultPort
  , defaultDBFile
  ) where

import Donna.Server.Import

import System.Environment (getArgs, getEnvironment)
import Text.Read

import Options.Applicative

import Donna.Server.OptParse.Types

getInstructions :: IO Instructions
getInstructions = do
  Arguments cmd flags <- getArguments
  config <- getConfiguration cmd flags
  env <- getEnv
  combineToInstructions cmd flags config env

combineToInstructions :: Command -> Flags -> Configuration -> Environment -> IO Instructions
combineToInstructions (CommandServe ServeFlags {..}) Flags Configuration Environment {..} = do
  let port = fromMaybe defaultPort $ serveFlagPort `mplus` envPort
  dbFile <-
    case serveFlagDBFile `mplus` envDBFile of
      Nothing -> resolveFile' defaultDBFile
      Just dbf -> resolveFile' dbf
  let disp = DispatchServe ServeSettings {serveSetPort = port, serveSetDBFile = dbFile}
  pure $ Instructions disp Settings

defaultPort :: Int
defaultPort = 8001

defaultDBFile :: FilePath
defaultDBFile = "donna-server.sqlite"

getConfiguration :: Command -> Flags -> IO Configuration
getConfiguration _ _ = pure Configuration

getEnv :: IO Environment
getEnv = do
  env <- getEnvironment
  let mv k = lookup k env
  pure
    Environment {envPort = mv "PORT" >>= readMaybe, envDBFile = mv "DATABASE_FILE" <|> mv "DB_FILE"}

getArguments :: IO Arguments
getArguments = do
  args <- getArgs
  let result = runArgumentsParser args
  handleParseResult result

runArgumentsParser :: [String] -> ParserResult Arguments
runArgumentsParser = execParserPure prefs_ argParser
  where
    prefs_ =
      ParserPrefs
        { prefMultiSuffix = ""
        , prefDisambiguate = True
        , prefShowHelpOnError = True
        , prefShowHelpOnEmpty = True
        , prefBacktrack = True
        , prefColumns = 80
        }

argParser :: ParserInfo Arguments
argParser = info (helper <*> parseArgs) help_
  where
    help_ = fullDesc <> progDesc description
    description = "Donna server"

parseArgs :: Parser Arguments
parseArgs = Arguments <$> parseCommand <*> parseFlags

parseCommand :: Parser Command
parseCommand = hsubparser $ mconcat [command "serve" parseCommandServe]

parseCommandServe :: ParserInfo Command
parseCommandServe = info parser modifier
  where
    parser =
      CommandServe <$>
      (ServeFlags <$>
       option
         (Just <$> auto)
         (mconcat [long "port", metavar "PORT", value Nothing, help "the port to serve on"]) <*>
       option
         (Just <$> str)
         (mconcat [long "db-file", value Nothing, help "the database file to use"]))
    modifier = fullDesc <> progDesc "Serve."

parseFlags :: Parser Flags
parseFlags = pure Flags
