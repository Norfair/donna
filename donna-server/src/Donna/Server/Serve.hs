{-# LANGUAGE AllowAmbiguousTypes #-}
{-# LANGUAGE ExplicitForAll #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}

module Donna.Server.Serve
  ( donnaServantServer
  ) where

import Servant.API.Generic
import Servant.Auth.Server
import Servant.Server
import Servant.Server.Generic

import Donna.API

import Donna.Server.Env

import Donna.Server.Handler

donnaServantServer :: ServerT DonnaAPI DonnaHandler
donnaServantServer = toServant donnaServerRecord

donnaServerRecord :: DonnaAPIRoutes (AsServerT DonnaHandler)
donnaServerRecord =
  DonnaAPIRoutes
    { unprotecedRoutes = toServant donnaUnprotectedServerRoutes
    , protectedRoutes = toServant donnaProtectedServerRoutes
    }

donnaUnprotectedServerRoutes :: DonnaUnprotectedRoutes (AsServerT DonnaHandler)
donnaUnprotectedServerRoutes =
  DonnaUnprotectedRoutes {postRegister = servePostRegister, postLogin = servePostLogin}

donnaProtectedServerRoutes :: DonnaProtectedRoutes (AsServerT DonnaHandler)
donnaProtectedServerRoutes =
  DonnaProtectedRoutes
    { getAliases = withAuthResult serveGetAliases
    , getPersonAliases = withAuthResult serveGetPersonAliases
    , postAlias = withAuthResult servePostAlias
    , deleteAlias = withAuthResult serveDeleteAlias
    , deletePersonAlias = withAuthResult serveDeletePersonAlias
    , getPerson = withAuthResult serveGetPerson
    , postPersonWithAliases = withAuthResult servePostPersonWithAliases
    , getPeopleProperties = withAuthResult serveGetPeopleProperties
    , postNote = withAuthResult servePostNote
    , putEntry = withAuthResult servePutEntry
    , postSync = withAuthResult servePostSync
    , getGraph = withAuthResult serveGetGraph
    }

withAuthResult :: ThrowAll a => (AuthCookie -> a) -> (AuthResult AuthCookie -> a)
withAuthResult func ar =
  case ar of
    Authenticated ac -> func ac
    _ -> throwAll err401
