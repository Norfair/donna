{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE RecordWildCards #-}

module Donna.Server.Env where

import Donna.Server.Import

import Control.Monad.Logger
import Crypto.JOSE.JWK (JWK)
import Data.Aeson as JSON
import Data.Aeson.Encode.Pretty as JSON
import qualified Data.ByteString.Lazy as LB
import qualified Data.Text as T
import qualified Database.Persist.Sqlite as DB
import System.Exit

import Lens.Micro

import Servant.Auth.Server as Auth
import Servant.Server

import Donna.Server.Data

import Donna.Server.OptParse.Types

type DonnaHandler = ReaderT ServerEnv Handler

data ServerEnv =
  ServerEnv
    { serverEnvConnectionPool :: DB.ConnectionPool
    , serverEnvCookieSettings :: CookieSettings
    , serverEnvJWTSettings :: JWTSettings
    }

runDB :: DB.SqlPersistT IO a -> DonnaHandler a
runDB func = do
  pool <- asks serverEnvConnectionPool
  liftIO $ DB.runSqlPool func pool

withServerEnv :: ServeSettings -> (ServerEnv -> IO a) -> IO a
withServerEnv ServeSettings {..} func = do
  runNoLoggingT $ do
    DB.withSqlitePoolInfo
      (DB.mkSqliteConnectionInfo (T.pack $ toFilePath serveSetDBFile) & DB.walEnabled .~ False &
       DB.fkEnabled .~ False)
      1 $ \pool -> do
      signingKey <- liftIO loadSigningKey
      let env =
            ServerEnv
              { serverEnvConnectionPool = pool
              , serverEnvCookieSettings = defaultCookieSettings
              , serverEnvJWTSettings = defaultJWTSettings signingKey
              }
      NoLoggingT $
        flip runReaderT env $ do
          ensureDir $ parent serveSetDBFile
          void $ DB.runSqlPool (DB.runMigrationSilent migrateAll) pool
          liftIO $ func env

signingKeyFile :: IO (Path Abs File)
signingKeyFile = resolveFile' "signing-key.json"

storeSigningKey :: JWK -> IO ()
storeSigningKey key_ = do
  skf <- signingKeyFile
  LB.writeFile (toFilePath skf) (JSON.encodePretty key_)

loadSigningKey :: IO JWK
loadSigningKey = do
  skf <- signingKeyFile
  mErrOrKey <- forgivingAbsence $ JSON.eitherDecode <$> LB.readFile (toFilePath skf)
  case mErrOrKey of
    Nothing -> do
      key_ <- Auth.generateKey
      storeSigningKey key_
      pure key_
    Just (Left err) ->
      die $ unlines ["Failed to load signing key from file", fromAbsFile skf, "with error:", err]
    Just (Right r) -> pure r
