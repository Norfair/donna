{-# LANGUAGE CPP #-}
{-# LANGUAGE NoImplicitPrelude #-}

module Donna.Server.Constants where

import Donna.Server.Import

development :: Bool
#ifdef DEVELOPMENT
development = True
#else
development = False
#endif
