module Donna.Server.OptParse.Types where

import Donna.Server.Import

data Instructions =
  Instructions Dispatch Settings

newtype Dispatch =
  DispatchServe ServeSettings
  deriving (Show, Eq)

data ServeSettings =
  ServeSettings
    { serveSetPort :: Int
    , serveSetDBFile :: Path Abs File
    }
  deriving (Show, Eq)

data Settings =
  Settings
  deriving (Show, Eq)

data Arguments = Arguments Command Flags

newtype Command =
  CommandServe ServeFlags
  deriving (Show, Eq)

data ServeFlags =
  ServeFlags
    { serveFlagPort :: Maybe Int
    , serveFlagDBFile :: Maybe FilePath
    }
  deriving (Show, Eq)

data Flags =
  Flags
  deriving (Show, Eq)

data Configuration =
  Configuration
  deriving (Show, Eq)

data Environment =
  Environment
    { envPort :: Maybe Int
    , envDBFile :: Maybe FilePath
    }
  deriving (Show, Eq)
